#!/usr/bin/python

# input file
filename='entprod_a640.dat'
filename='data/entropy_production_rate_N32_a80.dat'
gmax=450
gscale=1.
outfile='entropy_rate_scang.png'
xlab=r'$gM_P/m$'

#filename='data/entropy_production_rate_N32_3fld_g0_del10000_a20.dat'
#gmax=250
#gscale=100.
#outfile='entropy_rate_scangeff.png'
#xlab=r'$g_{eff}M_P/m$'

import sys
sys.path.insert(0, '/home/jbraden/Documents/python_scripts/')

import myfileutils as myf
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np
import matplotlib as mpl

mpl.rc('xtick',labelsize=20)
mpl.rc('ytick',labelsize=20)

# Start by reading in the data
vals=myf.read_timeblock(filename,[0,1,2])

gvals=[]
avals=[]
entvals=[]
for i in range(len(vals)):
    gvals.append(vals[i][0])
    avals.append(vals[i][1])
    entvals.append(vals[i][2])

# Normalize to maximum entropy production rate
entvals = entvals/np.max(entvals)

gvals=np.array(gvals)
avals=np.array(avals)
# Now make the contour plot
#plt.contourf(gvals,avals,entvals, np.linspace(0.1,0.9,5),cmap=cm.OrRd)
plt.pcolor(gscale*gvals,avals,entvals,cmap=cm.OrRd)
plt.xlabel(xlab,fontsize=30)
plt.ylabel(r'$\ln\left(a/a_{end}\right)$',fontsize=30)
plt.ylim((1.8,3.1))
plt.xlim((50.,gmax))
cb=plt.colorbar(orientation='vertical',ticks=[0,0.25,0.5,0.75,1])
cb.ax.set_ylabel(r'$(dS/dt)_{normalized}$',fontsize=30)
plt.subplots_adjust(bottom=0.17,left=0.15)

fig=plt.gcf()
fig_width=8.5
fig.set_size_inches(fig_width,fig_width*0.5*(np.sqrt(5.)-1.))
fig.savefig(outfile)

plt.show()
