#!/usr/bin/env python

# Take the data stored in my CDF file and compute the resulting PDFs
# Inputs : infile - file storing the CDF
#        : datacols - columns with appropriate data, first column is the percentiles, others are CDF values at that percentile
#        : nlat - number of sampling sites for CDF

# To do : for ease of use make a dictionary associating variables with columns

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import MaxNLocator
# needed for Gaussian fitting
from scipy.optimize import curve_fit

savePlots=False

def fitGauss(t,a,b):
    return np.exp(-a*(t-b)**2)

#infile="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_current_split/CDF"
#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/kc2_current_split/"
basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc1_current_split/"
infile=basedir+"CDF"
logfile=basedir+"LOG.out"
datacols=[0,1,2,3,4,9,10,5,6,7,8,11,12,13,14]

component_file="current_components_pdf_m2g2_t{:.1f}.pdf"
field_file="current_fieldcomp_pdf_m2g2_t{:.1f}.pdf"
rho_file="current_lnrho_pdf_m2g2_t{:.1f}.pdf"

nlat=513   #513 #129  #257

a=np.genfromtxt(infile,usecols=datacols)
b=np.genfromtxt(logfile,usecols=[0,1])

tvals=b[:,0]
avals=b[:,1]

times=np.reshape(a[:,0],(-1,nlat))
times=times[:,0:(nlat-1)] # resize since I differentiate the CDF
percentiles=np.reshape(a[:,1],(-1,nlat))

cdf=[]
for i in range( 2,len(a[0]) ):
    cdf.append(np.reshape(a[:,i],(-1,nlat)))

pdf=[]
vals=[]
# Now create the PDFs
for i in range(len(cdf)):
    size=len(cdf[i])
    pdf.append(1./np.diff(cdf[i],axis=1))
    vals.append( 0.5*(cdf[i][:,1:nlat]+cdf[i][:,0:(nlat-1)]) )

pdfnorm=[]
for v in range(len(pdf)):
    tmp=pdf[v]
    for x in range(len(tmp)):
        tmp[x] /= np.max(tmp[x])
    pdfnorm.append(tmp)

def plot_pdfs(tindex,doFit=False,sigma=[1.,1.]):
    plt.plot(vals[0][tindex],pdf[0][tindex],'bo',label=r'$\log(\rho/\bar{\rho})$',mec='b')
    plt.plot(vals[1][tindex],pdf[1][tindex],'ro',label=r'$\partial_t\log(\rho/\bar{\rho})$',mec='r')
#    plt.plot(vals[2][tindex],pdf[2][tindex],'g--',label=r'$3H\left(\frac{P}{\rho}-\frac{\bar{P}}{\bar{\rho}}\right)$',linewidth=2)
#    plt.plot(vals[3][tindex],pdf[3][tindex],'k-.',label=r'$\partial_iT_{\phi}^{i0}/\rho$',linewidth=1.5)
#    plt.plot(vals[4][tindex],pdf[4][tindex],'c--',label=r'$\partial_iT_{\chi}^{i0}/\rho$',linewidth=1.5)

# Now do Gaussian fitting
    if doFit:
        x=vals[0][tindex][5:nlat-6]
        y=pdf[0][tindex][5:nlat-6]
        pFit, cFit = curve_fit(fitGauss,x,y,p0=(sigma[0],0.))
        plt.plot(vals[0][tindex],fitGauss(vals[0][tindex],pFit[0],pFit[1]),'b',label='Gaussian fit')
        print pFit, cFit
        x=vals[1][tindex][5:nlat-6]
        y=pdf[1][tindex][5:nlat-6]
        pFit, cFit = curve_fit(fitGauss,x,y,p0=(sigma[1],0.))
        plt.plot(vals[1][tindex],fitGauss(vals[1][tindex],pFit[0],pFit[1]),'r',label='Gaussian fit')

    plt.legend()
    plt.ylabel(r'$P/P_{max}$')
    plt.text(0.05,0.95,r'$mt='+str(times[tindex][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

def plot_pdfs_current_split(tindex):
    plt.plot(vals[5][tindex],pdf[5][tindex],'b',label=r'$\dot{\phi}\nabla^2\phi/a^2\rho$')
    plt.plot(vals[6][tindex],pdf[6][tindex],'r',label=r'$\dot{\chi}\nabla^2\chi/a^2\rho$')
    plt.plot(vals[7][tindex],pdf[7][tindex],'g--',label=r'$\nabla\dot{\phi}\cdot\nabla\phi/a^2\rho$')
    plt.plot(vals[8][tindex],pdf[8][tindex],'k--',label=r'$\nabla\dot{\chi}\cdot\nabla\chi/a^2\rho$')

    plt.legend()
    plt.ylabel(r'$P/P_{max}$')
    plt.text(0.05,0.95,r'$mt='+str(times[tindex][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

def plot_pdfs_components(tindex):
    plt.plot(vals[3][tindex],pdf[3][tindex],'b',label=r'$\partial_iT^{i0}_{\phi}/\rho$')
    plt.plot(vals[4][tindex],pdf[4][tindex],'r',label=r'$\partial_iT^{i0}_{\chi}/\rho$')
    plt.plot(vals[9][tindex],pdf[9][tindex],'g-.',label=r'$\partial_iT^{i0}_{kin}/\rho$')
    plt.plot(vals[10][tindex],pdf[10][tindex],'k-.',label=r'$\partial_iT^{i0}_{grad}/\rho$')
    plt.plot(vals[11][tindex],pdf[11][tindex],'c--',label=r'$\partial_i(T^{i0}_{kin}-T^{i0}_{grad}/\rho$')
    plt.plot(vals[12][tindex],pdf[12][tindex],'m--',label=r'$\partial_i(T^{i0}_\phi-T^{i0}_\chi)/\rho$')

    plt.legend()
    plt.ylabel(r'$P/P_{max}$')
    plt.text(0.05,0.95,r'$mt='+str(times[tindex][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

time=220
plot_pdfs_components(time)
plt.xlim(-0.02,0.02)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
plt.show()

plot_pdfs_current_split(time)
plt.xlim(-0.015,0.015)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
plt.show()

plot_pdfs(time,True,[1./0.01**2,1./0.01**2])
plt.xlim(-0.03,0.03)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
plt.show()

time=240
plot_pdfs_components(time)
plt.xlim(-0.5,0.5)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
plt.show()

plot_pdfs_current_split(time)
plt.xlim(-0.5,0.5)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
plt.show()

plot_pdfs(time,True)
plt.xlim(-0.6,0.6)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
plt.show()

time=245
plot_pdfs_components(time)
plt.xlim(-0.6,0.6)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
plt.show()
plot_pdfs_current_split(time)
plt.xlim(-0.4,0.4)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
plt.show()
plot_pdfs(time,True,sigma=[0.5,0.5])
plt.xlim(-0.75,0.75)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
plt.show()

time=250
plot_pdfs_components(time)
plt.xlim(-1.2,1.2)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
plt.show()
plot_pdfs_current_split(time)
plt.xlim(-0.7,0.7)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
plt.show()
plot_pdfs(time,True)
plt.xlim(-1.,1.)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
plt.show()

time=300
plot_pdfs_components(time)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
plt.show()
plot_pdfs_current_split(time)
plt.xlim(-0.7,0.7)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
plt.show()
plot_pdfs(time,True)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
plt.show()

time=600
plot_pdfs_components(time)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
plt.show()
plot_pdfs_current_split(time)
plt.xlim(-1.2,1.2)
if savePlots:
    plt.savefig(field_file.format(times[time][0]) )
plt.show()

plot_pdfs(time,True)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
plt.show()


tcut=700
plt.contourf(times[0:tcut],vals[0][0:tcut],pdf[0][0:tcut],cmap=plt.cm.OrRd)
plt.ylim(-3.,3.)
plt.xlabel(r'$mt$')
plt.ylabel(r'$\ln(\rho/\bar{\rho})$')
cbar=plt.colorbar()
cbar.set_label(r'$P(\ln(\rho/\bar{\rho}))/P_{max}$')
plt.show()

plt.contourf(times[0:tcut],vals[1][0:tcut],pdf[1][0:tcut],cmap=plt.cm.OrRd)
plt.ylim(-5.,5.)
plt.xlabel(r'$mt$')
plt.ylabel(r'$\partial_t\ln(\rho/\bar{\rho})$')
cbar=plt.colorbar()
cbar.set_label(r'$P(\partial_t\ln(\rho/\bar{\rho}))/P_{max}$')
plt.show()
