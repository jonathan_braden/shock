#!/usr/bin/python

paper_width=5.95

import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
import mymathutils as mym
from matplotlib.ticker import LogLocator, MaxNLocator
from pylab import rcParams

def plot_components(itime):
    for i in range(len(spectra)):
        plt.plot( kvals[itime],np.abs(spectra[i][itime]) )
    plt.xscale('log')
    plt.yscale('log')
    return

#
# Plot the cross-correlators normalized to the fluctuations in the fields
#
def plot_crosscorrelations_normalized_fields(itime,time):
    plt.plot( kvals[itime], np.abs(spectra[ind_dict['phiPi']][itime])/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$|C_{\phi\Pi}|$')
    plt.plot( kvals[itime], np.real(spectra[ind_dict['phiPi']][itime])/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$Re(C_{\phi\Pi})$')
    plt.plot( kvals[itime], np.imag(spectra[ind_dict['phiPi']][itime])/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$Im(C_{\phi\Pi})$')

    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1.1,1.1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$')
    plt.ylabel(r'$C_{\alpha\beta}$')

    tlabel=r'$\sqrt{\lambda}M_P\tau='+'{:.2f}'.format(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel)  # adjust as needed depending on choice of scales

    plt.legend(loc='lower right')
    return

def plot_crosscorrelations_normalized_lnrho(itime,time):
    plt.plot( kvals[itime], np.abs(spectra[ind_dict['lnrlnp']][itime])/spectra[ind_dict['lnrho']][itime]**0.5/spectra[ind_dict['lnp']][itime]**0.5, label=r'$|C_{\ln\rho,\partial_t\ln\rho}|$')
    plt.plot( kvals[itime], np.real(spectra[ind_dict['lnrlnp']][itime])/spectra[ind_dict['lnrho']][itime]**0.5/spectra[ind_dict['lnp']][itime]**0.5, label=r'$Re(C_{\ln\rho\partial_t\ln\rho})$' )
    plt.plot( kvals[itime], np.imag(spectra[ind_dict['lnrlnp']][itime])/spectra[ind_dict['lnrho']][itime]**0.5/spectra[ind_dict['lnp']][itime]**0.5, label=r'$Im(C_{\ln\rho\partial_t\ln\rho})$')

    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1.1,1.1)
    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$')
    plt.ylabel(r'$C_{\alpha\beta}$')
    plt.text(kvals[0][nlat/20],0.75,r'$\sqrt{\lambda}M_P\tau='+'{:.2f}'.format(time)+'$'.format(time))

    plt.legend(loc='lower right')

def plot_determinants(itime):
    plt.plot( kvals[itime], np.log(nphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}})$')
    plt.plot( kvals[itime], np.log(detphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}} - P_{\phi\dot{\phi}}^2)$')

    plt.legend()
    return

def plot_determinants_normed(itime,tnorm):
    plt.plot( kvals[itime], nphi[itime]/nphi[tnorm], 'b', label=r'$P_{\phi\phi}P_{\Pi_{\phi}\Pi_{\phi}}$')
    plt.plot( kvals[itime], detphi[itime]/detphi[tnorm], 'b--', label=r'$\Delta_{\phi}$')
    plt.plot( kvals[itime], nlnrho[itime]/nlnrho[tnorm], 'r',label=r'$n_{\ln\rho}$')
    plt.plot( kvals[itime], detlnrho[itime]/detlnrho[tnorm], 'r--', label=r'$\Delta_{\ln\rho}$')

    plt.legend()

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$')
    plt.ylabel(r'$F(k,t)/F(k,t=0)$')
    return

def compute_determinants():
    global nphi
    nphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]
    global detphi
    detphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]-np.abs(spectra[ind_dict['phiPi']])**2

    global nlnrho
    nlnrho=spectra[ind_dict['lnrho']]*spectra[ind_dict['lnp']]
    global detlnrho
    detlnrho=spectra[ind_dict['lnrho']]*spectra[ind_dict['lnp']]-np.abs(spectra[ind_dict['lnrlnp']])**2
                
def compute_crossspec_norm():
    global phi_phidot
    phi_phidot = np.abs(spectra[ind_dict['phiPi']])/spectra[ind_dict['phi']]**0.5/spectra[ind_dict['Pi']]**0.5 

    global lnr_dlnr
    lnr_dlnr = np.abs(spectra[ind_dict['lnrlnp']])/spectra[ind_dict['lnrho']]**0.5/spectra[ind_dict['lnp']]**0.5

#To do: add dk part
def compute_entropies(kcut):
    global s_phi, ds_phi
    s_phi=[]
    global s_detphi, ds_detphi
    s_detphi=[]
    global s_lnr, s_dlnr
    s_lnr=[]
    s_dlnr=[]
    global s_detlnr
    s_detlnr=[]
    global s_detlnr_noncanon
    s_detlnr_noncanon=[]

    dk = kvals[0,1]-kvals[0,0]

    for kmax in kcut:
        kvol = dk*np.sum(kvals[0][1:kmax]**2)
        s_phi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(nphi[:,1:kmax]),axis=1) )
        s_detphi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(detphi[:,1:kmax]),axis=1) )
        s_lnr.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(nlnrho[:,1:kmax]),axis=1) )
        s_detlnr.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(detlnrho[:,1:kmax]),axis=1) )
        s_detlnr_noncanon.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(detlnrho[:,1:kmax]/jacobian_linear[:,1:kmax]**2),axis=1) )
 
    s_phi=np.array(s_phi)
    ds_phi=np.diff(s_phi,axis=1)
    s_detphi=np.array(s_detphi)
    ds_detphi=np.diff(s_detphi,axis=1)

    s_lnr=np.array(s_lnr)
    s_detlnr=np.array(s_detlnr)
    s_detlnr_noncanon=np.array(s_detlnr_noncanon)

    return

def plot_entropy_differences(kind):
#    plt.plot( ,s_phi[kind]-s_detphi[kind],label=r'$S_{n_{\phi}}-S_{\Delta_{\phi}}$')
#    plt.plot( ,s_chi[kind]-s_detchi[kind],label=r'$S_{n_{\chi}}-S_{\Delta_{\chi}}$')
#    plt.plot( ,s_detphi[kind]+s_detchi[kind]-s_dettot,label=r'$S_{\Delta_{\phi}}+S_{\Delta_{\chi}} - S_{\Delta_{tot}}$')
    
    plt.yscale('log')
    plt.xlim(0.,500.)
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$\Delta S$')

    tlabel=r'$\frac{k_{cut}}{m}='+str(kvals[kcuts[kind]])+'$'
#    plt.text(,0.75,tlabel,fontsize=20)

# User modified specifications for the files to make the graph
basedir='/mnt/scratch-lustre/jbraden/entropy_paper/l4/cross_spectra/L20/kc2_lnrho_derivs_wcurdiff/'
infile=basedir+'PSD'
logfile=basedir+'LOG.out'

ind_dict={'phi':3,
          'Pi':4,
          'phiPi':5,
          'lnrho':0,
          'lnp':1,
          'lnrlnp':2}

datacols=[3,4,5,6,7,8,9,10,11]
nlat=512

numk=int(3.**0.5*nlat/2)+2  # don't touch this
a=np.genfromtxt(infile,usecols=datacols)

kvals=np.reshape(a[:,0],(-1,numk))

spectra=[]
spectra.append( np.reshape(a[:,1],(-1,numk)) )
spectra.append( np.reshape(a[:,2],(-1,numk)) )
spectra.append( np.reshape(a[:,3],(-1,numk)) + 1j*np.reshape(a[:,4],(-1,numk)) )

spectra.append( np.reshape(a[:,5],(-1,numk)) )
spectra.append( np.reshape(a[:,6],(-1,numk)) )
spectra.append( np.reshape(a[:,7],(-1,numk)) + 1j*np.reshape(a[:,8],(-1,numk)) )

b=np.genfromtxt(logfile,usecols=[0,1,2,3,4,12,13,5])
times=b[:,0]
afac=b[:,1]
hubvals=b[:,2]
rho_ave=b[:,3]
prs_ave=b[:,4]
phi_ave=b[:,5]
piphi_ave=b[:,6]
lnr_ave=b[:,7]
w_ave=prs_ave/rho_ave

# Now I need to get the a-dependence
jacobian_norm_1 = piphi_ave**2/rho_ave**2/afac**2
jacobian_norm_2 = -6.*hubvals*piphi_ave*phi_ave**3/rho_ave**2
jacobian_norm_1 = jacobian_norm_1 / afac**3
jacobian_norm_2 = jacobian_norm_2 / afac**3
jacobian_linear=[]
for i in range(len(kvals)):
    jacobian_linear.append(kvals[i]**2*jacobian_norm_1[i]+jacobian_norm_2[i])
jacobian_linear=np.array(jacobian_linear)

compute_determinants()
compute_crossspec_norm()

kcuts=[63,127,191,255]
dk=kvals[0,1]-kvals[0,0]
def compute_jacobian_entropy(kc):
    s_jac=[]
    for kmax in kcuts:
        kvol = dk*np.sum(kvals[0][1:kmax]**2)
        s_jac.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(jacobian_linear[:,1:kmax]**2),axis=1) )
    return s_jac
compute_entropies(kcuts)
s_jacobian = compute_jacobian_entropy(kcuts)

# Make the comparison with Jacobian
def plot_noncanonical_determinant(tcur,leg):
    knyq=nlat/2
    plt.plot(kvals[tcur,1:knyq],detlnrho[tcur,1:knyq]/(kvals[tcur,1:knyq]**2*jacobian_norm_1[tcur]+jacobian_norm_2[tcur])**2,'b',label=r'$\Delta^2_{\ln\rho}/\mathcal{J}^2$')
    plt.plot(kvals[tcur,1:knyq],detphi[tcur,1:knyq],'r--',label=r'$\Delta^2_\phi$')
    if (leg):
        plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure)
    plt.yscale('log')
    plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
    plt.xscale('log')
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$')
    plt.ylabel(r'$\Delta^2_\phi,\Delta^2_{\ln\rho}/\mathcal{J}^2$')
    plt.xlim(kvals[0,1],kvals[0,knyq])

def plot_noncanonical_determinant_full(tcur,leg,leg_loc='upper left'):
    knyq=nlat/2
    plt.plot(kvals[tcur,1:knyq],detlnrho[tcur,1:knyq]/(kvals[tcur,1:knyq]**2*jacobian_norm_1[tcur]+jacobian_norm_2[tcur])**2,'b',label=r'$\Delta^2_{\ln\rho}/\mathcal{J}^2$')
    plt.plot(kvals[tcur,1:knyq],detphi[tcur,1:knyq],'r-',label=r'$\Delta^2_\phi$')
    plt.plot(kvals[tcur,1:knyq],jacobian_linear[tcur,1:knyq]**2,'g-.',label=r'$\mathcal{J}^2$')
    plt.plot(kvals[tcur,1:knyq],detlnrho[tcur,1:knyq],'m',label=r'$\Delta^2_{\ln\rho}$')
    if (leg):
        plt.legend(loc=leg_loc,bbox_to_anchor=(0,0,1.1,1.1))
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$')
    plt.ylabel(r'$\Delta^2_\alpha$')
    plt.xlim(kvals[0,1],kvals[0,knyq])
    plt.ylim(1.e-7,1.e6)
    plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
    plt.text(0.05,0.95,r'$\sqrt{\lambda}M_P\tau = %.2f$' % times[tcur],transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

##################################
fsize = myplt.set_output_figure(0.48*paper_width,[0.22,0.23,0.95,0.9])

plt.figure(figsize=fsize)
plot_noncanonical_determinant(0,True)
for tind in range(100,601,100):
    plot_noncanonical_determinant(tind,False)
plt.savefig('determinants_noncanonical_phi4_preshock.pdf')
if showPreview:
    plt.show()
plt.clf()

plt.figure(figsize=fsize)
plot_noncanonical_determinant(650,True)
for tind in range(700,951,50):
    plot_noncanonical_determinant(tind,False)
plt.savefig('determinants_noncanonical_phi4_postshock.pdf')
if showPreview:
    plt.show()
plt.clf()

#############
fsize=myplt.set_output_figure(0.32*paper_width,[0.3,0.3,0.95,0.9])
tplots=[5,6,8,50,2000]
for tcur in tplots:
    plt.figure(1,fsize)
    plot_noncanonical_determinant_full(tcur,True) # keep
    plt.savefig('noncanonical_det_components_l4_t{:.1f}.pdf'.format(times[tcur]))
    if showPreview:
        plt.show()
    plt.clf()
plot_noncanonical_determinant_full(213,True,leg_loc='upper left')
plt.ylim(1.e-14,1.e10)
plt.savefig('noncanonical_det_components_l4_t{:.1f}.pdf'.format(times[213]))
if showPreview:
    plt.show()
plt.clf()

#unified plot for paper
left, right, bottom, top = 0.1,0.95,0.2,0.85
fsize=myplt.set_output_figure(0.95*paper_width,[0.12,0.25,0.95,0.78],ratio=1./3./1.618)
plt.figure()
plt.clf()
plt.subplot(131)
plot_noncanonical_determinant_full(8,False)
plt.gca().yaxis.set_major_locator(LogLocator(numticks=4))
plt.subplot(132)
plot_noncanonical_determinant_full(6,False)
plt.ylabel('')
plt.gca().yaxis.set_major_locator(LogLocator(numticks=4))
plt.subplot(133)
plot_noncanonical_determinant_full(213,False)
plt.ylabel('')
plt.gca().yaxis.set_major_locator(LogLocator(numticks=4))
plt.ylim(1.e-14,1.e10)

plt.legend(loc='upper center',bbox_to_anchor=(left,top,right-left,1-top),bbox_transform=plt.gcf().transFigure,ncol=4)
plt.subplots_adjust(wspace=0.28)
plt.savefig('noncanonical_det_components_multipanel.pdf')
plt.show()

print 'Cross Correlation Size :', myplt.set_output_figure(0.48*paper_width,[0.25,0.25,0.95,0.9])
plot_times=[400,500,600,700,800,2000]
for tcur in plot_times:
    plot_crosscorrelations_normalized_lnrho(tcur,times[tcur])
    plt.savefig('crosscorrelations_fields_l4_t{:.1f}.pdf'.format(times[tcur]))
    if showPreview:
        plt.show()
    plt.clf()
    plot_crosscorrelations_normalized_fields(tcur,times[tcur])
    plt.savefig('crosscorrelations_lnr_l4_t{:.1f}.pdf'.format(times[tcur]))
    if showPreview:
        plt.show()
    plt.clf()


def plot_entropies(kc_ind,tc_ind):
    kmax_ind=kcuts[kc_ind]
    fig, axi = plt.subplots(nrows=2,sharex=True)
    axi[0].plot(times[:tc_ind],s_detlnr_noncanon[kc_ind][:tc_ind],'g:',alpha=0.5)
    axi[0].plot(times[:tc_ind],mym.smooth(s_detlnr_noncanon[kc_ind][:tc_ind],51),'g-',label=r'$S_{\ln\rho}^{nc}$')
    axi[0].plot(times[:tc_ind],s_detphi[kc_ind][:tc_ind],'b:',alpha=0.25)
    axi[0].plot(times[:tc_ind],mym.smooth(s_detphi[kc_ind][:tc_ind],51),'b-',label=r'$S_{\phi}$')

    axi[1].plot(times[:tc_ind],s_detlnr[kc_ind][:tc_ind],'r:',alpha=0.25)
    axi[1].plot(times[:tc_ind],mym.smooth(s_detlnr[kc_ind][:tc_ind],51),'r-',label=r'$S_{\ln\rho}^{c}$')
#    axi[0].plot(times[:tc_ind],mym.savitzky_golay(s_detphi[kc_ind][:tc_ind],101,4),'b-',label=r'$S_{\phi}$')
#    axi[1].plot(times[:tc_ind],mym.savitzky_golay(s_detlnr[kc_ind][:tc_ind],101,4),'r-',label=r'$S_{\ln\rho}^{c}$')
#    axi[0].plot(times[:tc_ind],mym.smooth(s_detlnr_noncanon[kc_ind][:tc_ind],51),'g--')
#    axi[0].plot(times[:tc_ind],mym.savitzky_golay(s_detlnr_noncanon[kc_ind][:tc_ind],101,4),'g-',label=r'$S_{\ln\rho}^{nc}$')

    axi[1].plot(times[:tc_ind],s_jacobian[kc_ind][:tc_ind],'c:',alpha=0.25)
    axi[1].plot(times[:tc_ind],mym.smooth(s_jacobian[kc_ind][:tc_ind],51),'c',label=r'$S_{\mathcal{J}}$')

    axi[0].legend(loc='upper left',bbox_to_anchor=(0,0,1.1,1.1))
    axi[1].legend(loc='lower right',bbox_to_anchor=(0,-0.1,1.1,1))

    axi[0].set_xlim(0.,times[tc_ind])
    axi[1].set_xlim(0.,times[tc_ind])
    axi[0].set_ylabel(r'$S/\mathcal{N}_{eff}$')
    axi[1].set_ylabel(r'$S/\mathcal{N}_{eff}$')

    axi[0].yaxis.set_major_locator(MaxNLocator(5))
    axi[1].yaxis.set_major_locator(MaxNLocator(5))

    plt.xlabel(r'$\sqrt{\lambda}M_P\tau$')
    plt.subplots_adjust(hspace=0)

fsize=myplt.set_output_figure(0.48*paper_width,[0.22,0.23,0.95,0.9])    
plt.figure(figsize=fsize)
plot_entropies(3,len(times)-1)
plt.savefig('entropies_l4_fullt.pdf')
if showPreview:
    plt.show()
plt.clf()
plot_entropies(3,600)
plt.savefig('entropies_l4_preshock.pdf')
if showPreview:
    plt.show()
plt.clf()

fsize=myplt.set_output_figure(0.32*paper_width,[0.3,0.3,0.95,0.9])
def plot_entropy_derivs(kc_ind,wsize=[51,51,51],line_type='-'):
    numt=len(times)
    tave=0.5*(times[:numt-1]+times[1:])
#    plt.plot(tave,np.diff(s_detphi[kc_ind])/np.diff(times),'b:',alpha=0.25)
    plt.plot(tave,mym.smooth(np.diff(s_detphi[kc_ind]),wsize[0])/np.diff(times),'b',label=r'$dS_{\phi}/d\tau$')
#    plt.plot(tave,np.diff(s_detlnr[kc_ind])/np.diff(times),'r:',alpha=0.25)
    plt.plot(tave,mym.smooth(np.diff(s_detlnr[kc_ind]),wsize[1])/np.diff(times),'r',label=r'$dS^c_{\ln\rho}/d\tau$')
#    plt.plot(tave,np.diff(s_detlnr_noncanon[kc_ind])/np.diff(times),'g:',alpha=0.25)
    plt.plot(tave,mym.smooth(np.diff(s_detlnr_noncanon[kc_ind]),wsize[2])/np.diff(times),'g',label=r'$dS_{\ln\rho}^{nc}/d\tau$')

# Third paper width figures
fsize=myplt.set_output_figure(0.32*paper_width,[0.3,0.3,0.95,0.9])
plt.figure(figsize=fsize)
kind_cur=3
tmax_ind=len(times)-1
#plt.plot(times[0:tmax_ind],s_detphi[kind_cur][0:tmax_ind],label=r'$S_{\phi}$')
#plt.plot(times[0:tmax_ind],s_noncanonical[kind_cur][0:tmax_ind],label=r'$S_{\ln\rho}^{noncanonical}$')
#plt.plot(times[0:tmax_ind],s_detlnr[kind_cur][0:tmax_ind],label=r'$S_{\ln\rho}^{canonical}$')
#plt.plot(times[0:tmax_ind],kvol[kind_cur]*np.log(jacobian[0:tmax_ind]),label=r'$\ln(\mathcal{J})$')
#plt.plot(times[0:tmax_ind],np.abs(s_detlnr_noncanon[kind_cur][0:tmax_ind]-s_detphi[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{noncanonical}-S_{\phi}|$')
#plt.plot(times[0:tmax_ind],np.abs(s_detlnr[kind_cur][0:tmax_ind]-s_detphi[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{canonical}-S_{\phi}|$')

#plt.plot(times[0:tmax_ind],s_detlnr_noncanon[kind_cur][0:tmax_ind],label=r'$S_{\ln\rho}^{noncanonical}$')
plt.plot(times[0:tmax_ind],s_detphi[kind_cur][0:tmax_ind],'b:',alpha=0.5)
plt.plot(times[0:tmax_ind],s_detlnr[kind_cur][0:tmax_ind],'r:',alpha=0.5)
plt.plot(times[0:tmax_ind],mym.smooth(s_detphi[kind_cur][0:tmax_ind],101),'b',label=r'$S_{\phi}$')
plt.plot(times[0:tmax_ind],mym.smooth(s_detlnr[kind_cur][0:tmax_ind],101),'r',label=r'$S_{\ln\rho}^{c}$')
#plt.plot(times[0:tmax_ind],s_jacobian[kind_cur][0:tmax_ind],label=r'$S_{\mathcal{J}}$',alpha=0.5)
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$')
plt.ylabel(r'$S/\mathcal{N}_{eff}$')
plt.legend(loc='lower right',bbox_to_anchor=(0,0,1.1,1),borderaxespad=0.25)
plt.xlim(0,1000)
plt.ylim(-22,20)
plt.gca().set_xticks([0,300,600,900])
plt.gca().yaxis.set_major_locator(MaxNLocator(5))
plt.savefig('entropy_l4_lnrho_and_field.pdf')
if showPreview:
    plt.show()
plt.clf()

# To do: add the volume factor into the above normalizations
#plot_entropy_derivs(3,[51,51,51])
#plt.legend(loc='upper right')
plt.figure(figsize=fsize)
plot_entropy_derivs(3,[101,101,101])
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.25)
#plot_entropy_derivs(3,[201,201,201])
plt.ylim(-0.05,0.3)
plt.xlim(times[0],times[len(times)-1])
plt.gca().set_yticks([0,0.1,0.2,0.3])
plt.gca().set_xticks([0,300,600,900])
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$')
plt.ylabel(r'$\mathcal{N}_{eff}^{-1}dS/d\tau$')
plt.savefig('dsdt_smooth_l4.pdf')
if showPreview:
    plt.show()
plt.clf()

plt.plot(times,1./np.abs(lnr_ave),'b',alpha=0.25)
plt.plot(times,np.abs(1./mym.smooth(lnr_ave,51)),'b')

plt.xlabel(r'$\sqrt{\lambda}M_P\tau$')
plt.ylabel(r'$1/|\ln(\rho/\bar{\rho})|$')

plt.yscale('log')
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))
plt.xlim(0,1000)
plt.gca().set_xticks([0,300,600,900])
plt.savefig('mach_number_l4.pdf')
if showPreview:
    plt.show()
plt.clf()
