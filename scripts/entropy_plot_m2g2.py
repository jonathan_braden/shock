#!/usr/bin/python

# Plot the spectra for the fields and cross-correlators individually
# as well as the resulting determinant to go into the entropy

# To do (Jan. 29, 2014) - pass in a dictionary or something to set up the determinants instead of hard coding the various indices

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import mymathutils as mym

mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

def plot_components(itime):
    for i in range(len(spectra)):
        plt.plot( kvals[itime],np.abs(spectra[i][itime]) )
    plt.xscale('log')
    plt.yscale('log')
    return

#
# Plot the cross-correlators normalized to the fluctuations in the fields
#
def plot_crosscorrelations_normalized(itime,time,legend):
    plt.plot( kvals[itime], np.abs(spectra[ind_dict['phiPi']][itime])/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$|C_{\ln\rho,\partial_t\ln\rho}|$')
    plt.plot( kvals[itime], np.real(spectra[ind_dict['phiPi']][itime])/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$Re(C_{\ln\rho,\partial_t\ln\rho})$')
    plt.plot( kvals[itime], np.imag(spectra[ind_dict['phiPi']][itime])/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$Im(C_{\ln\rho,\partial_t\ln\rho})$')

    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1.1,1.1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$',fontsize=20)
    plt.ylabel(r'$C_{\alpha\beta}$',fontsize=20)

    tlabel=r'$mt='+str(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel,fontsize=20)  # adjust as needed depending on choice of scales
    if (legend):
        plt.legend(fontsize=20,loc='best')
    return

def plot_determinants(itime):
    plt.plot( kvals[itime], np.log(nphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}})$')
    plt.plot( kvals[itime], np.log(detphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}} - P_{\phi\dot{\phi}}^2)$')

    plt.legend(loc='upper right')
    plt.xscale('log')
    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    return

def plot_determinants_normed(itime,tnorm,legend):
    plt.plot( kvals[itime], nphi[itime]/nphi[tnorm], 'b', label=r'$P_{\ln\rho\ln\rho}P_{\partial_t\ln\rho\partial_t\ln\rho}$')
    plt.plot( kvals[itime], detphi[itime]/detphi[tnorm], 'r--',markersize=5., label=r'$\Delta_{\ln\rho\partial_t\ln\rho}$')

    if (legend):
        plt.legend(fontsize=18)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$k/m$',fontsize=26)
    plt.ylabel(r'$F(k,t)/F(k,t=0)$',fontsize=26)
    return

def compute_determinants():
    global nphi
    nphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]
    global detphi
    detphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]-np.abs(spectra[ind_dict['phiPi']])**2
    global detphi_re
    detphi_re=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]-np.real(spectra[ind_dict['phiPi']])**2
    global detphi_im
    detphi_im=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]-np.imag(spectra[ind_dict['phiPi']])**2

def compute_crossspec_norm():
    global phi_phidot
    phi_phidot = spectra[ind_dict['phiPi']]/spectra[ind_dict['phi']]**0.5/spectra[ind_dict['Pi']]**0.5 

#To do: add dk part
def compute_entropies(kcut):
    global s_phi, ds_phi
    s_phi=[]
    global s_detphi, ds_detphi
    s_detphi=[]
    global s_lnrho,s_dlnrho
    s_lnrho=[]
    s_dlnrho=[]

    dk = kvals[0,1]-kvals[0,0]

    for kmax in kcut:
        kvol=np.sum(kvals[0][1:kmax]**2)*dk
        lnr_det=spectra[ind_dict['phi']]
        s_lnrho.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(lnr_det[:,1:kmax]),axis=1) )
        dlnr_det=spectra[ind_dict['Pi']]
        s_dlnrho.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(dlnr_det[:,1:kmax]),axis=1) )
        s_phi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(nphi[:,1:kmax]),axis=1) )
        s_detphi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(detphi[:,1:kmax]),axis=1) )
 
    s_phi=np.array(s_phi)
    ds_phi=np.diff(s_phi,axis=1)/np.diff(times)
    s_detphi=np.array(s_detphi)
    ds_detphi=np.diff(s_detphi,axis=1)/np.diff(times)
    s_lnrho=np.array(s_lnrho)

    return

def plot_entropy_differences(kind):
#    plt.plot( ,s_phi[kind]-s_detphi[kind],label=r'$S_{n_{\phi}}-S_{\Delta_{\phi}}$')
#    plt.plot( ,s_chi[kind]-s_detchi[kind],label=r'$S_{n_{\chi}}-S_{\Delta_{\chi}}$')
#    plt.plot( ,s_detphi[kind]+s_detchi[kind]-s_dettot,label=r'$S_{\Delta_{\phi}}+S_{\Delta_{\chi}} - S_{\Delta_{tot}}$')
    
    plt.yscale('log')
    plt.xlim(0.,500.)
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$\Delta S$')

    tlabel=r'$\frac{k_{cut}}{m}='+str(kvals[kcuts[kind]])+'$'
#    plt.text(,0.75,tlabel,fontsize=20)

def compute_crossentropies(kcut):
    global s_cross
    
    s_cross=[]
    for kmax in kcut:
        s_tmp=[]
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_phidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-chi_chidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_chi[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_chidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phidot_chi[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1-phidot_chidot[:,1:kmax]**2),axis=1) )
        s_cross.append(s_tmp)
        
    return

# User modified specifications for the files to make the graph
#basedir='/mnt/scratch-lustre/jbraden/crossspec/l4/n512/kc2_nomean/'
#basedir='/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/rho/L10/kc2_lnrho_derivs/'
basedir='/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_lnr_dlnr_cross/'
infile=basedir+'PSD'
logfile=basedir+'LOG.out'

ind_dict={'phi':0,
          'Pi':1,
          'phiPi':2}

datacols=[3,4,5,6,7]
nlat=512

numk=int(3.**0.5*nlat/2)+2  # don't touch this
a=np.genfromtxt(infile,usecols=datacols)

kvals=np.reshape(a[:,0],(-1,numk))

spectra=[]
spectra.append( np.reshape(a[:,1],(-1,numk)) )
spectra.append( np.reshape(a[:,2],(-1,numk)) )
spectra.append( np.reshape(a[:,3],(-1,numk)) + 1j*np.reshape(a[:,4],(-1,numk)) )
spectra=np.array(spectra)

# Edit this stuff to get the correct results
b=np.genfromtxt(logfile,usecols=[0,1,3,4,5,12,13,14,15,6])
times=b[:,0]
afac=b[:,1]
rho_ave=b[:,2]
prs_ave=b[:,3]
lnr_ave=b[:,4]
weff_ave=b[:,5]
# Account for having 2 fields (edit for single-field model or more than 2 fields)
phi_ave=b[:,5]
chi_ave=b[:,6]
piphi_ave=b[:,7]
pichi_ave=b[:,8]
w_ave=prs_ave/rho_ave

# Edit this (this one is for rho/pressure as variables?)
jacobian = 2.*np.abs(phi_ave**3*piphi_ave)/(afac**4*rho_ave**2*(1.+w_ave))

compute_determinants()

kcuts=[63,127,191,255]
compute_entropies(kcuts)

kvols=[]
s_noncanonical=[]
dk = kvals[0][1]-kvals[0][0]
for i in range(len(kcuts)):
    kvols.append(0.5*dk*np.sum(kvals[0][0:kcuts[i]]**2))


w_sm=mym.smooth(w_ave,51)
tplot=times
tplot_ave=0.5*(tplot[0:len(tplot)-1]+tplot[1:])
ds_sm=mym.smooth(ds_phi[2],75)
ds_sm=ds_sm/np.max(ds_sm)
cut_ind=0
t_init_ind=100

fig, axi = plt.subplots(nrows=3,sharex=True)
axi[0].plot(tplot,s_detphi[cut_ind]-s_detphi[cut_ind][t_init_ind],'b',alpha=0.25)
axi[0].plot(tplot,mym.smooth(s_detphi[cut_ind]-s_detphi[cut_ind][t_init_ind],51),'b',label=r'$S_{\ln\rho}^{tot}$',linewidth=2)
axi[0].plot(tplot,s_phi[cut_ind]-s_phi[cut_ind][t_init_ind],'r',alpha=0.25)
axi[0].plot(tplot,mym.smooth(s_detphi[cut_ind]-s_detphi[cut_ind][t_init_ind],51),'r',label=r'$S_{\ln\rho}$',linewidth=2)
axi[0].plot(tplot_ave,ds_sm,'g',label=r'$dS/dt$')
axi[0].set_ylabel(r'$(S-S(t_{ref})/\mathcal{N}_{eff}$',fontsize=22)
axi[0].set_ylim(-1.,2.)

axi[1].plot(tplot,1./np.abs(lnr_ave),'b',alpha=0.25)
axi[1].plot(tplot,1./mym.smooth(np.abs(lnr_ave),51),'b',linewidth=2)
axi[1].set_yscale('log')
axi[1].set_ylabel(r'$|\ln(\rho/\bar{\rho})|^{-1}$',fontsize=22)

axi[2].plot(tplot,3.*np.log(afac)+np.log(rho_ave)/(1.+w_sm))
#axi[2].plot(tplot,3.*(1.+w_sm)*np.log(afac)+np.log(rho_ave))
axi[2].set_ylabel(r'$3\ln a +\frac{\ln\bar{\rho}}{1+\bar{w}}$',fontsize=22)
#axi[2].plot(tplot,3.*np.log(afac)+np.log(rho_ave)/(1.+w_ave))

plt.xlabel(r'$mt$',fontsize=30)
plt.xlim(50.,400.)
# Now remove vertical space
plt.subplots_adjust(hspace=0.02,bottom=0.15,left=0.17)
