#!/usr/bin/python

######
# A collection of useful tools for making publication quality papers
######

import matplotlib.pyplot as plt
import numpy as np

#
# Give the plotted axes the same aspect ratio as the figure
# Curfig is the current figure, curax is the current axis
#
def fix_axes_aspect(curfig, curax, fix_to_fig=False, rat=0.5*(np.sqrt(5.)-1.) ):
    ax_rat=curax.get_data_ratio()
    if fix_to_fig:
        fig_size = curfig.get_size_inches()
        fig_rat = fig_size[1]/fig_size[0]
    else:
        fig_rat=rat
#    plt.axes().set_aspect(fig_rat/ax_rat,'box')
    curax.set_aspect(fig_rat/ax_rat,'box')

from matplotlib.collections import Collection
from matplotlib.artist import allow_rasterization

class ListCollection(Collection):
    def __init__(self, collections, **kwargs):
        Collection.__init__(self, **kwargs)
        self.set_collections(collections)
    def set_collections(self, collections):
        self._collections = collections
    def get_collections(self):
        return self._collections
    @allow_rasterization
    def draw(self, renderer):
        for _c in self._collections:
            _c.draw(renderer)

def insert_rasterized_contour_plot(c):
    collections = c.collections
    for _c in collections:
        _c.remove()
    cc = ListCollection(collections, rasterized=True)
    ax = plt.gca()
    ax.add_artist(cc)
    return cc

from matplotlib import rcParams
def set_output_figure(fig_width,borders,ratio=1./1.61803398875):
    left=borders[0]
    bottom=borders[1]
    right=borders[2]
    top=borders[3]

    h_rat=right-left
    v_rat=top-bottom

    f_height = fig_width*(h_rat/v_rat)*ratio

    rcParams['figure.figsize']        = fig_width, f_height
    rcParams['figure.subplot.bottom'] = bottom
    rcParams['figure.subplot.top']    = top
    rcParams['figure.subplot.left']   = left
    rcParams['figure.subplot.right']  = right

#    plt.figure()
#    plt.clf()
    rcParams.update()
    params = {'figure.figsize' : [fig_width,f_height],
              'figure.subplot.left' : left,
              'figure.subplot.bottom' : bottom,
              'figure.subplot.right' : right,
              'figure.subplot.top' : top
              }
    rcParams.update(params)
    plt.subplots_adjust(left=left,bottom=bottom,right=right,top=top)
    plt.gcf().set_size_inches(fig_width,f_height)

    return (fig_width, f_height)
