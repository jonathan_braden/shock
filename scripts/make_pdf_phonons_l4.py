#!/usr/bin/env python

# Take the data stored in my CDF file and compute the resulting PDFs
# Inputs : infile - file storing the CDF
#        : datacols - columns with appropriate data, first column is the percentiles, others are CDF values at that percentile
#        : nlat - number of sampling sites for CDF

# To do : for ease of use make a dictionary associating variables with columns

import numpy as np
import matplotlib.pyplot as plt

infile='/mnt/scratch-lustre/jbraden/entropy_paper/l4/cross_spectra/kc2_lnrho_derivs_wcurdiff/CDF'
datacols=[0,1,2,3]

nlat=513 #129  #257

a=np.genfromtxt(infile,usecols=datacols)

times=np.reshape(a[:,0],(-1,nlat))
times=times[:,0:(nlat-1)] # resize since I differentiate the CDF
percentiles=np.reshape(a[:,1],(-1,nlat))

cdf=[]
for i in range( 2,len(a[0]) ):
    cdf.append(np.reshape(a[:,i],(-1,nlat)))

pdf=[]
vals=[]
# Now create the PDFs
for i in range(len(cdf)):
    size=len(cdf[i])
    pdf.append(1./np.diff(cdf[i],axis=1))
    vals.append( 0.5*(cdf[i][:,1:nlat]+cdf[i][:,0:(nlat-1)]) )

pdfnorm=[]
for v in range(len(pdf)):
    tmp=pdf[v]
    for x in range(len(tmp)):
        tmp[x] /= np.max(tmp[x])
    pdfnorm.append(tmp)

def plot_pdfs(tindex):
    plt.plot(vals[0][tindex],pdf[0][tindex],'b',label=r'$\log(\rho/\bar{\rho})$',linewidth=2)
    plt.plot(vals[1][tindex],pdf[1][tindex],'r',label=r'$\partial_t\log(\rho/\bar{\rho})$',linewidth=2)
    plt.plot(vals[2][tindex],pdf[2][tindex],'g-',label=r'$3H\left(\frac{P}{\rho}-\frac{\bar{P}}{\bar{\rho}}\right)$',linewidth=2)
    plt.plot(vals[4][tindex],pdf[4][tindex],'k-.',label=r'$\partial_iT_{\phi}^{i0}/\rho$',linewidth=2)
    plt.plot(vals[5][tindex],pdf[5][tindex],'c--',label=r'$\partial_iT_{\chi}^{i0}/\rho$',linewidth=2)

    plt.legend()
    plt.ylabel(r'$P/P_{max}$',fontsize=20)

time=400
plot_pdfs(time)
plt.text(-1.5,0.8,r'$mt='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-0.1,0.1)
plt.show()

time=500
plot_pdfs(time)
plt.text(-1.5,0.8,r'$mt='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-2,2)
plt.show()

time=600
plot_pdfs(time)
plt.text(-2.5,0.8,r'$mt='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-3.,3.)
plt.show()    

tcut=1000
plt.contourf(times[0:tcut],vals[0][0:tcut],pdf[0][0:tcut],cmap=plt.cm.OrRd)
plt.xlabel(r'$mt$',fontsize=20)
plt.ylabel(r'$\ln(\rho/\bar{\rho})$',fontsize=20)
cbar=plt.colorbar()
cbar.set_label(r'$P(\ln(\rho/\bar{\rho}))/P_{max}$',fontsize=20)
plt.show()

plt.contourf(times[0:tcut],vals[1][0:tcut],pdf[1][0:tcut],cmap=plt.cm.OrRd)
plt.xlabel(r'$mt$',fontsize=20)
plt.ylabel(r'$\partial_t\ln(\rho/\bar{\rho})$',fontsize=20)
cbar=plt.colorbar()
cbar.set_label(r'$P(\partial_t\ln(\rho/\bar{\rho}))/P_{max}$',fontsize=20)
plt.show()
