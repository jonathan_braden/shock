import numpy as np
import matplotlib.pyplot as plt

# This is for lnrho
#basedir = "/mnt/scratch-lustre/jbraden/entropy_calcs/m2g2/four_moms/N512/10_bins/L10/kc1_a2.5/"
basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/"
momfile_phi=basedir+"kc2_phi_fourdist/FOURMOMS"
momfile_chi=basedir+"kc2_chi_fourdist/FOURMOMS"
logfile_phi=basedir+"kc2_phi_fourdist/LOG.out"
logfile_chi=basedir+"kc2_chi_fourdist/LOG.out"

nlat=512
knyq=nlat/2

c1=np.genfromtxt(logfile_phi,usecols=[0,1])
c2=np.genfromtxt(logfile_chi,usecols=[0,1])
tvals_phi=c1[:,0]
avals_phi=c1[:,1]
tvals_chi=c2[:,0]
avals_chi=c2[:,1]

numt1=len(c1[:,0])
numt2=len(c2[:,0])

datacols=[0,1,4,5,8,9]
a=np.genfromtxt(momfile_phi,usecols=datacols)
b=np.genfromtxt(momfile_chi,usecols=datacols)

numk_phi=np.reshape(a[:,0],(numt1,-1))
kvals_phi=np.reshape(a[:,1],(numt1,-1))
numk_chi=np.reshape(b[:,0],(numt2,-1))
kvals_chi=np.reshape(b[:,1],(numt2,-1))

r2_phi=np.reshape(a[:,2],(numt1,-1))
r4_phi=np.reshape(a[:,4],(numt1,-1))
i2_phi=np.reshape(a[:,3],(numt1,-1))
i4_phi=np.reshape(a[:,5],(numt1,-1))

r2_phi=0.5*(r2_phi+i2_phi)
r4_phi=0.5*(r4_phi+i4_phi)

r2_chi=np.reshape(b[:,2],(numt2,-1))
r4_chi=np.reshape(b[:,4],(numt2,-1))
i2_chi=np.reshape(b[:,3],(numt2,-1))
i4_chi=np.reshape(b[:,5],(numt2,-1))

r2_chi=0.5*(r2_chi+i2_chi)
r4_chi=0.5*(r4_chi+i4_chi)

mycmap=plt.cm.OrRd
tplots=[220,240,245,250,300,600]
for i in tplots:
    plt.plot(kvals_phi[i][1:knyq],r4_phi[i][1:knyq]/r2_phi[i][1:knyq]**2-3.,'b',label=r'$\phi$')
    plt.plot(kvals_chi[i][1:knyq],r4_chi[i][1:knyq]/r2_chi[i][1:knyq]**2-3.,'r--',label=r'$\chi$')
    plt.xlabel(r'$k/m$')
#    plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=20)
    plt.ylabel(r'$\kappa_4$')
    plt.xlim(kvals_phi[0][1],kvals_phi[0][knyq])
    plt.ylim(-1.,4.)
    plt.text(kvals_phi[i][5*knyq/8],-0.5,r'$mt='+str(tvals_phi[i])+'$')
    plt.legend()
    plt.savefig( 'kurtosis_fields_m2g2_t{:.1f}.pdf'.format(tvals_phi[i]) )
    plt.show()

# To do, put in ubiased estimators here and also combine low kmodes
def bin_lowk():
    return

from matplotlib.ticker import LogLocator
# Plots with the spectrum on the bottom
for i in tplots:
    fig, ax0 = plt.subplots(nrows=2,ncols=1,sharex=True)
    ax0[0].plot(kvals_phi[i][1:knyq],r4_phi[i][1:knyq]/r2_phi[i][1:knyq]**2-3.,'b',label=r'$\phi$')
    ax0[0].plot(kvals_chi[i][1:knyq],r4_chi[i][1:knyq]/r2_chi[i][1:knyq]**2-3.,'r--',label=r'$\chi$')
    ax0[0].set_ylabel(r'$\kappa_4$')
    ax0[1].plot(kvals_phi[i][1:knyq],r2_phi[i][1:knyq]*kvals_phi[i][1:knyq]**3,'b',label=r'$\phi$')
    ax0[1].plot(kvals_chi[i][1:knyq],r2_chi[i][1:knyq]*kvals_chi[i][1:knyq]**3,'r--',label=r'$\chi$')
    ax0[1].set_ylabel(r'$k^3P(k)$')
    plt.subplots_adjust(hspace=0)
    plt.xlabel(r'$k/m$')
    ax0[0].set_xscale('linear')
    ax0[1].set_xscale('linear')
    ax0[1].set_yscale('log')
    ax0[1].yaxis.set_major_locator(LogLocator(numticks=5))
    ax0[1].set_ylim(1.e7,1.e16)
    ax0[1].set_yticks([1.e8,1.e11,1.e14])
    ax0[0].set_ylim(-1,2.)
    plt.xlim(kvals_phi[0][1],kvals_phi[0][knyq])
    plt.legend()
    ax0[0].text(kvals_phi[i][knyq/8],-0.7,r'$mt='+str(tvals_phi[i])+'$')
    plt.savefig( 'kurtosis_fields_m2g2_wspec_t{:.1f}.pdf'.format(tvals_phi[i]) )
    plt.show()

clevs=np.linspace(-1.,1.,21)
plt.contourf(kvals_phi[0,1:knyq],tvals_phi,r4_phi[:,1:knyq]/r2_phi[:,1:knyq]**2-3.,clevs,extend='both',cmap=plt.cm.RdBu)
plt.xlabel(r'$k/m$')
plt.ylabel(r'$mt$')
cb=plt.colorbar()
cb.set_label(r'$\kappa_4$')
plt.show()

plt.contourf(kvals_chi[0,1:knyq],tvals_chi,r4_chi[:,1:knyq]/r2_chi[:,1:knyq]**2-3.,clevs,extend='both',cmap=plt.cm.RdBu)
plt.xlabel(r'$k/m$')
plt.ylabel(r'$mt$')
cb=plt.colorbar()
cb.set_label(r'$\kappa_4$')
plt.show()
