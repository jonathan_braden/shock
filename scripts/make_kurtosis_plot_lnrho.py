import numpy as np
import matplotlib.pyplot as plt

# This is for lnrho
basedir = "/mnt/scratch-lustre/jbraden/four_moms/m2g2/N1024/L10/kc2_full/"
momfile=basedir+"FOURMOMS"
logfile=basedir+"LOG.out"

nlat=1024
knyq=nlat/2

b=np.genfromtxt(logfile,usecols=[0,1])
tvals=b[:,0]
avals=b[:,1]

numt=len(tvals)
datacols=[0,1,11,13,2,4,6,8]
a=np.genfromtxt(momfile,usecols=datacols)

times=np.reshape(a[:,0],(numt,-1))
kvals=np.reshape(a[:,1],(numt,-1))
f2=np.reshape(a[:,2],(numt,-1))
f4=np.reshape(a[:,3],(numt,-1))

r1=np.reshape(a[:,4],(numt,-1))
r2=np.reshape(a[:,5],(numt,-1))
r3=np.reshape(a[:,6],(numt,-1))
r4=np.reshape(a[:,7],(numt,-1))

mycmap=plt.cm.OrRd
for i in range(len(tvals)):
    plt.plot(kvals[i][0:knyq],f4[i][0:knyq]/f2[i][0:knyq]**2-2.,color=mycmap(i))
plt.xlabel(r'$k/m$',fontsize=24)
plt.ylabel(r'$\frac{\langle|f_k|^4\rangle}{\langle|f_k|^2\rangle^2}-2$',fontsize=24)
plt.show()

mycmap=plt.cm.OrRd
for i in range(len(tvals)):
    plt.plot(kvals[i][0:knyq],r4[i][0:knyq]/r2[i][0:knyq]**2-3.,color=mycmap(i))
plt.xlabel(r'$k/m$',fontsize=24)
plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=24)
plt.show()

mycmap=plt.cm.OrRd
for i in range(len(tvals)):
    plt.plot(kvals[i][0:knyq],r3[i][0:knyq]/r2[i][0:knyq]**1.5,color=mycmap(i))
plt.xlabel(r'$k/m$',fontsize=24)
plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=24)
plt.show()

# To do, put in ubiased estimators here

plt.contourf(times[:,0:knyq],kvals[:,0:knyq],r4[:,0:knyq]/r2[:,0:knyq]**2-3.,cmap=plt.cm.OrRd)
