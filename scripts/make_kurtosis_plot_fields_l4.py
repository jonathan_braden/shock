import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt

basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/"
momfile_phi=basedir+"phi_fourmoms/kc2_l20/FOURMOMS"
momfile_chi=basedir+"dphi_fourmoms/kc2_l20/FOURMOMS"
logfile_phi=basedir+"phi_fourmoms/kc2_l20/LOG.out"
logfile_chi=basedir+"dphi_fourmoms/kc2_l20/LOG.out"

nlat=512
knyq=nlat/2

c1=np.genfromtxt(logfile_phi,usecols=[0,1])
c2=np.genfromtxt(logfile_chi,usecols=[0,1])
tvals_phi=c1[:,0]
avals_phi=c1[:,1]
tvals_chi=c2[:,0]
avals_chi=c2[:,1]

numt1=len(c1[:,0])
numt2=len(c2[:,0])

datacols=[0,1,4,5,8,9]
a=np.genfromtxt(momfile_phi,usecols=datacols)
b=np.genfromtxt(momfile_chi,usecols=datacols)

numk_phi=np.reshape(a[:,0],(numt1,-1))
kvals_phi=np.reshape(a[:,1],(numt1,-1))
numk_chi=np.reshape(b[:,0],(numt2,-1))
kvals_chi=np.reshape(b[:,1],(numt2,-1))

r2_phi=np.reshape(a[:,2],(numt1,-1))
r4_phi=np.reshape(a[:,4],(numt1,-1))
i2_phi=np.reshape(a[:,3],(numt1,-1))
i4_phi=np.reshape(a[:,5],(numt1,-1))

r2_phi=0.5*(r2_phi+i2_phi)
r4_phi=0.5*(r4_phi+i4_phi)

r2_chi=np.reshape(b[:,2],(numt2,-1))
r4_chi=np.reshape(b[:,4],(numt2,-1))
i2_chi=np.reshape(b[:,3],(numt2,-1))
i4_chi=np.reshape(b[:,5],(numt2,-1))

r2_chi=0.5*(r2_chi+i2_chi)
r4_chi=0.5*(r4_chi+i4_chi)

mycmap=plt.cm.OrRd
tplots=[500,600,700,800,900,4000]
for i in tplots:
    plt.plot(kvals_phi[i][1:knyq],r4_phi[i][1:knyq]/r2_phi[i][1:knyq]**2-3.,'b',label=r'$a\phi$')
    plt.plot(kvals_chi[i][1:knyq],r4_chi[i][1:knyq]/r2_chi[i][1:knyq]**2-3.,'r--',label=r'$a\partial_\tau\phi$')
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$')
#    plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=20)
    plt.ylabel(r'$\kappa_4$')
    plt.xlim(kvals_phi[0][1],kvals_phi[0][knyq])
    plt.ylim(-1.,5.)
    plt.text(0.95,0.04,r'$\sqrt{\lambda}M_P\tau='+str(tvals_phi[i])+'$',horizontalalignment='right',verticalalignment='bottom',transform=plt.gca().transAxes)
    plt.legend(bbox_to_anchor=(0,0,1.,1.),loc='upper right',bbox_transform=plt.gcf().transFigure)
    plt.savefig('kurtosis_fields_l4_t{:.1f}.pdf'.format(tvals_phi[i]))
    plt.show()

# To do, put in ubiased estimators here and also combine low kmodes
def bin_lowk():
    return

clevs=np.linspace(0.,2.,21)
plt.contourf(kvals_phi[0,1:knyq],tvals_phi,r4_phi[:,1:knyq]/r2_phi[:,1:knyq]**2-3.,clevs,extend='both',cmap=plt.cm.Greens)
plt.xlabel(r'$k/\sqrt{\lambda}M_P$',fontsize=20)
plt.ylabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=20)
cb=plt.colorbar()
cb.set_label(r'$\kappa_4$',fontsize=20)
plt.show()

plt.contourf(kvals_chi[0,1:knyq],tvals_chi,r4_chi[:,1:knyq]/r2_chi[:,1:knyq]**2-3.,clevs,extend='both',cmap=plt.cm.Greens)
plt.xlabel(r'$k/\sqrt{\lambda}M_P$',fontsize=20)
plt.ylabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=20)
cb=plt.colorbar()
cb.set_label(r'$\kappa_4$',fontsize=20)
plt.show()
