#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

def read_cdf_data(infile, cdfcols, logfile, logcols):
    a=np.genfromtxt(infile,usecols=cdfcols)
    b=np.genfromtxt(logfile,usecols=logcols)

    numt = len(b[:,0])

    cdf=[]
    for i in range( 2,len(a[0]) ):
        cdf.append( np.reshape(a[:,i],(numt,-1)) )
    return cdf

def compute_pdf(cdf):
    return


