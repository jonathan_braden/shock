import numpy as np
import matplotlib.pyplot as plt

# This is for lnrho
#basedir = "/mnt/scratch-lustre/jbraden/entropy_calcs/m2g2/four_moms/N512/10_bins/L10/kc1_a2.5/"
basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/"
momfile_phi=basedir+"kc2_phi_fourdist/FOURMOMS"
momfile_chi=basedir+"kc2_chi_fourdist/FOURMOMS"
logfile=basedir+"kc2_phi_fourdist/LOG.out"

nlat=512
knyq=nlat/2

b=np.genfromtxt(logfile,usecols=[0,1])
tvals=b[:,0]
avals=b[:,1]

numt=len(tvals)
datacols=[0,1,4,5,8,9]
a=np.genfromtxt(momfile_phi,usecols=datacols)

numk=np.reshape(a[:,0],(numt,-1))
kvals=np.reshape(a[:,1],(numt,-1))
f2=np.reshape(a[:,2],(numt,-1))
f4=np.reshape(a[:,3],(numt,-1))

r2=np.reshape(a[:,2],(numt,-1))
r4=np.reshape(a[:,4],(numt,-1))
i2=np.reshape(a[:,3],(numt,-1))
i4=np.reshape(a[:,5],(numt,-1))

k4=r4/r2**2

#mycmap=plt.cm.OrRd
#for i in range(len(tvals)):
#    plt.plot(kvals[i][0:knyq],f4[i][0:knyq]/f2[i][0:knyq]**2-2.,color=mycmap(i))
#plt.xlabel(r'$k/m$',fontsize=24)
#plt.ylabel(r'$\frac{\langle|f_k|^4\rangle}{\langle|f_k|^2\rangle^2}-2$',fontsize=24)
#plt.show()

mycmap=plt.cm.OrRd
for i in range(len(tvals)):
    plt.plot(kvals[i][0:knyq],r4[i][0:knyq]/r2[i][0:knyq]**2-3.,color=mycmap(i))
plt.xlabel(r'$k/m$',fontsize=24)
plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=24)
plt.show()

#mycmap=plt.cm.OrRd
#for i in range(len(tvals)):
#    plt.plot(kvals[i][0:knyq],r3[i][0:knyq]/r2[i][0:knyq]**1.5,color=mycmap(i))
#plt.xlabel(r'$k/m$',fontsize=24)
#plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=24)
#plt.show()

# To do, put in ubiased estimators here

plt.contourf(times[:,0:knyq],kvals[:,0:knyq],r4[:,0:knyq]/r2[:,0:knyq]**2-3.,cmap=plt.cm.OrRd)
