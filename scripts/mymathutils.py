#!/usr/bin/python
# Filename: mymathutils.py

import numpy as np

#
# A collection of useful mathematical subroutines in PYTHON
# To do: Write some smoothing filters, etc. here
#

def smooth(x,window_len=11,window='hanning', kaiserpar=16.):  # check what = signs do
    if x.ndim !=1:
        raise ValueError, "smooth only accepts 1-d arrays"
    if x.size < window_len:
        raise ValueError, "Cannot smooth data shorter than the window size"
    if window_len<3:
        return x
    if not window in ['flat','hanning','hamming','bartlett','blackman','kaiser']:
        raise ValueError, "Window is not of a recognized type"

# This is the correct choice for an odd sized window
# Is it correct for an even window?
    win_pad = window_len/2 + 1
    s=np.r_[x[win_pad-1:0:-1],x,x[-1:-win_pad:-1]]

    if window == 'flat':  #moving average
        w=np.ones(window_len,'d')
    elif window == 'kaiser':
        w=eval('np.'+window+'(window_len, kaiserpar)')
    else:
        w=eval('np.'+window+'(window_len)')

    y=np.convolve(w/w.sum(),s,mode='valid')
    return y


def savitzky_golay(y, window_size, order, deriv=0):
    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be integers")

    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size must be positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for polynomial order")

    order_range = range(order+1)
    half_window = (window_size - 1) // 2
# precompute coeffs
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv]
# pad the signal, using mirror- images
    firstvals = y[0] - np.abs(y[1:half_window+1][::-1] - y[0])
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))

    return np.convolve( m, y, mode='valid')

def derivative(time, data):
    deriv=[]
    deriv.append((data[1]-data[0])/(time[1]-time[0]))
    for i in range(1,len(data)-1):
        deriv.append((data[i+1]-data[i-1])/(time[i+1]-time[i-1]))

    last = len(data)
    deriv.append((data[last-1]-data[last-2])/(time[last-1]-time[last-2]))

    return deriv
