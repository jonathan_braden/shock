#!/usr/bin/python

# Plot the spectra for the fields and cross-correlators individually
# as well as the resulting determinant to go into the entropy

# To do (Jan. 29, 2014) - pass in a dictionary or something to set up the determinants instead of hard coding the various indices

import numpy as np
import matplotlib.pyplot as plt

def plot_components(itime):
    for i in range(len(spectra)):
        plt.plot( kvals[itime],np.abs(spectra[i][itime]) )
    plt.xscale('log')
    plt.yscale('log')
    return

#
# Plot the cross-correlators normalized to the fluctuations in the fields
#
def plot_crosscorrelations_normalized(itime,time,legend):
    plt.plot( kvals[itime], spectra[ind_dict['phiPi']][itime]/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$\Delta_{\ln\rho,\partial_t\ln\rho}$')

    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1,1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$\Delta_{ij}$')

    tlabel=r'$mt='+str(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel,fontsize=20)  # adjust as needed depending on choice of scales
    if (legend):
        plt.legend()
    return

def plot_determinants(itime):
    plt.plot( kvals[itime], np.log(nphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}})$')
    plt.plot( kvals[itime], np.log(detphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}} - P_{\phi\dot{\phi}}^2)$')

    plt.legend(loc='upper right')
    plt.xscale('log')
    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    return

def plot_determinants_normed(itime,tnorm,legend):
    plt.plot( kvals[itime], nphi[itime]/nphi[tnorm], 'b', label=r'$P_{\ln(\rho)\ln(\rho)}P_{\partial_t\ln(\rho)\partial_t\ln(\rho)}$')
    plt.plot( kvals[itime], detphi[itime]/detphi[tnorm], 'r--',markersize=5., label=r'$\Delta_{\ln(\rho)\partial_t\ln(\rho)}$')

    if (legend):
        plt.legend()

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$k/m$',fontsize=20)
    plt.ylabel(r'$F(k,t)/F(k,t=0)$',fontsize=20)
    return

def compute_determinants():
    global nphi
    nphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]
    global detphi
    detphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]-spectra[ind_dict['phiPi']]**2
                
def compute_crossspec_norm():
    global phi_phidot
    phi_phidot = spectra[ind_dict['phiPi']]/spectra[ind_dict['phi']]**0.5/spectra[ind_dict['Pi']]**0.5 

#To do: add dk part
def compute_entropies(kcut):
    global s_phi, ds_phi
    s_phi=[]
    global s_detphi, ds_detphi
    s_detphi=[]
    global s_lnrho,s_dlnrho
    s_lnrho=[]
    s_dlnrho=[]

    dk = kvals[0,1]-kvals[0,0]

    for kmax in kcut:
        lnr_det=spectra[ind_dict['phi']]
        s_lnrho.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(lnr_det[:,1:kmax]),axis=1) )
        dlnr_det=spectra[ind_dict['Pi']]
        s_dlnrho.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(dlnr_det[:,1:kmax]),axis=1) )
        s_phi.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(nphi[:,1:kmax]),axis=1) )
        s_detphi.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(detphi[:,1:kmax]),axis=1) )
 
    s_phi=np.array(s_phi)
    ds_phi=np.diff(s_phi,axis=1)
    s_detphi=np.array(s_detphi)
    ds_detphi=np.diff(s_detphi,axis=1)
    s_lnrho=np.array(s_lnrho)

    return

def plot_entropy_differences(kind):
#    plt.plot( ,s_phi[kind]-s_detphi[kind],label=r'$S_{n_{\phi}}-S_{\Delta_{\phi}}$')
#    plt.plot( ,s_chi[kind]-s_detchi[kind],label=r'$S_{n_{\chi}}-S_{\Delta_{\chi}}$')
#    plt.plot( ,s_detphi[kind]+s_detchi[kind]-s_dettot,label=r'$S_{\Delta_{\phi}}+S_{\Delta_{\chi}} - S_{\Delta_{tot}}$')
    
    plt.yscale('log')
    plt.xlim(0.,500.)
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$\Delta S$')

    tlabel=r'$\frac{k_{cut}}{m}='+str(kvals[kcuts[kind]])+'$'
#    plt.text(,0.75,tlabel,fontsize=20)

def compute_crossentropies(kcut):
    global s_cross
    
    s_cross=[]
    for kmax in kcut:
        s_tmp=[]
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_phidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-chi_chidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_chi[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_chidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phidot_chi[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1-phidot_chidot[:,1:kmax]**2),axis=1) )
        s_cross.append(s_tmp)
        
    return

# User modified specifications for the files to make the graph
#basedir='/mnt/scratch-lustre/jbraden/crossspec/l4/n512/kc2_nomean/'
#basedir='/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/rho/L10/kc2_lnrho_derivs/'
basedir='/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_lnr_dlnr_cross/'
infile=basedir+'PSD'
logfile=basedir+'LOG.out'

ind_dict={'phi':0,
          'Pi':1,
          'phiPi':2,
          'phiPi_im':3}

datacols=[3,4,5,6,7]
nlat=512

numk=int(3.**0.5*nlat/2)+2  # don't touch this
a=np.genfromtxt(infile,usecols=datacols)

kvals=np.reshape(a[:,0],(-1,numk))

spectra=[]
for i in range(1,len(a[0])):
    spectra.append( np.reshape(a[:,i],(-1,numk)) )
spectra=np.array(spectra)

# Edit this stuff to get the correct results
b=np.genfromtxt(logfile,usecols=[0,1,3,4,12,13,14,15])
times=b[:,0]
afac=b[:,1]
rho_ave=b[:,2]
prs_ave=b[:,3]
# Account for having 2 fields (edit for single-field model or more than 2 fields)
phi_ave=b[:,4]
chi_ave=b[:,5]
piphi_ave=b[:,6]
pichi_ave=b[:,7]
w_ave=prs_ave/rho_ave

# Edit this (this one is for rho/pressure as variables?)
jacobian = 2.*np.abs(phi_ave**3*piphi_ave)/(afac**4*rho_ave**2*(1.+w_ave))

compute_determinants()

kcuts=[63,127,191,255]
compute_entropies(kcuts)

kvol=[]
s_noncanonical=[]
dk = kvals[0][1]-kvals[0][0]
for i in range(len(kcuts)):
    kvol.append(dk*np.sum(kvals[0][0:kcuts[i]]**2))
#    s_noncanonical.append(s_detphi[i]-kvol[i]*np.log(jacobian))


# Now make all of the desired plots (edit this as needed)
plot_determinants_normed(100,0,True)
plot_times=[100,150,175,200,225,250,275]
for tcur in plot_times:
    plot_determinants_normed(tcur,0,False)
plt.show()

plot_crosscorrelations_normalized(0,times[0],True)
#for i in range(0,600,50):
for i in range(0,300,25):
    plot_crosscorrelations_normalized(i,times[i],False)
plt.show()

# A contour plot of the above would be nice to show the oscillations
kcut_plot=nlat/2
plt.contourf(kvals[0][1:kcut_plot],times,spectra[2][:,1:kcut_plot]/spectra[1][:,1:kcut_plot]**0.5/spectra[0][:,1:kcut_plot]**0.5,30,cmap=plt.cm.RdBu)
plt.xlabel(r'$k/m$',fontsize=20)
plt.ylabel(r'$mt$',fontsize=20)
cbar=plt.colorbar()
cbar.set_label(r'$P_{\ln\rho,\partial_t\ln\rho}/\sqrt{P_{\ln\rho,\ln\rho}P_{\partial_t\ln\rho,\partial_t\ln\rho}}$',fontsize=20)
plt.show()

tcut_plot=len(times)
plt.contourf(kvals[0][1:kcut_plot],times[0:tcut_plot],kvals[0:tcut_plot,1:kcut_plot]**2*np.log(detphi[0:tcut_plot,1:kcut_plot]),30,cmap=plt.cm.OrRd)
plt.xlabel(r'$k/m$',fontsize=20)
plt.ylabel(r'$mt$',fontsize=20)
