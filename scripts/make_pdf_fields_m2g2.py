#!/usr/bin/env python

# Take the data stored in my CDF file and compute the resulting PDFs
# Inputs : infile - file storing the CDF
#        : datacols - columns with appropriate data, first column is the percentiles, others are CDF values at that percentile
#        : nlat - number of sampling sites for CDF

# To do : for ease of use make a dictionary associating variables with columns

paper_width=5.95

import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
from scipy.optimize import curve_fit
from matplotlib.ticker import MaxNLocator
from matplotlib import rcParams

savePlots=False

def fitGauss(t,a,b):
    return np.exp(-a*(t-b)**2)

basedir="/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/fields/L10/kc2_nomean/"
infile=basedir+"CDF"
logfile=basedir+"LOG.out"
datacols=[0,1,2,3,4,5]
nlat=513 #129  #257

a=np.genfromtxt(infile,usecols=datacols)
b=np.genfromtxt(logfile,usecols=[0,1])

times=np.reshape(a[:,0],(-1,nlat))
times=times[:,0:(nlat-1)] # resize since I differentiate the CDF
percentiles=np.reshape(a[:,1],(-1,nlat))

cdf=[]
for i in range( 2,len(a[0]) ):
    cdf.append(np.reshape(a[:,i],(-1,nlat)))

pdf=[]
vals=[]
# Now create the PDFs
for i in range(len(cdf)):
    size=len(cdf[i])
    pdf.append(1./np.diff(cdf[i],axis=1))
    vals.append( 0.5*(cdf[i][:,1:nlat]+cdf[i][:,0:(nlat-1)]) )

pdfnorm=[]
for v in range(len(pdf)):
    tmp=pdf[v]
    for x in range(len(tmp)):
        tmp[x] /= np.max(tmp[x])
    pdfnorm.append(tmp)

# check the factors of a in here
def plot_pdfs(tindex,doFit=False,sigma=[1.,1.]):
    plt.plot(vals[0][tindex],pdf[0][tindex],'bo',label=r'$a^{3/2}\delta\phi$',markeredgecolor='b')
    plt.plot(vals[1][tindex],pdf[1][tindex],'r^',label=r'$a^{3/2}\delta\chi$',markeredgecolor='r')
    plt.plot(vals[2][tindex],pdf[2][tindex],'g:',label=r'$a^{3/2}\delta\dot{\phi}$')
    plt.plot(vals[3][tindex],pdf[3][tindex],'k-.',label=r'$a^{3/2}\delta\dot{\chi}$')

    if doFit:
        x=vals[0][tindex][5:nlat-6]
        y=pdf[0][tindex][5:nlat-6]
        pFit, cFit = curve_fit(fitGauss,x,y,p0=(sigma[0],0.))
        plt.plot(vals[0][tindex],fitGauss(vals[0][tindex],pFit[0],pFit[1]),'b',label=r'$\phi$ Fit')

        x=vals[1][tindex][5:nlat-6]
        y=pdf[1][tindex][5:nlat-6]
        pFit, cFit = curve_fit(fitGauss,x,y,p0=(sigma[1],0.))
        plt.plot(vals[1][tindex],fitGauss(vals[1][tindex],pFit[0],pFit[1]),'r',label=r'$\chi$ Fit')

    plt.legend(bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,loc='upper right',borderaxespad=0.25)
    plt.ylabel(r'$P/P_{max}$')
    plt.ylim(0,1.2)
    plt.gca().set_yticks([0,0.25,0.5,0.75,1])
    plt.gca().xaxis.set_major_locator(MaxNLocator(5))
    plt.text(0.05,0.95,r'$mt='+str(times[time][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

rcParams['figure.figsize'] = 2.86, 1.53
rcParams['figure.subplot.bottom'] = 0.15
rcParams['figure.subplot.top'] = 0.9
rcParams['figure.subplot.left'] = 0.2
rcParams['figure.subplot.right'] = 0.85
plt.figure()
plt.clf()

time=400
plot_pdfs(time,True,[1000.,1.])
plt.xlim(-0.1,0.1)
plt.savefig('fields_pdf_t{:.1f}.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()
plt.clf()

time=440
plot_pdfs(time,True,[1000.,1.])
plt.xlim(-0.5,0.5)
plt.savefig('fields_pdf_t{:.1f}.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()
plt.clf()

time=480
plot_pdfs(time,True)
plt.xlim(-2,2)
plt.savefig('fields_pdf_t{:.1f}.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()
plt.clf()

time=490
plot_pdfs(time,True)
plt.xlim(-2,2)
plt.savefig('fields_pdf_t{:.1f}.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()
plt.clf()

time=500
plot_pdfs(time,doFit=True)
plt.xlim(-2,2)
plt.savefig('fields_pdf_t{:.1f}.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()
plt.clf()

time=600
plot_pdfs(time,doFit=True)
plt.xlim(-3.,3.)
plt.savefig('fields_pdf_t{:.1f}.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()    
plt.clf()

time=2000
plot_pdfs(time,doFit=True)
plt.xlim(-3.,3.)
plt.savefig('fields_pdf_t{:.1f}.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()
plt.clf()

# Make contour plots
rcParams['figure.figsize'] = 2.86, 1.92
rcParams['figure.subplot.bottom'] = 0.23
rcParams['figure.subplot.top'] = 0.92
rcParams['figure.subplot.left'] = 0.17
rcParams['figure.subplot.right'] = 0.92
plt.figure()
plt.clf()
label_loc=-0.15

tcut=2000
c=plt.contourf(times[0:tcut],vals[0][0:tcut],pdf[0][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.xlabel(r'$mt$')
plt.ylabel(r'$a^{3/2}\delta\phi$')
plt.ylim(-1.,1.)
plt.gca().yaxis.set_label_coords(label_loc,0.5)
plt.xlim(0,500)
cbar=plt.colorbar(ticks=[0,0.5,1],pad=0.02)
cbar.set_label(r'$P(a^{3/2}\delta\phi)/P_{max}$')
cbar.solids.set_rasterized(True)
plt.savefig('phi_pdf_tevolve.pdf')
if showPreview:
    plt.show()
plt.clf()

tcut=2000
c=plt.contourf(times[0:tcut],vals[1][0:tcut],pdf[1][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.xlabel(r'$mt$')
plt.ylabel(r'$a^{3/2}\delta\chi$')
plt.ylim(-2.5,2.5)
plt.gca().yaxis.set_label_coords(label_loc,0.5)
plt.xlim(0,500)
cbar=plt.colorbar(ticks=[0,0.5,1],pad=0.02)
cbar.set_label(r'$P(a^{3/2}\delta\chi)/P_{max}$')
cbar.solids.set_rasterized(True)
plt.savefig('chi_pdf_tevolve.pdf')
if showPreview:
    plt.show()
plt.clf()

tcut=2000
c=plt.contourf(times[0:tcut],vals[2][0:tcut],pdf[2][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.xlabel(r'$mt$')
plt.ylabel(r'$a^{3/2}\delta\dot{\phi}$')
plt.gca().yaxis.set_label_coords(label_loc,0.5)
plt.ylim(-1,1)
plt.xlim(0,500)
cbar=plt.colorbar(ticks=[0,0.5,1],pad=0.02)
cbar.set_label(r'$P(a^{3/2}\delta\dot{\phi})/P_{max}$')
cbar.solids.set_rasterized(True)
plt.savefig('phidot_pdf_tevolve.pdf')
if showPreview:
    plt.show()
plt.clf()

tcut=2000
c=plt.contourf(times[0:tcut],vals[3][0:tcut],pdf[3][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.xlabel(r'$mt$')
plt.ylabel(r'$a^{3/2}\delta\dot{\chi}$')
plt.gca().yaxis.set_label_coords(label_loc,0.5)
plt.ylim(-1,1)
plt.xlim(0,500)
cbar=plt.colorbar(ticks=[0,0.5,1],pad=0.02)
cbar.set_label(r'$P(a^{3/2}\delta\dot{\chi})/P_{max}$')
cbar.solids.set_rasterized(True)
plt.savefig('chidot_pdf_tevolve.pdf')
if showPreview:
    plt.show()
plt.clf()

#plt.plot(vals[var][time],pdf[var][time]/np.max(pdf[var][time]))
