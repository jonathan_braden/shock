#!/usr/bin/python

# Plot the spectra for the fields and cross-correlators individually
# as well as the resulting determinant to go into the entropy

# To do (Jan. 29, 2014) - pass in a dictionary or something to set up the determinants instead of hard coding the various indices

import numpy as np
import matplotlib.pyplot as plt

def plot_components(itime):
    for i in range(len(spectra)):
        plt.plot( kvals[itime],np.abs(spectra[i][itime]) )
    plt.xscale('log')
    plt.yscale('log')
    return

#
# Plot the cross-correlators normalized to the fluctuations in the fields
#
def plot_crosscorrelations_normalized(itime,time):
    plt.plot( kvals[itime], spectra[4][itime]/spectra[0][itime]**0.5/spectra[2][itime]**0.5, label=r'$\Delta_{\phi\dot{\phi}}$')
    plt.plot( kvals[itime], spectra[5][itime]/spectra[1][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\chi\dot{\chi}}$')
    plt.plot( kvals[itime], spectra[6][itime]/spectra[0][itime]**0.5/spectra[1][itime]**0.5, label=r'$\Delta_{\phi\chi}$')
    plt.plot( kvals[itime], spectra[8][itime]/spectra[0][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\phi\dot{\chi}}$')
    plt.plot( kvals[itime], spectra[9][itime]/spectra[1][itime]**0.5/spectra[2][itime]**0.5, label=r'$\Delta_{\dot{\phi}\chi}$')
    plt.plot( kvals[itime], spectra[7][itime]/spectra[2][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\dot{\phi}\dot{\chi}}$')

    plt.xscale('linear')
    plt.yscale('linear')
    plt.ylim(-1,1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$\Delta_{ij}$')

    tlabel=r'$mt='+str(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel,fontsize=20)  # adjust as needed depending on choice of scales

    plt.legend()
    return

def plot_crosscorrelations_normalized_abs(itime,time):
    plt.plot( kvals[itime], np.abs(spectra[4][itime])/spectra[0][itime]**0.5/spectra[2][itime]**0.5, label=r'$\Delta_{\phi\dot{\phi}}$')
    plt.plot( kvals[itime], np.abs(spectra[5][itime])/spectra[1][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\chi\dot{\chi}}$')
    plt.plot( kvals[itime], np.abs(spectra[6][itime])/spectra[0][itime]**0.5/spectra[1][itime]**0.5, label=r'$\Delta_{\phi\chi}$')
    plt.plot( kvals[itime], np.abs(spectra[8][itime])/spectra[0][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\phi\dot{\chi}}$')
    plt.plot( kvals[itime], np.abs(spectra[9][itime])/spectra[1][itime]**0.5/spectra[2][itime]**0.5, label=r'$\Delta_{\dot{\phi}\chi}$')
    plt.plot( kvals[itime], np.abs(spectra[7][itime])/spectra[2][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\dot{\phi}\dot{\chi}}$')

    plt.xscale('linear')
    plt.yscale('linear')
    plt.ylim(-0.1,1.1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$\Delta_{ij}$')

    tlabel=r'$mt='+str(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel,fontsize=20)  # adjust as needed depending on choice of scales

    plt.legend()
    return


def plot_determinants(itime):
    plt.plot( kvals[itime], np.log(nphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}})$')
    plt.plot( kvals[itime], np.log(nchi[itime]), label=r'$\log(P_{\chi\chi}P_{\dot{\chi}\dot{\chi}})$')
    plt.plot( kvals[itime], np.log(detphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi} - P_{\phi\dot{\phi}}^2)$')
    plt.plot( kvals[itime], np.log(detchi[itime]), label=r'$\log(P_{\chi\chi}P_{\dot{\chi}\dot{\chi} - P_{\chi\dot{\chi}}^2)$')

    corr = [ [ spectra[0][itime], spectra[4][itime], spectra[6][itime], spectra[8][itime] ],
             [ spectra[4][itime], spectra[2][itime], spectra[9][itime], spectra[7][itime] ],
             [ spectra[6][itime], spectra[9][itime], spectra[1][itime], spectra[5][itime] ],
             [ spectra[8][itime], spectra[7][itime], spectra[5][itime], spectra[3][itime] ]
           ]
    corr=np.array(corr)
    npart=[]
    for i in range(len(corr[0][0])):
        npart.append(np.linalg.det(corr[:,:,i]))

    plt.plot( kvals[itime], np.log(npart), label=r'$n_k$' )
    plt.plot( kvals[itime], np.log(detphi[itime])+np.log(detchi[itime]), label='sum')

    plt.legend()
    return

def plot_determinants_normed(itime,tnorm,legend):
    plt.plot( kvals[itime], nphi[itime]/nphi[tnorm], 'b', label=r'$P_{\phi\phi}P_{\Pi_{\phi}\Pi_{\phi}}$')
    plt.plot( kvals[itime], nchi[itime]/nchi[tnorm], 'r', label=r'$P_{\chi\chi}P_{\Pi_{\chi}\Pi_{\chi}}$')
    plt.plot( kvals[itime], detphi[itime]/detphi[tnorm], 'b--',markersize=5., label=r'$\Delta_{\phi}$')
    plt.plot( kvals[itime], detchi[itime]/detchi[tnorm], 'r--',markersize=5., label=r'$\Delta_{\chi}$')
    plt.plot( kvals[itime], dettot[itime]/dettot[tnorm], 'g',label=r'$\Delta_{tot}$')

    if (legend):
        plt.legend(fontsize=20)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.ylim(ymin=0.1)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$k/m$',fontsize=20)
    plt.ylabel(r'$F(k,t)/F(k,t=0)$',fontsize=20)
    return

def compute_determinants():
    global nphi
    nphi=spectra[0]*spectra[2]
    global nchi
    nchi=spectra[1]*spectra[3]
    global detphi
    detphi=spectra[0]*spectra[2]-spectra[4]**2
    global detchi
    detchi=spectra[1]*spectra[3]-spectra[5]**2
    global dettot
    dettot=[]
    for tstep in range(len(spectra[0])):
        corr = [ [ spectra[0][tstep], spectra[4][tstep], spectra[6][tstep], spectra[8][tstep] ],
                 [ spectra[4][tstep], spectra[2][tstep], spectra[9][tstep], spectra[7][tstep] ],
                 [ spectra[6][tstep], spectra[9][tstep], spectra[1][tstep], spectra[5][tstep] ],
                 [ spectra[8][tstep], spectra[7][tstep], spectra[5][tstep], spectra[3][tstep] ] ]
        corr=np.array(corr)
        npart=[]
        for kstep in range(len(corr[0][0])):
            npart.append(np.linalg.det(corr[:,:,kstep]))
        dettot.append(npart)
    dettot=np.array(dettot)     
                
def compute_crossspec_norm():
    global phi_phidot
    phi_phidot = spectra[4]/spectra[0]**0.5/spectra[2]**0.5
    global chi_chidot
    chi_chidot = spectra[5]/spectra[1]**0.5/spectra[3]**0.5
    global phi_chi
    phi_chi = spectra[6]/spectra[0]**0.5/spectra[1]**0.5
    global phi_chidot
    phi_chidot = spectra[8]/spectra[0]**0.5/spectra[3]**0.5
    global phidot_chi
    phidot_chi = spectra[9]/spectra[1]**0.5/spectra[2]**0.5
    global phidot_chidot
    phidot_chidot = spectra[7]/spectra[2]**0.5/spectra[3]**0.5    

#To do: add dk part
def compute_entropies(kcut):
    global s_phi, ds_phi
    s_phi=[]
    global s_chi, ds_chi
    s_chi=[]
    global s_detphi, ds_detphi
    s_detphi=[]
    global s_detchi, ds_detchi
    s_detchi=[]
    global s_dettot, ds_dettot
    s_dettot=[]

    for kmax in kcut:
      s_phi.append( np.sum(kvals[:,1:kmax]**2*np.log(nphi[:,1:kmax]),axis=1) )
      s_chi.append( np.sum(kvals[:,1:kmax]**2*np.log(nchi[:,1:kmax]),axis=1) )
      s_detphi.append( np.sum(kvals[:,1:kmax]**2*np.log(detphi[:,1:kmax]),axis=1) )
      s_detchi.append( np.sum(kvals[:,1:kmax]**2*np.log(detchi[:,1:kmax]),axis=1) )
      s_dettot.append( np.sum(kvals[:,1:kmax]**2*np.log(dettot[:,1:kmax]),axis=1) )

    s_phi=np.array(s_phi)
    ds_phi=np.diff(s_phi,axis=1)
    s_chi=np.array(s_chi)
    ds_chi=np.diff(s_chi,axis=1)
    s_detphi=np.array(s_detphi)
    ds_detphi=np.diff(s_detphi,axis=1)
    s_detchi=np.array(s_detchi)
    ds_detchi=np.diff(s_detchi,axis=1)
    s_dettot=np.array(s_dettot)
    ds_dettot=np.diff(s_dettot,axis=1)
    
    return

def plot_entropy_differences(kind):
    plt.plot(times,s_phi[kind]-s_detphi[kind],label=r'$S_{n_{\phi}}-S_{\Delta_{\phi}}$')
    plt.plot(times,s_chi[kind]-s_detchi[kind],label=r'$S_{n_{\chi}}-S_{\Delta_{\chi}}$')
    plt.plot(times,s_detphi[kind]+s_detchi[kind]-s_dettot[kind],label=r'$S_{\Delta_{\phi}}+S_{\Delta_{\chi}} - S_{\Delta_{tot}}$')
    
    plt.yscale('log')
    plt.xlim(0.,500.)
    plt.xlabel(r'$mt$',fontsize=20)
    plt.ylabel(r'$\Delta S$',fontsize=20)
    plt.legend(fontsize=20)

    tlabel=r'$\frac{k_{cut}}{m}='+str(kvals[kcuts[kind]])+'$'
#    plt.text(,0.75,tlabel,fontsize=20)

def compute_crossentropies(kcut):
    global s_cross
    
    s_cross=[]
    for kmax in kcut:
        s_tmp=[]
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_phidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-chi_chidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_chi[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phi_chidot[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1.-phidot_chi[:,1:kmax]**2),axis=1) )
        s_tmp.append( np.sum(kvals[:,1:kmax]**2*np.log(1-phidot_chidot[:,1:kmax]**2),axis=1) )
        s_cross.append(s_tmp)        
    return

# User modified specifications for the files to make the graph
#basedir="/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/fields/L10/kc2_nomean/"
basedir="/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/fields/L5/kc2_nomean/"
infile=basedir+'PSD'
logfile=basedir+'LOG.out'
nlat = 512

datadict={
    'k':3 , 
    r'$|\phi_k|^2$':4, 
    r'$|\chi_k|^2$':5,
    r'$|\Pi^{\phi}_k|^2$':6,
    r'$|\Pi^{\chi}_k|^2$':7,
    r'$|\phi_k\Pi^{\phi}_{-k}|$':8,
    r'$|\chi_k\Pi^{\chi}_{-k}|$':10,
    r'$|\phi_k\chi_{-k}|$':12,
    r'$|\Pi^{\phi}_k\Pi^{\chi}_{-k}|$':14,
    r'$|\phi_k|\Pi^{\chi}_{-k}|$':16,
    r'$|\chi_k\Pi^{\phi}_{-k}|$':18
    }

datacols=datadict.values()
datacols.sort()
labels=datadict.keys()

numk=int(3.**0.5*nlat/2)+2  # don't touch this
a=np.genfromtxt(infile,usecols=datacols)
b=np.genfromtxt(logfile,usecols=[0,1])
times=b[:,0]
avals=b[:,1]

kvals=np.reshape(a[:,0],(-1,numk))

spectra=[]
for i in range(1,len(a[0])):
    spectra.append( np.reshape(a[:,i],(-1,numk)) )

compute_determinants()

kcuts=[63,127,191,255]
compute_entropies(kcuts)

# Now make all of the desired plots (edit this as needed)
plot_determinants_normed(200,0,True)
plt.ylim(ymax=1.e40)
plt.show()
plot_determinants_normed(400,0,True)
plt.ylim(ymax=1.e40)
plt.show()
plot_determinants_normed(410,0,True)
plt.ylim(ymax=1.e40)
plt.show()
plot_determinants_normed(420,0,True)
plt.ylim(ymax=1.e40)
plt.show()
plot_determinants_normed(440,0,True)
plt.show()
plot_determinants_normed(1200,0,True)
plt.ylim(ymax=1.e40)
plt.show()

tnorm=0
kmax=nlat/2
dk=kvals[0][1]-kvals[0][0]
for i in range(0,1200,20):
    plt.plot(kvals[i,1:kmax],(kvals[i][1:kmax]**2*dk/kvals[i][kmax]**3)*np.log(detphi[i,1:kmax]/detphi[tnorm,1:kmax]))
plt.xlim(kvals[0,1],kvals[0,kmax])
#plt.ylim(0.1,1.e20)
plt.xscale('log')
plt.yscale('log')
plt.xlabel(r'$k/m$',fontsize=24)
plt.ylabel(r'$\ln(P_{\phi\phi}P_{\Pi_\phi\Pi_\phi})$',fontsize=24)
plt.show()

#plt.contourf( , (kvals[i][1:kmax]**2*dk/kvals[:,1:kmax]**3)*np.log(nphi[:,1:kmax])
plt.contourf(kvals[0,1:kmax],times,kvals[:,1:kmax]**2*dk*np.log(detphi[:,1:kmax]/detphi[0,1:kmax]),cmap=plt.cm.OrRd)
plt.xlabel(r'$k/m$',fontsize=20)
plt.ylabel(r'$mt$',fontsize=20)
cb=plt.colorbar()
cb.set_label(r'$2\pi L^{-1}k^2\ln(\Delta_\phi/\Delta_\phi(t=0)$',fontsize=20)
plt.show()

plt.contourf(kvals[0,1:kmax],times,kvals[:,1:kmax]**2*dk*np.log(detchi[:,1:kmax]/detchi[0,1:kmax]),cmap=plt.cm.OrRd)
plt.xlabel(r'$k/m$',fontsize=20)
plt.ylabel(r'$mt$',fontsize=20)
cb=plt.colorbar()
cb.set_label(r'$2\pi L^{-1}k^2\ln(\Delta_\chi/\Delta_\chi(t=0)$',fontsize=20)
plt.show()

plt.contourf(kvals[0,1:kmax],times,kvals[:,1:kmax]**2*dk*np.log(dettot[:,1:kmax]/dettot[0,1:kmax]),cmap=plt.cm.OrRd)
plt.xlabel(r'$k/m$',fontsize=20)
plt.ylabel(r'$mt$',fontsize=20)
cb=plt.colorbar()
cb.set_label(r'$2\pi L^{-1}k^2\ln(\Delta_{tot}/\Delta_{tot}(t=0))$',fontsize=20)
plt.show()
# Plots of integrated entropy

# Plot contributions of off-diagonal terms (need to get times from log file still)
compute_crossspec_norm()
compute_crossentropies(kcuts)
kmax_ind=3
plt.plot(times,-s_cross[kmax_ind][0],label=r'$\Delta S_{\phi\dot{\phi}}$')
plt.plot(times,-s_cross[kmax_ind][1],label=r'$\Delta S_{\chi\dot{\chi}}$')
plt.plot(times,-s_cross[kmax_ind][2],label=r'$\Delta S_{\phi\chi}$')
plt.plot(times,-s_cross[kmax_ind][3],label=r'$\Delta S_{\phi\dot{\chi}}$')
plt.plot(times,-s_cross[kmax_ind][4],label=r'$\Delta S_{\dot{\phi}\chi}$')
plt.plot(times,-s_cross[kmax_ind][5],label=r'$\Delta S_{\dot{\phi}\dot{\chi}}$')
plt.ylabel(r'$-\Delta S$',fontsize=24)
plt.xlabel(r'$mt$',fontsize=24)
plt.yscale('log')
plt.legend(fontsize=20)
plt.show()

tplots=[100,150,200,250,300]
for tcur in tplots:
   plot_crosscorrelations_normalized_abs(tcur,times[tcur])
   plt.show()

#dt=0.03125
#for i in range(6400):
#    plot_crosscorrelations_normalized(i,i*dt)
#    figname="cross_%04d.png" % i
#    plt.savefig(figname)
#    plt.clf()

#j=3
#plt.plot(s_phi[j]-s_detphi[j],label=r'$\phi$')
#plt.plot(s_chi[j]-s_detchi[j],label=r'$\chi$')
#plt.plot(-s_dettot[j]+s_detphi[j]+s_detchi[j],label=r'$tot$')

#j=2
#plt.plot(s_detphi,label=r'$\phi$')
#plt.plot(s_detchi,label=r'$\chi$')
#plt.plot(s_dettot,label=r'$tot$')
