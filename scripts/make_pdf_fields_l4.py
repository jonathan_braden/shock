#!/usr/bin/env python

# Take the data stored in my CDF file and compute the resulting PDFs
# Inputs : infile - file storing the CDF
#        : datacols - columns with appropriate data, first column is the percentiles, others are CDF values at that percentile
#        : nlat - number of sampling sites for CDF

# To do : for ease of use make a dictionary associating variables with columns

import numpy as np
import matplotlib.pyplot as plt

basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/cross_spectra/kc2_lnrho_derivs_wcurdiff/"
infile=basedir+"CDF"
logfile=basedir+"LOG.out"
datacols=[0,1,2,3,4,5]
nlat=513 #129  #257

a=np.genfromtxt(infile,usecols=datacols)
b=np.genfromtxt(logfile,usecols=[1])
avals=b[:]

times=np.reshape(a[:,0],(-1,nlat))
times=times[:,0:(nlat-1)] # resize since I differentiate the CDF
agrid=np.ones( (len(times),len(times[0]) ) )
for i in range(len(agrid[0])):
    agrid[:,i]=avals
percentiles=np.reshape(a[:,1],(-1,nlat))

cdf=[]
for i in range( 2,len(a[0]) ):
    cdf.append(np.reshape(a[:,i],(-1,nlat)))

pdf=[]
vals=[]
# Now create the PDFs
for i in range(len(cdf)):
    size=len(cdf[i])
    pdf.append(1./np.diff(cdf[i],axis=1))
    vals.append( 0.5*(cdf[i][:,1:nlat]+cdf[i][:,0:(nlat-1)]) )

pdfnorm=[]
for v in range(len(pdf)):
    tmp=pdf[v]
    for x in range(len(tmp)):
        tmp[x] /= np.max(tmp[x])
    pdfnorm.append(tmp)

# check the factors of a in here
def plot_pdfs_rho(tindex):
    plt.plot(vals[0][tindex],pdf[0][tindex],'b',label=r'$\ln(\rho/\bar{\rho})$',linewidth=2)
    plt.plot(avals[tindex]**0.5*vals[1][tindex],pdf[1][tindex],'r--',label=r'$\sqrt{a}\partial_\tau\ln(\rho/\bar{\rho})$',linewidth=2)

    plt.legend()
    plt.ylabel(r'$P/P_{max}$',fontsize=20)

def plot_pdfs_fields(tindex):
    plt.plot(vals[2][tindex],pdf[2][tindex],'b-',label=r'$a\delta\phi$',linewidth=2)
    plt.plot(vals[3][tindex],pdf[3][tindex],'r--',label=r'$a\partial_\tau\delta\phi}$',linewidth=2)    
    plt.legend()
    plt.ylabel(r'$P/P_{max}$',fontsize=20)

time=500
plot_pdfs_fields(time)
plt.text(-0.04,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-0.05,0.05)
plt.show()

time=1000
plot_pdfs_fields(time)
plt.text(-9,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-10.,10.)
plt.show()

time=1500
plot_pdfs_fields(time)
plt.text(-9,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-10.,10.)
plt.show()    

time=4000
plot_pdfs_fields(time)
plt.text(-9,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-10.,10.)
plt.show()

time=500
plot_pdfs_rho(time)
plt.text(-0.018,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-0.02,0.02)
plt.show()

time=1000
plot_pdfs_rho(time)
plt.text(-2.6,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-3.,3.)
plt.show()

time=1500
plot_pdfs_rho(time)
plt.text(-2.3,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-2.5,2.5)
plt.show()    

time=4000
plot_pdfs_rho(time)
plt.text(-2.3,0.8,r'$\sqrt{\lambda}M_P\tau='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-2.5,2.5)
plt.show()

# Make contour plots
tcut=4000
plt.contourf(times[0:tcut],vals[0][0:tcut],pdf[0][0:tcut],cmap=plt.cm.OrRd)
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=20)
plt.ylabel(r'$(\ln(\rho/\bar{\rho})$',fontsize=20)
plt.ylim(-2.5,2.5)
cbar=plt.colorbar()
cbar.set_label(r'$P(\ln(\rho/\bar{\rho}))/P_{max}$',fontsize=20)
plt.show()

tcut=4000
plt.contourf(times[0:tcut],agrid[0:tcut]**0.5*vals[1][0:tcut],pdf[1][0:tcut],cmap=plt.cm.OrRd)
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=20)
plt.ylabel(r'$\sqrt{a}\partial_\tau\ln(\rho/\bar{\rho})$',fontsize=20)
plt.ylim(-2.,2.)
cbar=plt.colorbar()
cbar.set_label(r'$P(\sqrt{a}\partial_\tau\ln(\rho/\bar{\rho}))/P_{max}$',fontsize=20)
plt.show()

tcut=4000
plt.contourf(times[0:tcut],vals[2][0:tcut],pdf[2][0:tcut],cmap=plt.cm.OrRd)
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=20)
plt.ylabel(r'$a\delta\phi$',fontsize=20)
plt.ylim(-1.5,1.5)
cbar=plt.colorbar()
cbar.set_label(r'$P(a\delta\phi)/P_{max}$',fontsize=20)
plt.show()

tcut=4000
plt.contourf(times[0:tcut],vals[3][0:tcut],pdf[3][0:tcut],cmap=plt.cm.OrRd)
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=20)
plt.ylabel(r'$a\partial_\tau\delta\phi$',fontsize=20)
plt.ylim(-7.5,7.5)
cbar=plt.colorbar()
cbar.set_label(r'$P(a\partial_\tau\delta\phi)/P_{max}$',fontsize=20)
plt.show()

#plt.plot(vals[var][time],pdf[var][time]/np.max(pdf[var][time]))
