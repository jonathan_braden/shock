#!/usr/bin/python
# Filename : get_entropy.py

# To do : add switch for whether or not power spectrum has been logged

from myfileutils import *
import numpy as np

def get_entropy(psdfile, kcol, datcol, knyq, logged=True):
    datcols=[kcol, datcol]

    pspec=read_timeblock(psdfile, datcols)
    powthresh=1.e-10

    norm = 0.5
#    norm=3./(4.*np.pi)  # overall normalization of entropy

    ent=[]

    for i in range(len(pspec)):
        temp=0.
        for j in range(knyq):
#        for j in range(len(pspec[i][0])):
            if logged:
                temp = temp + j**2*pspec[i][1][j]
#                temp = temp + pspec[i][0][j]**2*pspec[i][1][j]
            else:
                temp = temp + j**2*np.log(pspec[i][1][j])
        if (i==0):
            ent0 = temp
        temp = temp - ent0    
        ent.append(temp)

    return (norm*np.array(ent)/knyq**3)    

def smooth_spectrum(psdfile, kcol, datcol, knyq, logged=False):
    datcols=[kcol, datcol]
    pspec = np.genfromtxt(psdfile,usecols=datcols)
    
    return

def get_entropy_kscan(psdfile, kcol, datcol, kmin, kmax, kstep, logged=False):
    datcols=[kcol, datcol]

    pspec = read_timeblock(psdfile, datcols)
    norm = 3./(4.*np.pi)

    print len(pspec)
    print len(pspec[0])
    print len(pspec[0][0])

    values=[]
    for k in range(kmin,kmax,kstep):
        ent=[]
        for i in range(len(pspec)):
            temp = 0.
            for j in range(k):
                if logged:
                    temp = temp + j**2*pspec[i][1][j]
#                    temp = temp + pspec[i][0][j]**2*pspec[i][1][j]
                else:
                    temp = temp + j**2*np.log(pspec[i][1][j])
#                    temp = temp + pspec[i][0][j]**2*np.log(pspec[i][1][j])

            if (i != 0):
                temp = (temp - ent[0])
            if (i == 0):
                temp = 0.
                   
            ent.append(temp)

        values.append(norm*np.array(ent)/k**3)

    return values
