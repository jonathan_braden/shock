#!/usr/bin/python

# Plot the spectra for the fields and cross-correlators individually
# as well as the resulting determinant to go into the entropy

# To do (Jan. 29, 2014) - pass in a dictionary or something to set up the determinants instead of hard coding the various indices

import numpy as np
import matplotlib.pyplot as plt

def plot_components(itime):
    for i in range(len(spectra)):
        plt.plot( kvals[itime],np.abs(spectra[i][itime]) )
    plt.xscale('log')
    plt.yscale('log')
    return

#
# Plot the cross-correlators normalized to the fluctuations in the fields
#
def plot_crosscorrelations_normalized_fields(itime,time):
    plt.plot( kvals[itime], spectra[ind_dict['phiPi']][itime]/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$\Delta_{\phi\dot{\phi}}$')

    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1,1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$\Delta_{ij}$')

    tlabel=r'$mt='+str(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel,fontsize=20)  # adjust as needed depending on choice of scales

    plt.legend()
    return

def plot_crosscorrelations_normalized_lnrho(itime,time):
    plt.plot( kvals[itime], spectra[ind_dict['lnrlnp']][itime]/spectra[ind_dict['lnrho']][itime]**0.5/spectra[ind_dict['lnp']][itime]**0.5, label=r'$\Delta_{\ln\rho,\partial_t\ln\rho}$')
    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1.2,1.2)
    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$')
    plt.ylabel(r'$\Delta_{ij}$')

    plt.legend()

def plot_determinants(itime):
    plt.plot( kvals[itime], np.log(nphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}})$')
    plt.plot( kvals[itime], np.log(detphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}} - P_{\phi\dot{\phi}}^2)$')

    plt.legend()
    return

def plot_determinants_normed(itime,tnorm):
    plt.plot( kvals[itime], nphi[itime]/nphi[tnorm], 'b', label=r'$P_{\phi\phi}P_{\Pi_{\phi}\Pi_{\phi}}$')
    plt.plot( kvals[itime], detphi[itime]/detphi[tnorm], 'b--',markersize=5., label=r'$\Delta_{\phi}$')
    plt.plot( kvals[itime], nlnrho[itime]/nlnrho[tnorm], 'r',label=r'$n_{\ln\rho}$')
    plt.plot( kvals[itime], detlnrho[itime]/detlnrho[tnorm], 'r--', label=r'$\Delta_{\ln\rho}$')

    plt.legend(fontsize=20)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$',fontsize=20)
    plt.ylabel(r'$F(k,t)/F(k,t=0)$',fontsize=20)
    return

def compute_determinants():
    global nphi
    nphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]
    global detphi
    detphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]-spectra[ind_dict['phiPi']]**2

    global nlnrho
    nlnrho=spectra[ind_dict['lnrho']]*spectra[ind_dict['lnp']]
    global detlnrho
    detlnrho=spectra[ind_dict['lnrho']]*spectra[ind_dict['lnp']]-spectra[ind_dict['lnrlnp']]**2
                
def compute_crossspec_norm():
    global phi_phidot
    phi_phidot = spectra[ind_dict['phiPi']]/spectra[ind_dict['phi']]**0.5/spectra[ind_dict['Pi']]**0.5 

    global lnr_dlnr
    lnr_dlnr = spectra[ind_dict['lnrlnp']]/spectra[ind_dict['lnrho']]**0.5/spectra[ind_dict['lnp']]**0.5

#To do: add dk part
def compute_entropies(kcut):
    global s_phi, ds_phi
    s_phi=[]
    global s_detphi, ds_detphi
    s_detphi=[]
    global s_lnr, s_dlnr
    s_lnr=[]
    s_dlnr=[]
    global s_detlnr
    s_detlnr=[]
    global s_detlnr_noncanon
    s_detlnr_noncanon=[]

    dk = kvals[0,1]-kvals[0,0]

    for kmax in kcut:
      s_phi.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(nphi[:,1:kmax]),axis=1) )
      s_detphi.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(detphi[:,1:kmax]),axis=1) )
      s_lnr.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(nlnrho[:,1:kmax]),axis=1) )
      s_detlnr.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(detlnrho[:,1:kmax]),axis=1) )
      s_detlnr_noncanon.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(detlnrho[:,1:kmax]/jacobian_linear[:,1:kmax]**2),axis=1) )
 
    s_phi=np.array(s_phi)
    ds_phi=np.diff(s_phi,axis=1)
    s_detphi=np.array(s_detphi)
    ds_detphi=np.diff(s_detphi,axis=1)

    s_lnr=np.array(s_lnr)
    s_detlnr=np.array(s_detlnr)
    s_detlnr_noncanon=np.array(s_detlnr_noncanon)

    return

def plot_entropy_differences(kind):
#    plt.plot( ,s_phi[kind]-s_detphi[kind],label=r'$S_{n_{\phi}}-S_{\Delta_{\phi}}$')
#    plt.plot( ,s_chi[kind]-s_detchi[kind],label=r'$S_{n_{\chi}}-S_{\Delta_{\chi}}$')
#    plt.plot( ,s_detphi[kind]+s_detchi[kind]-s_dettot,label=r'$S_{\Delta_{\phi}}+S_{\Delta_{\chi}} - S_{\Delta_{tot}}$')
    
    plt.yscale('log')
    plt.xlim(0.,500.)
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$\Delta S$')

    tlabel=r'$\frac{k_{cut}}{m}='+str(kvals[kcuts[kind]])+'$'
#    plt.text(,0.75,tlabel,fontsize=20)

# User modified specifications for the files to make the graph
basedir='/mnt/scratch-lustre/jbraden/entropy_paper/l4/cross_spectra/kc2_lnrho_derivs_wcurdiff/'
infile=basedir+'PSD'
logfile=basedir+'LOG.out'

ind_dict={'phi':3,
          'Pi':4,
          'phiPi':5,
          'lnrho':0,
          'lnp':1,
          'lnrlnp':2}

datacols=[3,4,5,6,8,9,10]
nlat=512

numk=int(3.**0.5*nlat/2)+2  # don't touch this
a=np.genfromtxt(infile,usecols=datacols)

kvals=np.reshape(a[:,0],(-1,numk))

spectra=[]
for i in range(1,len(a[0])):
    spectra.append( np.reshape(a[:,i],(-1,numk)) )

b=np.genfromtxt(logfile,usecols=[0,1,2,3,4,12,13])
times=b[:,0]
afac=b[:,1]
hubvals=b[:,2]
rho_ave=b[:,3]
prs_ave=b[:,4]
phi_ave=b[:,5]
piphi_ave=b[:,6]
w_ave=prs_ave/rho_ave

# Now I need to get the a-dependence
jacobian_norm_1 = piphi_ave**2/rho_ave**2/afac**2
jacobian_norm_2 = -6.*hubvals*piphi_ave*phi_ave**3/rho_ave**2
jacobian_norm_1 = jacobian_norm_1 / afac**3
jacobian_norm_2 = jacobian_norm_2 / afac**3
jacobian_linear=[]
for i in range(len(kvals)):
    jacobian_linear.append(kvals[i]**2*jacobian_norm_1[i]+jacobian_norm_2[i])
jacobian_linear=np.array(jacobian_linear)

compute_determinants()
compute_crossspec_norm()

kcuts=[63,127,191,255]
dk=kvals[0,1]-kvals[0,0]
def compute_jacobian_entropy(kc):
    s_jac=[]
    for kmax in kcuts:
        s_jac.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(jacobian_linear[:,1:kmax]**2),axis=1) )
    return s_jac
compute_entropies(kcuts)
s_jacobian = compute_jacobian_entropy(kcuts)

# Make the comparison with Jacobian
def plot_noncanonical_determinant(tcur,leg):
    knyq=nlat/2
    plt.plot(kvals[tcur,1:knyq],detlnrho[tcur,1:knyq]/(kvals[tcur,1:knyq]**2*jacobian_norm_1[tcur]+jacobian_norm_2[tcur])**2,'b',label=r'$\Delta_{\ln\rho}/\mathcal{J}^2$')
    plt.plot(kvals[tcur,1:knyq],detphi[tcur,1:knyq],'r--',label=r'$\Delta_\phi$')
    if (leg):
        plt.legend(fontsize=20,loc='upper right')
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$',fontsize=20)
    plt.ylabel(r'$det$',fontsize=20)
    plt.xlim(kvals[0,1],kvals[0,knyq])

def plot_noncanonical_determinant_full(tcur,leg):
    knyq=nlat/2
    plt.plot(kvals[tcur,1:knyq],detlnrho[tcur,1:knyq]/(kvals[tcur,1:knyq]**2*jacobian_norm_1[tcur]+jacobian_norm_2[tcur])**2,'b',label=r'$\Delta_{\ln\rho}/\mathcal{J}^2$')
    plt.plot(kvals[tcur,1:knyq],detphi[tcur,1:knyq],'r--',label=r'$\Delta_\phi$')
    plt.plot(kvals[tcur,1:knyq],jacobian_linear[tcur,1:knyq]**2,'g-.',label=r'$\mathcal{J}^2$')
    plt.plot(kvals[tcur,1:knyq],detlnrho[tcur,1:knyq],'m',label=r'$\Delta_{\ln\rho}$')
    if (leg):
        plt.legend(fontsize=20,loc='best')
    plt.yscale('log')
    plt.xscale('log')
    plt.xlabel(r'$k/\sqrt{\lambda}M_P$',fontsize=20)
    plt.ylabel(r'$det$',fontsize=20)
    plt.xlim(kvals[0,1],kvals[0,knyq])
    plt.ylim(1.e-7,1.e6)
    plt.text(10.,1.e-6,r'$mt = %.1f$' % times[tcur],fontsize=20)

plot_noncanonical_determinant(0,True)
plot_noncanonical_determinant(100,False)
plot_noncanonical_determinant(200,False)
plot_noncanonical_determinant(300,False)
plot_noncanonical_determinant(400,False)
plot_noncanonical_determinant(500,False)
plot_noncanonical_determinant(600,False)
plot_noncanonical_determinant(700,False)
plot_noncanonical_determinant(800,False)
plot_noncanonical_determinant(1500,False)
plt.show()

#plot_noncanonical_determinant_full(0,True)
#plt.show()
#plot_noncanonical_determinant_full(1,True)
#plt.show()
#plot_noncanonical_determinant_full(2,True)
#plt.show()
#plot_noncanonical_determinant_full(3,True)
#plt.show()
#plot_noncanonical_determinant_full(4,True)
#plt.show()
plot_noncanonical_determinant_full(5,True) # keep
plt.show()
plot_noncanonical_determinant_full(6,True) #keep
plt.show()
plot_noncanonical_determinant_full(50,True)
plt.show()
plot_noncanonical_determinant_full(8,True)
plt.show()
plot_noncanonical_determinant_full(213,True)
plt.ylim(1.e-14,1.e6)
plt.show()
# Now make all of the desired plots (edit this as needed)

kind_cur=2
tmax_ind=400
#plt.plot(times[0:tmax_ind],s_detphi[kind_cur][0:tmax_ind],label=r'$S_{\phi}$')
#plt.plot(times[0:tmax_ind],s_noncanonical[kind_cur][0:tmax_ind],label=r'$S_{\ln\rho}^{noncanonical}$')
#plt.plot(times[0:tmax_ind],s_detlnr[kind_cur][0:tmax_ind],label=r'$S_{\ln\rho}^{canonical}$')
#plt.plot(times[0:tmax_ind],kvol[kind_cur]*np.log(jacobian[0:tmax_ind]),label=r'$\ln(\mathcal{J})$')
#plt.plot(times[0:tmax_ind],np.abs(s_detlnr_noncanon[kind_cur][0:tmax_ind]-s_detphi[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{noncanonical}-S_{\phi}|$')
#plt.plot(times[0:tmax_ind],np.abs(s_detlnr[kind_cur][0:tmax_ind]-s_detphi[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{canonical}-S_{\phi}|$')
plt.plot(times[0:tmax_ind],np.abs(s_detlnr_noncanon[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{noncanonical}|$')
plt.plot(times[0:tmax_ind],np.abs(s_detphi[kind_cur][0:tmax_ind]),label=r'$|S_{\phi}|$')
plt.plot(times[0:tmax_ind],np.abs(s_detlnr[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{canonical}|$')
plt.plot(times[0:tmax_ind],np.abs(s_jacobian[kind_cur][0:tmax_ind]),label=r'$|S_{\mathcal{J}}|$')
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=20)
plt.ylabel(r'$S_i$',fontsize=20)
plt.legend(loc='lower right')
plt.yscale('log')
plt.savefig('entropy_l4_compare_noncanonical_lnrho.pdf')
plt.show()

