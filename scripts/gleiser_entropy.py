#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt
import mymathutils as mym

# Define location of relevant file
basedir='/mnt/scratch-lustre/jbraden/entropy_paper/l4/lnrho_fourmoms/kc2_l10/'
#basedir='/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_lnrho_fourdist/'
infile=basedir+'PSD'
logfile=basedir+'LOG.out'
spec_cols=[3,4]
nlat=512

numk=int(3.**0.5*nlat/2)+2 # don't touch this

a=np.genfromtxt(infile,usecols=spec_cols)
b=np.genfromtxt(logfile,usecols=[0,1,13])

spectra=[]
spectra.append( np.reshape(a[:,1],(-1,numk)) )
spectra=np.array(spectra)

kvals=np.reshape(a[:,0],(-1,numk))

times=b[:,0]
afac=b[:,1]
lnr_ave=b[:,2]

variances=np.sum(spectra[0]*kvals**2,axis=1)

probs=[]
for i in range(len(spectra[0])):
    probs.append(spectra[0,i,:]*kvals[i]**2/variances[i])
probs=np.array(probs)        

entropy=-np.sum(probs*np.log(probs+1.e-10),axis=1)

plt.plot(times,entropy,'b',alpha=0.25)
plt.plot(times,mym.smooth(entropy,101),'b')
plt.ylabel(r'$S$',fontsize=30)
plt.xlabel(r'$\sqrt{\lambda}M_P\tau$',fontsize=30)
plt.tight_layout()

import matplotlib as mpl
mpl.rc('xtick',labelsize=18)
mpl.rc('ytick',labelsize=18)

plt.savefig('gleiser_entropy.pdf')
