#!/usr/bin/env python

# Take the data stored in my CDF file and compute the resulting PDFs
# Inputs : infile - file storing the CDF
#        : datacols - columns with appropriate data, first column is the percentiles, others are CDF values at that percentile
#        : nlat - number of sampling sites for CDF

# To do : for ease of use make a dictionary associating variables with columns

paper_width=5.95

import sys
print sys.argv
viewPlots = (sys.argv[1] == 'True')
print "preview plots is ", viewPlots, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
from matplotlib.ticker import MaxNLocator
# needed for Gaussian fitting
from scipy.optimize import curve_fit

savePlots=True

def fitGauss(t,a,b):
    return np.exp(-a*(t-b)**2)

#infile="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_current_split/CDF"
#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/kc2_current_split/"

basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_current_split/"
#basedir='/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc1_current_split/'

infile=basedir+"CDF"
logfile=basedir+"LOG.out"
datacols=[0,1,2,3,4,9,10,5,6,7,8,11,12,13,14]

component_file="current_components_pdf_m2g2_t{:.1f}.pdf"
field_file="current_fieldcomp_pdf_m2g2_t{:.1f}.pdf"
rho_file="current_lnrho_pdf_m2g2_t{:.1f}.pdf"

nlat=513   #513 #129  #257

a=np.genfromtxt(infile,usecols=datacols)
b=np.genfromtxt(logfile,usecols=[0,1])

tvals=b[:,0]
avals=b[:,1]

times=np.reshape(a[:,0],(-1,nlat))
times=times[:,0:(nlat-1)] # resize since I differentiate the CDF
percentiles=np.reshape(a[:,1],(-1,nlat))

agrid=np.ones( (len(times), len(times[0])) )
for i in range(len(agrid[0])):
    agrid[:,i]=avals

cdf=[]
for i in range( 2,len(a[0]) ):
    cdf.append(np.reshape(a[:,i],(-1,nlat)))

pdf=[]
vals=[]
# Now create the PDFs
for i in range(len(cdf)):
    size=len(cdf[i])
    pdf.append(1./np.diff(cdf[i],axis=1))
    vals.append( 0.5*(cdf[i][:,1:nlat]+cdf[i][:,0:(nlat-1)]) )

pdfnorm=[]
for v in range(len(pdf)):
    tmp=pdf[v]
    for x in range(len(tmp)):
        tmp[x] /= np.max(tmp[x])
    pdfnorm.append(tmp)

def plot_pdfs(tindex,doFit=False,sigma=[1.,1.]):
    plt.plot(vals[0][tindex],pdf[0][tindex],'bo',label=r'$\log(\rho/\bar{\rho})$',mec='b')
    plt.plot(vals[1][tindex],pdf[1][tindex],'ro',label=r'$\partial_t\log(\rho/\bar{\rho})$',mec='r')
#    plt.plot(vals[2][tindex],pdf[2][tindex],'g--',label=r'$3H\left(\frac{P}{\rho}-\frac{\bar{P}}{\bar{\rho}}\right)$',linewidth=2)
#    plt.plot(vals[3][tindex],pdf[3][tindex],'k-.',label=r'$\partial_iT_{\phi}^{i0}/\rho$',linewidth=1.5)
#    plt.plot(vals[4][tindex],pdf[4][tindex],'c--',label=r'$\partial_iT_{\chi}^{i0}/\rho$',linewidth=1.5)

# Now do Gaussian fitting
    if doFit:
        x=vals[0][tindex][5:nlat-6]
        y=pdf[0][tindex][5:nlat-6]
        pFit, cFit = curve_fit(fitGauss,x,y,p0=(sigma[0],0.))
        plt.plot(vals[0][tindex],fitGauss(vals[0][tindex],pFit[0],pFit[1]),'b',label='Gaussian fit')
        print pFit, cFit
        x=vals[1][tindex][5:nlat-6]
        y=pdf[1][tindex][5:nlat-6]
        pFit, cFit = curve_fit(fitGauss,x,y,p0=(sigma[1],0.))
        plt.plot(vals[1][tindex],fitGauss(vals[1][tindex],pFit[0],pFit[1]),'r',label='Gaussian fit')

    plt.legend(bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,loc='upper right')
    plt.ylabel(r'$P/P_{max}$')
    plt.ylim(0.,1.2)
    plt.gca().set_yticks([0,0.25,0.5,0.75,1])
    plt.gca().xaxis.set_major_locator(MaxNLocator(5))
    plt.text(0.05,0.95,r'$mt='+str(times[tindex][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

def plot_pdfs_current_split(tindex):
    plt.plot(vals[5][tindex],pdf[5][tindex],'b',label=r'$\dot{\phi}\nabla^2\phi/a^2\rho$')
    plt.plot(vals[6][tindex],pdf[6][tindex],'r',label=r'$\dot{\chi}\nabla^2\chi/a^2\rho$')
    plt.plot(vals[7][tindex],pdf[7][tindex],'g--',label=r'$\nabla\dot{\phi}\cdot\nabla\phi/a^2\rho$')
    plt.plot(vals[8][tindex],pdf[8][tindex],'k--',label=r'$\nabla\dot{\chi}\cdot\nabla\chi/a^2\rho$')

    plt.legend(bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure)
    plt.ylabel(r'$P/P_{max}$')
    plt.ylim(0.,1.2)
    plt.gca().set_yticks([0,0.25,0.5,0.75,1])
    plt.gca().xaxis.set_major_locator(MaxNLocator(5))
    plt.text(0.05,0.95,r'$mt='+str(times[tindex][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

def plot_pdfs_components(tindex, add_leg=True, add_ylab=True):
    plt.plot(vals[3][tindex],pdf[3][tindex],'b',label=r'$\partial_iT^{i0}_{\phi}/\rho$')
    plt.plot(vals[4][tindex],pdf[4][tindex],'r',label=r'$\partial_iT^{i0}_{\chi}/\rho$')
    plt.plot(vals[9][tindex],pdf[9][tindex],'g-.',label=r'$\partial_iT^{i0}_{loc}/\rho$')
    plt.plot(vals[10][tindex],pdf[10][tindex],'k-.',label=r'$\partial_iT^{i0}_{grad}/\rho$')
    plt.plot(vals[11][tindex],pdf[11][tindex],'c--',label=r'$\partial_i(T^{i0}_{loc}-T^{i0}_{grad})/\rho$')
    plt.plot(vals[12][tindex],pdf[12][tindex],'m--',label=r'$\partial_i(T^{i0}_\phi-T^{i0}_\chi)/\rho$')

    if add_leg:
        plt.legend(bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure)
    plt.gca().set_yticks([0,0.25,0.5,0.75,1])
    if add_ylab:
        plt.ylabel(r'$P/P_{max}$')
    else:
        plt.gca().set_yticklabels([])
    plt.ylim(0.,1.2)
    plt.gca().xaxis.set_major_locator(MaxNLocator(5))
    plt.text(0.05,0.95,r'$mt='+str(times[tindex][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

time=220
plot_pdfs_components(time)
plt.xlim(-0.2,0.2)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
if viewPlots:
    plt.show()
plt.clf()

plot_pdfs_current_split(time)
plt.xlim(-0.15,0.15)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()

plot_pdfs(time,True,[1./0.01,1./0.1])
plt.xlim(-0.2,0.2)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()


time=240
plot_pdfs_components(time)
plt.xlim(-1.2,1.2)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()

plot_pdfs_current_split(time)
plt.xlim(-0.7,0.7)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()


plot_pdfs(time,True,sigma=[0.5,0.5])
plt.xlim(-1.2,1.2)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()

time=245
plot_pdfs_components(time)
plt.xlim(-1.2,1.2)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()
plot_pdfs_current_split(time)
plt.xlim(-0.7,0.7)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()

plot_pdfs(time,True,sigma=[0.5,0.5])
plt.xlim(-1.2,1.2)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()

time=250
plot_pdfs_components(time)
plt.xlim(-1.2,1.2)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()
plot_pdfs_current_split(time)
plt.xlim(-0.7,0.7)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()
plot_pdfs(time,True,sigma=[0.5,0.5])
plt.xlim(-1.5,1.5)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()

time=300
plot_pdfs_components(time)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()
plot_pdfs_current_split(time)
plt.xlim(-0.7,0.7)
if savePlots:
    plt.savefig(field_file.format(times[time][0]))
if savePlots:
    plt.show()
else:
    plt.clf()
plot_pdfs(time,True)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
if viewPlots:
    plt.show()
else:
    plt.clf()

time=600
plot_pdfs_components(time)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(component_file.format(times[time][0]))
if viewPlots:
    plt.show()
plt.clf()
plot_pdfs_current_split(time)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(field_file.format(times[time][0]) )
if viewPlots:
    plt.show()
plt.clf()

plot_pdfs(time,True)
plt.xlim(-2.5,2.5)
if savePlots:
    plt.savefig(rho_file.format(times[time][0]))
if viewPlots:
    plt.show()
plt.clf()

# compute ratio in here, need to add 2times the vertical sep and 1 times the horizontal
fsize=myplt.set_output_figure(0.95*paper_width,[0.1,0.05,0.95,0.9],1.75/1.618)
t_plots=[220,240,245,250,300,600]
plt.subplot(321)
plot_pdfs_components(t_plots[0], add_leg=False, add_ylab=True)
plt.xlim(-0.22,0.22)
plt.subplot(322)
plot_pdfs_components(t_plots[1], add_leg=False, add_ylab=False)
plt.xlim(-1.1,1.1)
plt.gca().set_xticks([-1,-0.5,0,0.5,1])
plt.subplot(323)
plot_pdfs_components(t_plots[2], add_leg=False, add_ylab=True)
plt.xlim(-1.1,1.1)
plt.gca().set_xticks([-1,-0.5,0,0.5,1])
plt.subplot(324)
plot_pdfs_components(t_plots[3], add_leg=False, add_ylab=False)
plt.xlim(-1.1,1.1)
plt.gca().set_xticks([-1,-0.5,0,0.5,1])
plt.subplot(325)
plot_pdfs_components(t_plots[4], add_leg=False, add_ylab=True)
plt.xlim(-2.5,2.5)
plt.gca().set_xticks([-2,-1,0,1,2])
plt.subplot(326)
plot_pdfs_components(t_plots[5], add_leg=False, add_ylab=False)
plt.xlim(-2.5,2.5)
plt.gca().set_xticks([-2,-1,0,1,2])

plt.subplots_adjust(hspace=0.25,wspace=0.02)
plt.legend(loc='upper center',bbox_to_anchor=(0.1,0.9,0.85,0.1),bbox_transform=plt.gcf().transFigure,ncol=3)
plt.savefig("current_components_pdf_m2g2_multipanel.pdf")
plt.show()

fsize=myplt.set_output_figure(0.48*paper_width,[0.18,0.22,0.92,0.9])
tcut=700
c=plt.contourf(times[0:tcut],vals[0][0:tcut],pdf[0][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.ylim(-3.,3.)
plt.xlim(0.,350.)
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
plt.xlabel(r'$mt$')
plt.ylabel(r'$\ln(\rho/\bar{\rho})$')
cbar=plt.colorbar(ticks=[0,0.5,1],pad=0.02)
cbar.set_label(r'$P(\ln(\rho/\bar{\rho}))/P_{max}$')
cbar.solids.set_rasterized(True)
if savePlots:
    plt.savefig('lnrho_pdf_tevolve.pdf')
plt.show()

c=plt.contourf(times[0:tcut],vals[1][0:tcut],pdf[1][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.ylim(-5.,5.)
plt.xlim(0.,350.)
plt.gca().xaxis.set_major_locator(MaxNLocator(5))
plt.xlabel(r'$mt$')
plt.ylabel(r'$\partial_t\ln(\rho/\bar{\rho})$')
cbar=plt.colorbar(ticks=[0,0.5,1],pad=0.02)
cbar.set_label(r'$P(\partial_t\ln(\rho/\bar{\rho}))/P_{max}$')
cbar.solids.set_rasterized(True)
if savePlots:
    plt.savefig('dlnrho_pdf_tevolve.pdf')
plt.show()
