import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt
import mymathutils as mym
from matplotlib.ticker import LogLocator, MaxNLocator

basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/cross_spectra/L10/kc2_lnrho_derivs_wcurdiff/"

logfile=basedir+'LOG.out'

b=np.genfromtxt(logfile,usecols=[0,1,3,4,5])

times=b[:,0]
afac=b[:,1]
rho_ave=b[:,2]
prs_ave=b[:,3]
lnr_ave=b[:,4]

plt.plot(times,1./np.abs(lnr_ave),'b',alpha=0.25)#,rasterized=True)
plt.plot(times,np.abs(1./mym.smooth(lnr_ave,51)),'b')

plt.xlabel(r'$\sqrt{\lambda}M_P\tau$')
plt.ylabel(r'$1/|\ln(\rho/\bar{\rho})|$')
plt.yscale('log')
plt.gca().yaxis.set_major_locator(LogLocator(numticks=5))

#plt.tight_layout(pad=0.5); myplt.fix_axes_aspect( plt.gcf(), plt.gca() )
plt.xlim(0,1000)
plt.gca().xaxis.set_major_locator(MaxNLocator(5))

plt.savefig('mach_number_l4.pdf')
plt.show()
