#!/usr/bin/python

# Plot the spectra for the fields and cross-correlators individually
# as well as the resulting determinant to go into the entropy

# To do (Jan. 29, 2014) - pass in a dictionary or something to set up the determinants instead of hard coding the various indices

import numpy as np
import matplotlib.pyplot as plt

def plot_components(itime):
    for i in range(len(spectra)):
        plt.plot( kvals[itime],np.abs(spectra[i][itime]) )
    plt.xscale('log')
    plt.yscale('log')
    return

#
# Plot the cross-correlators normalized to the fluctuations in the fields
#
def plot_crosscorrelations_normalized_fields(itime,time):
    plt.plot( kvals[itime], spectra[ind_dict['phiPi']][itime]/spectra[ind_dict['phi']][itime]**0.5/spectra[ind_dict['Pi']][itime]**0.5, label=r'$\Delta_{\phi\dot{\phi}}$')

    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1,1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$\Delta_{ij}$')

    tlabel=r'$mt='+str(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel,fontsize=20)  # adjust as needed depending on choice of scales

    plt.legend()
    return

def plot_crosscorrelations_normalized_lnrho(itime,time):
    plt.plot( kvals[itime], spectra[ind_dict['lnrlnp']][itime]/spectra[ind_dict['lnrho']][itime]**0.5/spectra[ind_dict['lnp']][itime]**0.5, label=r'$\Delta_{\ln\rho,\partial_t\ln\rho}$')
    plt.xscale('log')
    plt.yscale('linear')
    plt.ylim(-1.2,1.2)
    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$\Delta_{ij}$')

    plt.legend()

def plot_determinants(itime):
    plt.plot( kvals[itime], np.log(nphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}})$')
    plt.plot( kvals[itime], np.log(detphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}} - P_{\phi\dot{\phi}}^2)$')

    plt.legend()
    return

def plot_determinants_normed(itime,tnorm):
    plt.plot( kvals[itime], nphi[itime]/nphi[tnorm], 'b', label=r'$P_{\phi\phi}P_{\Pi_{\phi}\Pi_{\phi}}$')
    plt.plot( kvals[itime], detphi[itime]/detphi[tnorm], 'b--',markersize=5., label=r'$\Delta_{\phi}$')

    plt.legend()

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$k/m$',fontsize=20)
    plt.ylabel(r'$F(k,t)/F(k,t=0)$',fontsize=20)
    return

def compute_determinants():
    global nphi
    nphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]
    global detphi
    detphi=spectra[ind_dict['phi']]*spectra[ind_dict['Pi']]-spectra[ind_dict['phiPi']]**2

    global nlnrho
    nlnrho=spectra[ind_dict['lnrho']]*spectra[ind_dict['lnp']]
    global detlnrho
    detlnrho=spectra[ind_dict['lnrho']]*spectra[ind_dict['lnp']]-spectra[ind_dict['lnrlnp']]**2
                
def compute_crossspec_norm():
    global phi_phidot
    phi_phidot = spectra[ind_dict['phiPi']]/spectra[ind_dict['phi']]**0.5/spectra[ind_dict['Pi']]**0.5 

    global lnr_dlnr
    lnr_dlnr = spectra[ind_dict['lnrlnp']]/spectra[ind_dict['lnrho']]**0.5/spectra[ind_dict['lnp']]**0.5

#To do: add dk part
def compute_entropies(kcut):
    global s_phi, ds_phi
    s_phi=[]
    global s_detphi, ds_detphi
    s_detphi=[]
    global s_lnr, s_dlnr
    s_lnr=[]
    s_dlnr=[]
    global s_detlnr
    s_detlnr=[]

    dk = kvals[0,1]-kvals[0,0]

    for kmax in kcut:
      s_phi.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(nphi[:,1:kmax]),axis=1) )
      s_detphi.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(detphi[:,1:kmax]),axis=1) )
      s_lnr.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(nlnrho[:,1:kmax]),axis=1) )
      s_detlnr.append( 0.5*dk*np.sum(kvals[:,1:kmax]**2*np.log(detlnrho[:,1:kmax]),axis=1) )
 
    s_phi=np.array(s_phi)
    ds_phi=np.diff(s_phi,axis=1)
    s_detphi=np.array(s_detphi)
    ds_detphi=np.diff(s_detphi,axis=1)

    s_lnr=np.array(s_lnr)
    s_detlnr=np.array(s_detlnr)

    return

def plot_entropy_differences(kind):
#    plt.plot( ,s_phi[kind]-s_detphi[kind],label=r'$S_{n_{\phi}}-S_{\Delta_{\phi}}$')
#    plt.plot( ,s_chi[kind]-s_detchi[kind],label=r'$S_{n_{\chi}}-S_{\Delta_{\chi}}$')
#    plt.plot( ,s_detphi[kind]+s_detchi[kind]-s_dettot,label=r'$S_{\Delta_{\phi}}+S_{\Delta_{\chi}} - S_{\Delta_{tot}}$')
    
    plt.yscale('log')
    plt.xlim(0.,500.)
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$\Delta S$')

    tlabel=r'$\frac{k_{cut}}{m}='+str(kvals[kcuts[kind]])+'$'
#    plt.text(,0.75,tlabel,fontsize=20)

# User modified specifications for the files to make the graph
basedir='/mnt/scratch-lustre/jbraden/crossspec/l4/n512/kc2_nomean/'
infile=basedir+'PSD'
logfile=basedir+'LOG.out'

ind_dict={'phi':0,
          'Pi':1,
          'phiPi':2,
          'lnrho':3,
          'lnp':4,
          'lnrlnp':5}

datacols=[3,4,5,6,8,10,11]
nlat=512

numk=int(3.**0.5*nlat/2)+2  # don't touch this
a=np.genfromtxt(infile,usecols=datacols)

kvals=np.reshape(a[:,0],(-1,numk))

spectra=[]
for i in range(1,len(a[0])):
    spectra.append( np.reshape(a[:,i],(-1,numk)) )

b=np.genfromtxt(logfile,usecols=[0,1,3,4,12,13])
times=b[:,0]
afac=b[:,1]
rho_ave=b[:,2]
prs_ave=b[:,3]
phi_ave=b[:,4]
piphi_ave=b[:,5]
w_ave=prs_ave/rho_ave

jacobian = 2.*np.abs(phi_ave**3*piphi_ave)/(afac**4*rho_ave**2*(1.+w_ave))

compute_determinants()
compute_crossspec_norm()

kcuts=[63,127,191,255]
compute_entropies(kcuts)

kvol=[]
s_noncanonical=[]
dk = kvals[0][1]-kvals[0][0]
for i in range(len(kcuts)):
    kvol.append(dk*np.sum(kvals[0][0:kcuts[i]]**2))
    s_noncanonical.append(s_detlnr[i]-kvol[i]*np.log(jacobian))

# Now make all of the desired plots (edit this as needed)



kind_cur=0
tmax_ind=800
#plt.plot(times[0:tmax_ind],s_detphi[kind_cur][0:tmax_ind],label=r'$S_{\phi}$')
#plt.plot(times[0:tmax_ind],s_noncanonical[kind_cur][0:tmax_ind],label=r'$S_{\ln\rho}^{noncanonical}$')
#plt.plot(times[0:tmax_ind],s_detlnr[kind_cur][0:tmax_ind],label=r'$S_{\ln\rho}^{canonical}$')
#plt.plot(times[0:tmax_ind],kvol[kind_cur]*np.log(jacobian[0:tmax_ind]),label=r'$\ln(\mathcal{J})$')
plt.plot(times[0:tmax_ind],np.abs(s_noncanonical[kind_cur][0:tmax_ind]-s_detphi[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{noncanonical}-S_{\phi}|$')
plt.plot(times[0:tmax_ind],np.abs(s_detlnr[kind_cur][0:tmax_ind]-s_detphi[kind_cur][0:tmax_ind]),label=r'$|S_{\ln\rho}^{canonical}-S_{\phi}|$')
plt.xlabel(r'$mt$',fontsize=24)
plt.ylabel(r'$S_i$',fontsize=24)
plt.legend(loc='lower right')
plt.yscale('log')

#dt=0.03125
#for i in range(6400):
#    plot_crosscorrelations_normalized(i,i*dt)
#    figname="cross_%04d.png" % i
#    plt.savefig(figname)
#    plt.clf()

#j=3
#plt.plot(s_phi[j]-s_detphi[j],label=r'$\phi$')
#plt.plot(s_chi[j]-s_detchi[j],label=r'$\chi$')
#plt.plot(-s_dettot[j]+s_detphi[j]+s_detchi[j],label=r'$tot$')

#j=2
#plt.plot(s_detphi,label=r'$\phi$')
#plt.plot(s_detchi,label=r'$\chi$')
#plt.plot(s_dettot,label=r'$tot$')
