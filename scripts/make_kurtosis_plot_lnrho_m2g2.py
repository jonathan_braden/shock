import numpy as np
import matplotlib.pyplot as plt

# This is for lnrho
basedir = "/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/"
momfile_dlnr=basedir+"kc2_dlnrhho_fourdist/FOURMOMS"
momfile_lnr=basedir+"kc2_lnrho_fourdist/FOURMOMS"
logfile_lnr=basedir+"kc2_lnrho_fourdist/LOG.out"
logfile_dlnr=basedir+"kc2_dlnrhho_fourdist/LOG.out"

nlat=512
knyq=nlat/2

c1=np.genfromtxt(logfile_lnr,usecols=[0,1])
c2=np.genfromtxt(logfile_dlnr,usecols=[0,1])
tvals=c1[:,0]
avals=c1[:,1]

numt1=len(c1[:,0])
numt2=len(c2[:,0])
datacols=[0,1,4,8,5,9]
a=np.genfromtxt(momfile_lnr,usecols=datacols)
b=np.genfromtxt(momfile_dlnr,usecols=datacols)

times_lnr=np.reshape(a[:,0],(numt1,-1))
kvals_lnr=np.reshape(a[:,1],(numt1,-1))
r2_lnr=np.reshape(a[:,2],(numt1,-1))
r4_lnr=np.reshape(a[:,3],(numt1,-1))
i2_lnr=np.reshape(a[:,4],(numt1,-1))
i4_lnr=np.reshape(a[:,5],(numt1,-1))

r2_lnr=0.5*(r2_lnr+i2_lnr)
r4_lnr=0.5*(r4_lnr+i4_lnr)

times_dlnr=np.reshape(b[:,0],(numt2,-1))
kvals_dlnr=np.reshape(b[:,1],(numt2,-1))
r2_dlnr=np.reshape(b[:,2],(numt2,-1))
r4_dlnr=np.reshape(b[:,3],(numt2,-1))
i2_dlnr=np.reshape(b[:,4],(numt2,-1))
i4_dlnr=np.reshape(b[:,5],(numt2,-1))

r2_dlnr=0.5*(r2_dlnr+i2_dlnr)
r4_dlnr=0.5*(r4_dlnr+i4_dlnr)

from matplotlib.ticker import MaxNLocator
mycmap_lnr=plt.cm.OrRd
mycmap_dlnr=plt.cm.BuGn
tplots=[220,240,245,250,300,600]
for i in tplots:
    plt.plot(kvals_lnr[i][1:knyq],r4_lnr[i][1:knyq]/r2_lnr[i][1:knyq]**2-3.,'b',label=r'$\ln(\rho/\bar{\rho})$')
    plt.plot(kvals_dlnr[i][1:knyq],r4_dlnr[i][1:knyq]/r2_dlnr[i][1:knyq]**2-3.,'r--',label=r'$\partial_t\ln(\rho/\bar{\rho})$')
    plt.legend()
    plt.xlim(kvals_lnr[0][1],kvals_lnr[0][knyq])
    plt.ylim(-1.1,3.1)
    plt.gca().yaxis.set_major_locator(MaxNLocator(5))
    plt.xlabel(r'$k/m$')
#    plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=24)
    plt.ylabel(r'$\kappa_4$')
    plt.text(200.,-0.5,r'$mt='+str(tvals[i])+'$')
    plt.savefig('kurtosis_lnr_m2g2_t{:.1f}.pdf'.format(tvals[i]))
    plt.show()

from matplotlib.ticker import LogLocator
# Plots with the spectrum on the bottom
for i in tplots:
    fig, ax0 = plt.subplots(nrows=2,ncols=1,sharex=True)
    ax0[0].plot(kvals_lnr[i][1:knyq],r4_lnr[i][1:knyq]/r2_lnr[i][1:knyq]**2-3.,'b',label=r'$\ln(\rho/\bar{\rho})$')
    ax0[0].plot(kvals_dlnr[i][1:knyq],r4_dlnr[i][1:knyq]/r2_dlnr[i][1:knyq]**2-3.,'r--',label=r'$\partial_t\ln(\rho/\bar{\rho})$')
    ax0[0].set_ylabel(r'$\kappa_4$')
    ax0[1].plot(kvals_lnr[i][1:knyq],r2_lnr[i][1:knyq]*kvals_lnr[i][1:knyq]**3,'b',label=r'$\ln(\rho/\bar{\rho})$')
    ax0[1].plot(kvals_dlnr[i][1:knyq],r2_dlnr[i][1:knyq]*kvals_dlnr[i][1:knyq]**3,'r--',label=r'$\partial_t\ln(\rho/\bar{\rho})$')
    ax0[1].set_ylabel(r'$k^3P(k)$')
    plt.subplots_adjust(hspace=0)
    plt.xlabel(r'$k/m$')
    ax0[0].set_xscale('linear')
    ax0[1].set_xscale('linear')
    ax0[1].set_yscale('log')
    ax0[1].set_ylim(1.e10,1.e16)
    ax0[1].set_yticks([1.e10,1.e12,1.e14])
#    ax0[1].set_ylim(1.e7,1.e16)
 #   ax0[1].set_yticks([1.e8,1.e11,1.e14])
    ax0[0].set_ylim(-1.1,2.1)
    ax0[0].set_yticks([-1,0,1,2])
    plt.xlim(kvals_lnr[0][1],kvals_lnr[0][knyq])
    plt.legend()
    ax0[0].text(kvals_lnr[i][knyq/8],-0.7,r'$mt='+str(tvals[i])+'$')
    plt.savefig( 'kurtosis_lnr_m2g2_wspec_t{:.1f}.pdf'.format(tvals[i]) )
    plt.show()

# To do, put in ubiased estimators here

plt.contourf(times_lnr[:,0:knyq],kvals_lnr[:,0:knyq],r4_lnr[:,0:knyq]/r2_lnr[:,0:knyq]**2-3.,cmap=plt.cm.OrRd)
plt.ylabel(r'$k/m$')
plt.xlabel(r'$mt$')
plt.show()

plt.contourf(times_dlnr[:,0:knyq],kvals_dlnr[:,0:knyq],r4_dlnr[:,0:knyq]/r2_dlnr[:,0:knyq]**2-3.,cmap=plt.cm.OrRd)

