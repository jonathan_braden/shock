import numpy as np
import matplotlib.pyplot as plt

#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/dlnrho_fourmoms/kc2_l20/"
#field=r'\partial_t\ln\rho'
#outfile_base="fourdist_lnr_l4_t"
#tplots=[250,300,350,400,450,2000]
#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/dlnrho_fourmoms/kc2_l20/"
#field=r'\ln\rho'
#outfile_base="fourdist_dlnr_l4_t"
#tplots=[250,300,350,400,450,2000]
#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/phi_fourmoms/kc2_l20/"
#field=r'a\delta\phi'
#outfile_base="fourdist_phi_l4_t"
#tplots=[500,600,700,800,900,4000]
basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/dphi_fourmoms/kc2_l20/"
field=r'a\partial_\tau\delta\phi'
outfile_base="fourdist_dphi_l4_t"
tplots=[500,600,700,800,900,4000]

refile=basedir+"REDIST"
imfile=basedir+"IMDIST"
logfile=basedir+"LOG.out"

kbins=[r'$0< k< \frac{k_{nyq}}{5}$',r'$\frac{k_{nyq}}{5} < k < \frac{2k_{nyq}}{5}$',r'$\frac{2k_{nyq}}{5} < k < \frac{3k_{nyq}}{5}$',r'$\frac{3k_{nyq}}{5} < k < \frac{4k_{nyq}}{5}$',r'$\frac{4k_{nyq}}{5} < k < k_{nyq}$']

a=np.genfromtxt(refile,usecols=[1,2,3,4,5,6])
b=np.genfromtxt(imfile,usecols=[1,2,3,4,5,6])
c=np.genfromtxt(logfile,usecols=[0,1])

times=c[:,0]
ntime=len(times)

vals=np.reshape(a[:,0],(ntime,-1))
re_pdf=np.reshape(a[:,1:],(ntime,-1,5))
im_pdf=np.reshape(b[:,1:],(ntime,-1,5))

pdf=(re_pdf+im_pdf)

numk=[]
for i in range(len(re_pdf[0][0])):
    numk.append(2.*np.sum(re_pdf[0,:,i]))

# Make the Gaussian to compare to
gauss_norm=0.01325 # how to compute this
gaussian=gauss_norm*np.exp(-vals[0]**2)

def plot_fourier_pdf(tind):
    colors=['b','r','g','c','m']
    for i in range(len(pdf[0][0])):
        plt.plot(vals[tind],pdf[tind,:,i]/numk[i],colors[i],label=kbins[i])
    plt.plot(vals[tind],gaussian,'k--',label=r'Gaussian')
    plt.text(-2.5,0.014,r'$\sqrt{\lambda}M_P\tau='+str(times[tind])+'$')
    plt.legend()
    plt.ylim(0,0.02)
    plt.xlabel(r'$'+field+'_k/\sqrt{2\sigma_k^2}$')
    plt.ylabel(r'$P('+field+'_k)$')

#for tcur in tplots:
#    plot_fourier_pdf(tcur)
#    outfile=outfile_base+'{:.0f}.pdf'.format(times[tcur])
#    plt.savefig(outfile)
#    plt.show()
