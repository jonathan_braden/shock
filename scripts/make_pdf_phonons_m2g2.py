#!/usr/bin/env python

# Take the data stored in my CDF file and compute the resulting PDFs
# Inputs : infile - file storing the CDF
#        : datacols - columns with appropriate data, first column is the percentiles, others are CDF values at that percentile
#        : nlat - number of sampling sites for CDF

# To do : for ease of use make a dictionary associating variables with columns

import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ", sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt; import myplotutils as myplt

#infile='/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/rho/L10/kc2_n128_current/CDF'
#datacols=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14]

infile="/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/rho/L10/kc2_lnrho_derivs/CDF"
datacols=[0,1,2,3,4,5,6,7,8,9,10]

#infile="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_/CDF"
#datacols=[2,3,4,9,10]

nlat=513 #129  #257

a=np.genfromtxt(infile,usecols=datacols)

times=np.reshape(a[:,0],(-1,nlat))
times=times[:,0:(nlat-1)] # resize since I differentiate the CDF
percentiles=np.reshape(a[:,1],(-1,nlat))

cdf=[]
for i in range( 2,len(a[0]) ):
    cdf.append(np.reshape(a[:,i],(-1,nlat)))

pdf=[]
vals=[]
# Now create the PDFs
for i in range(len(cdf)):
    size=len(cdf[i])
    pdf.append(1./np.diff(cdf[i],axis=1))
    vals.append( 0.5*(cdf[i][:,1:nlat]+cdf[i][:,0:(nlat-1)]) )

pdfnorm=[]
for v in range(len(pdf)):
    tmp=pdf[v]
    for x in range(len(tmp)):
        tmp[x] /= np.max(tmp[x])
    pdfnorm.append(tmp)

def plot_pdfs(tindex):
    plt.plot(vals[0][tindex],pdf[0][tindex],'b',label=r'$\log(\rho/\bar{\rho})$')
    plt.plot(vals[1][tindex],pdf[1][tindex],'r',label=r'$\partial_t\log(\rho/\bar{\rho})$')
    plt.plot(vals[2][tindex],pdf[2][tindex],'g-',label=r'$3H\left(\frac{P}{\rho}-\frac{\bar{P}}{\bar{\rho}}\right)$',linewidth=2)
    plt.plot(vals[4][tindex],pdf[4][tindex],'k-.',label=r'$\partial_iT_{\phi}^{i0}/\rho$')
    plt.plot(vals[5][tindex],pdf[5][tindex],'c--',label=r'$\partial_iT_{\chi}^{i0}/\rho$')

    plt.legend(bbox_to_anchor=(0,0,1.,1.),loc='upper right',bbox_transform=plt.gcf().transFigure,borderaxespad=0.25)
    plt.ylabel(r'$P/P_{max}$')
    plt.ylim(0,1.2)
    plt.gca().set_yticks([0,0.25,0.5,0.75,1])
    plt.text(0.05,0.95,r'$mt='+str(times[time][0])+'$',transform=plt.gca().transAxes,horizontalalignment='left',verticalalignment='top')

time=400
plot_pdfs(time)
plt.xlim(-0.1,0.1)
#plt.savefig('.pdf'.format(times[time][0]) )
if showPreview:
    plt.show()
plt.clf()

time=500
plot_pdfs(time)
plt.xlim(-2,2)
if showPreview:
    plt.show()
plt.clf()

time=600
plot_pdfs(time)
plt.text(-2.5,0.8,r'$mt='+str(times[time][0])+'$',fontsize=20)
plt.xlim(-3.,3.)
plt.show()    

tcut=1000
c=plt.contourf(times[0:tcut],vals[0][0:tcut],pdf[0][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.xlabel(r'$mt$',fontsize=20)
plt.ylabel(r'$\ln(\rho/\bar{\rho})$',fontsize=20)
cbar=plt.colorbar()
cbar.set_label(r'$P(\ln(\rho/\bar{\rho}))/P_{max}$',fontsize=20)
cbar.solids.set_rasterized(True)
plt.show()

c=plt.contourf(times[0:tcut],vals[1][0:tcut],pdf[1][0:tcut],cmap=plt.cm.OrRd)
myplt.insert_rasterized_contour_plot(c)
plt.xlabel(r'$mt$',fontsize=20)
plt.ylabel(r'$\partial_t\ln(\rho/\bar{\rho})$',fontsize=20)
cbar=plt.colorbar()
cbar.set_label(r'$P(\partial_t\ln(\rho/\bar{\rho}))/P_{max}$',fontsize=20)
cbar.solids.set_rasterized(True)
plt.show()
