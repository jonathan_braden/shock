import numpy as np
import matplotlib.pyplot as plt

#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_dlnrhho_fourdist/"
#outfile_base="fourdist_dlnr_m2g2_t"
#field=r'\partial_t\ln\rho'

#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_lnrho_fourdist/"
#outfile_base="fourdist_lnr_m2g2_t"
#field=r'\ln\rho'

#basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_phi_fourdist/"
#outfile_base="fourdist_phi_m2g2_t"
#field=r'a^{3/2}\phi'

basedir="/mnt/scratch-lustre/jbraden/entropy_paper/m2g2/n512/L5/kc2_chi_fourdist/"
outfile_base="fourdist_chi_m2g2_t"
field=r'a^{3/2}\chi'

knyq=np.pi*512/5.
dknyq=0.2

refile=basedir+"REDIST"
imfile=basedir+"IMDIST"
logfile=basedir+"LOG.out"

kbins=[r'$0< k< \frac{k_{nyq}}{5}$',r'$\frac{k_{nyq}}{5} < k < \frac{2k_{nyq}}{5}$',r'$\frac{2k_{nyq}}{5} < k < \frac{3k_{nyq}}{5}$',r'$\frac{3k_{nyq}}{5} < k < \frac{4k_{nyq}}{5}$',r'$\frac{4k_{nyq}}{5} < k < k_{nyq}$']

a=np.genfromtxt(refile,usecols=[1,2,3,4,5,6])
b=np.genfromtxt(imfile,usecols=[1,2,3,4,5,6])
c=np.genfromtxt(logfile,usecols=[0,1])

times=c[:,0]
ntime=len(times)

vals=np.reshape(a[:,0],(ntime,-1))
re_pdf=np.reshape(a[:,1:],(ntime,-1,5))
im_pdf=np.reshape(b[:,1:],(ntime,-1,5))

pdf=(re_pdf+im_pdf)

numk=[]
for i in range(len(re_pdf[0][0])):
    numk.append(2.*np.sum(re_pdf[0,:,i]))

# Make the Gaussian to compare to
gauss_norm=0.01325 # how to compute this
gaussian=gauss_norm*np.exp(-vals[0]**2)

def plot_fourier_pdf(tind):
    colors=['b','r','g','c','m']
    for i in range(3):   #range(len(pdf[0][0])):
        lab_cur = r'${:.0f}'.format(knyq*(i*dknyq))+r'< \frac{k}{m} <'+r'{:.0f}$'.format(knyq*(i+1)*dknyq)
        plt.plot(vals[tind],pdf[tind,:,i]/numk[i],colors[i],label=lab_cur) #label=kbins[i])
    plt.plot(vals[tind],gaussian,'k--',label=r'Gaussian')
    plt.text(-2.5,0.014,r'$mt='+str(times[tind])+'$')
    plt.legend(bbox_to_anchor=(0,0,1.,1.),bbox_transform=plt.gcf().transFigure,borderaxespad=0.2)
    plt.ylim(0,0.018)
    plt.xlim(-3.,3.)
    plt.xlabel(r'$'+field+'_k/\sqrt{2\sigma_k^2}$'  )
    plt.ylabel(r'$P('+field+'_k)$')

tplots=[220,240,245,250,300,600]
for tcur in tplots:
    plot_fourier_pdf(tcur)
    outfile=outfile_base+'{:.1f}.pdf'.format(times[tcur])
    cplot=plt.gca()
    cplot.axes.get_yaxis().set_ticks([])
    plt.savefig(outfile)
    plt.show()
