#!/usr/bin/python

#
# Make a plot of just the power spectrum of ln(rho) for possible inclusion in the paper
#

basefolder = '/media/data/April1/four_moms/m2g2/N512/kc2_a2.5/'
psdfile = basefolder+'PSD'
logfile = basefolder+'LOG.out'

# Columns to get data out of
kc=3  # fourth column for kvalues
dc=6  # column with power spectrum (check this)
kcut=256

import sys
sys.path.insert(0,'/home/jbraden/Documents/python_scripts/')

import mymathutils as mym
import myfileutils as myf
import numpy as np

specvals=np.array(myf.read_timeblock(psdfile,[kc,dc]))
kvals=specvals[0][0]
pspec=[]
entspec=[]
specinit=specvals[0][1]
for i in range(len(specvals)):
    pspec.append(specvals[i][1])
    entspec.append(kvals[:kcut]**2*np.log(specvals[i][1][:kcut]/specinit[:kcut])
acol=1
sclfac=np.log(np.array(myf.read_data_tstreams(logfile, 10, [acol])))
# Now get the kvalues and spectrum values

# Now make the plot
import matplotlib.pyplot as plt
mycmap=plt.cm.OrRd

for i in range(0,len(entspec),10):
    plt.plot(kvals[:kcut],entspec[i], color=mycmap(i))

plt.show()
#plt.contourf(entspec, cmap=plt.cm.OrRd)
#plt.xlabel(r'$\frac{k_{com}}{m}$')
#plt.ylabel(r'$k_{com}^2\log(P_k(a)/P_k(a_{end}))$')
#plt.show()
