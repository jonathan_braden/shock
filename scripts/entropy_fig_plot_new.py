#!/usr/bin/python

#
# Take a single PSD file and compute the entropy and entropy production rate
# from it.  Do this for several cutoff values to study the scale dependence
#
# Since this is producing the paper figure, we also take the corresponding
# LOG file and extract 1/<lnrho> and a^4<lnrho>e^(3(1+<w>))
# This quantity could also be the more usual one
#
# The mean value of lnrho is obtained from the zero mode of the power spectrum
#

import sys
sys.path.insert(0,'/home/jbraden/Documents/python_scripts/')

import mymathutils as mym
import myfileutils as myf
import numpy as np
import get_entropy_new as ent

import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

# Specify the folder containing the data we will use
#basefolder='/mnt/scratch-3week/jbraden/four_moms/m2g2/N512/kc2_a2.5/'
#basefolder='/mnt/scratch-3week/jbraden/g2_symp/N32/a20./kf2./L5./g100./N64/'
basefolder='/media/data/April1/four_moms/m2g2/N512/kc2_a2.5/'

psdfile=basefolder+'PSD'
logfile=basefolder+'LOG.out'

# What are these parameters
kc=3  # column with kvalues in PSD
dc=6  # column with desired variable in PSD
#dc=8
kmin=40
kmax=501
kstep=20
islogged=False
nhead=10

# Adjust this or read it in from a log file
#winsize=501  # for 64
winsize=301
wtype='kaiser'
kparam=300.
kcut = 75
#kcut=55

tcol=0
acol=1
rhocol=3
prscol=4
lnrhocol=5
eoscol=8

# Now pick out a particular value of kcut to plot
entropy=ent.get_entropy(psdfile, kc, dc, kcut, islogged)
psdvalues=myf.read_timeblock(psdfile, [dc])
lnrho=[]
# Get mean value of lnrho
for i in range(len(psdvalues)):
    lnrho.append((psdvalues[i][0][0])**0.5)
lnrho=np.array(lnrho)    
psdvalues=[]    

# Now get scale factor, time, mean density, etc. from file
logvalues=myf.read_data_tstreams(logfile, nhead, [tcol, acol, rhocol, prscol, eoscol])

time=logvalues[0]
entsm=mym.smooth(entropy, winsize, wtype, kparam)
entd=np.array(mym.derivative(time,entropy))
entdsm=np.array(mym.derivative(time,entsm))
#entdsm=np.array(mym.smooth(entd, winsize, wtype, kparam))
# Normalize entropy to maximum
entdsm=entdsm/entdsm.max()

# Get various interesting quantities out of the LOG.out file
weff=np.array(logvalues[3])/np.array(logvalues[2])
wsm=mym.smooth(weff, winsize, wtype,kparam)
wave=np.array(logvalues[4])
wave=mym.smooth(wave,winsize,wtype,kparam)
sclsm=mym.smooth(np.array(logvalues[1]),winsize,wtype,kparam)
rhosm=mym.smooth(np.array(logvalues[2]),winsize,wtype,kparam)
#lnrhosm=mym.smooth(np.log(lnrho),winsize,window=wtype)
#lnrhosm=np.exp(lnrhosm)
lnrhosm=mym.smooth(lnrho,winsize,window=wtype)


ceff=1./lnrho
ceffsm2=1./lnrhosm
ceffsm=mym.smooth(np.log(ceff),winsize,wtype,kparam)
ceffsm=np.exp(ceffsm)

#Now compute the remaining quantities (note, if necessary I can extract lnrho out of the psd, I should check that it matches the LOGfile one
#
# I still need to add multiple panels to this plot
                  
# Here we select what to use as our time axis
timepl=np.log(sclsm)
xvar=r'$\ln(a/a_{end})$'
#xmin=time.min()
xmin=2
xmax=timepl.max()
clevs=np.arange(0.35,1.01,0.05)


plt.subplot(311)
#plt.hold(True)
# First plot the entropy derivative

#entsm=mym.smooth(np.array(entropy), 18, window=wtype)
#entsm=mym.smooth(np.array(entropy), winsize, 'kaiser', kparam)
ymin=min(0.,2.*entsm.min())
ymax=max(0.,2.*entsm.max())         
y=np.linspace(ymin,ymax,51)

y_loc=-0.11

plt.contourf(timepl, y, len(y)*[entdsm], clevs, cmap=plt.cm.OrRd, alpha=0.7)
plt.plot(timepl, entsm, 'b-')
plt.plot(timepl, entropy, alpha=0.2)
#plt.plot(timepl,entd, 'g-', alpha=0.2)
plt.plot(timepl, entdsm, 'g-', alpha=0.6)
plt.ylabel(r'$\frac{S_{\ln\rho}-S_{\ln\rho}(t=0)}{\mathcal{N}_{eff}}$')
plt.xlim([xmin, xmax])
plt.ylim([ymin,ymax])
plt.gca().set_xticklabels([])
plt.gca().yaxis.set_label_coords(y_loc,0.5)

#Add a new panel
plt.subplot(312)
ymin=ceffsm.min() / 2.
ymax=ceffsm.max() * 2.
print ymin, ymax
y=np.logspace(np.log10(ymin),np.log(ymax),51)
plt.contourf(timepl, y, len(y)*[entdsm], clevs, cmap=plt.cm.OrRd, alpha=0.7)
plt.plot(timepl, ceff, alpha=0.2)
plt.plot(timepl, ceffsm, 'b-')
#plt.plot(timepl, ceffsm2, 'r')
plt.plot(timepl, (ymax/10)*np.exp(entdsm), 'g-', alpha=0.6)
plt.yscale('log')
plt.ylabel(r'$\frac{1}{|\langle\ln(\rho/\bar{\rho})\rangle|}$')
plt.xlim([xmin, xmax])
plt.ylim([ymin,ymax])
plt.gca().set_xticklabels([])
plt.gca().yaxis.set_label_coords(y_loc,0.5)

#Add a new panel
plt.subplot(313)
plt.hold(True)

#charge=3.*(1+weff)*np.log(np.array(logvalues[1])) + np.log(np.array(logvalues[2]))
#chargesm=3.*(1+wsm)*np.log(sclsm)+np.log(rhosm)
#chargesm2=3.*(1+wsm)*np.log(np.array(logvalues[1])) + np.log(np.array(logvalues[2]))

charge=np.log(np.array(logvalues[1])) + np.log(np.array(logvalues[2]))/3./(1.+weff)
charge2=np.log(np.array(logvalues[1])) + np.log(np.array(logvalues[2]))/3./(1.+wsm)
chargesm=np.log(sclsm) + np.log(rhosm)/3./(1.+wsm)
chargesm2=np.log(sclsm) + np.log(rhosm)/3./(1.+wsm)

chsm=mym.smooth(charge2,winsize,wtype,kparam)
ymin=min(0.,2.*chsm.min())
ymax=max(0,2.*chsm.max())
y=np.linspace(ymin,ymax,51)

plt.contourf(timepl ,y , len(y)*[entdsm], clevs, cmap=plt.cm.OrRd, alpha=0.7)
plt.plot(timepl, charge, alpha=0.2)
#plt.plot(timepl, charge2, 'ko')
plt.plot(timepl, chsm, 'b-')
plt.plot(timepl, (ymax/2)*entdsm, 'g-', alpha=0.6)
plt.ylabel(r'$\ln a+\frac{\ln\rho}{3(1+w)}$')
plt.xlabel(xvar)
plt.ylim([ymin, ymax])
plt.xlim([xmin, xmax])
plt.gca().yaxis.set_major_locator(MaxNLocator(4))
plt.gca().yaxis.set_label_coords(y_loc,0.5)

plt.subplots_adjust(hspace=0.02)
plt.savefig('shock_variables.pdf')
plt.show()
