#!/usr/bin/python
#
# Collection of plotting functions for the correlation and cross-correlation
# functions used in the entropy paper
#

import numpy as np
import matplotlib.pyplot as plt

#
# Functions to actually make the plots
#
def plot_crosscorrelations_normalized(kvals, specvals, legvals ):
    for i in range(len(specvals)):
        plt.plot(kvals, specvals[i] )
    return

def plot_crosscorrelations_normalized_abs():
    return

def plot_determinants():
    return

def plot_determinants_normed():
    return

#
# Functions for reading and processing data
#
def read_correlations():
    return

def compute_entropies():
    return

def compute_crossspec_norm():
    return

def plot_entropy_differences():
    return

def compute_crossentropies():
    return

def compute_determinants():
    return

def plot_cross_entropies_wsmooth():
    return

def plot_cross_entropies():
    return

def plot_entropies_wsmooth():
    return

def plot_entropy_derivs():
    return
