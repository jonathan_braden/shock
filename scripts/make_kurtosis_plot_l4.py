import numpy as np
import matplotlib.pyplot as plt

# This is for lnrho
#basedir = "/mnt/scratch-lustre/jbraden/entropy_calcs/m2g2/four_moms/N512/10_bins/L10/kc1_a2.5/"
basedir="/mnt/scratch-lustre/jbraden/entropy_paper/l4/"
momfile_phi=basedir+"phi_fourmoms/kc2_l20/FOURMOMS"
momfile_dphi=basedir+"phi_fourmoms/kc2_l20/FOURMOMS"  # fix this
momfile_lnr=basedir+"lnrho_fourmoms/kc2_l20/FOURMOMS"
momfile_dlnr=basedir+"dlnrho_fourmoms/kc2_l20/FOURMOMS"
logfile_phi=basedir+"phi_fourmoms/kc2_l20/LOG.out"
logfile_dphi=basedir+"phi_fourmoms/kc2_l20/LOG.out"
logfile_lnr=basedir+"lnrho_fourmoms/kc2_l20/LOG.out"
logfile_dlnr=basedir+"dlnrho_fourmoms/kc2_l20/LOG.out"

nlat=512
knyq=nlat/2

c1=np.genfromtxt(logfile_phi,usecols=[0,1])
c2=np.genfromtxt(logfile_dphi,usecols=[0,1])
c3=np.genfromtxt(logfile_lnr,usecols=[0,1])
c4=np.genfromtxt(logfile_dlnr,usecols=[0,1])
tvals=c1[:,0]
avals=c1[:,1]

numt1=len(c1[:,0])
numt2=len(c2[:,0])
numt3=len(c3[:,0])
numt4=len(c4[:,0])

datacols=[0,1,4,5,8,9]
a=np.genfromtxt(momfile_phi,usecols=datacols)
b=np.genfromtxt(momfile_dphi,usecols=datacols)
c=np.genfromtxt(momfile_lnr,usecols=datacols)
d=np.genfromtxt(momfile_dlnr,usecols=datacols)

numk_phi=np.reshape(a[:,0],(numt1,-1))
kvals_phi=np.reshape(a[:,1],(numt1,-1))
r2_phi=np.reshape(a[:,2],(numt1,-1))
r4_phi=np.reshape(a[:,4],(numt1,-1))
i2_phi=np.reshape(a[:,3],(numt1,-1))
i4_phi=np.reshape(a[:,5],(numt1,-1))

r2_dphi=np.reshape(b[:,2],(numt2,-1))
r4_dphi=np.reshape(b[:,4],(numt2,-1))
i2_dphi=np.reshape(b[:,3],(numt2,-1))
i4_dphi=np.reshape(b[:,5],(numt2,-1))

r2_lnr=np.reshape(c[:,2],(numt3,-1))
r4_lnr=np.reshape(c[:,4],(numt3,-1))

r2_dlnr=np.reshape(d[:,2],(numt4,-1))
r4_dlnr=np.reshape(d[:,4],(numt4,-1))

mycmap=plt.cm.OrRd
tplots=[400,450,500,550,600,650]
for i in tplots:
    tfields=2*i  # adjust sampling frequency
    plt.plot(kvals_phi[i][0:knyq],r4_phi[i][0:knyq]/r2_phi[i][0:knyq]**2-3.,'b',label=r'$a\phi$')
    plt.plot(kvals_phi[0][0:knyq],r4_lnr[i][0:knyq]/r2_lnr[i][0:knyq]**2-3.,'r--',label=r'$\ln(\rho/\bar{\rho})$')
    plt.plot(kvals_phi[0][0:knyq],r4_dlnr[i][0:knyq]/r2_dlnr[i][0:knyq]**2-3.,'g-.',label=r'$\partial_t\ln(\rho/\bar{\rho})$')
#    plt.plot(kvals_phi[i][0:knyq],r4_chi[i][0:knyq]/r2_chi[i][0:knyq]**2-3.,'r--',label=r'$a^2\dot{\phi}$')
    plt.xlabel(r'$k/m$',fontsize=20)
    plt.ylabel(r'$\frac{\langle|Re(f)_k|^4\rangle}{\langle|Re(f)_k|^2\rangle^2}-3$',fontsize=20)
    plt.xlim(kvals_phi[0][1],kvals_phi[0][knyq])
    plt.ylim(-1.,4.)
    plt.text(60.,-0.5,r'$mt='+str(tvals[i])+'$',fontsize=20)
    plt.legend()

    print c1[tfields,0],c2[tfields,0],c3[i,0],c4[i,0]
    outfile="kurtosis_l4_t"+str(tvals[i])+".pdf"
    plt.savefig(outfile)
    plt.show()

# To do, put in ubiased estimators here and also combine low kmodes
def bin_lowk():
    return

plt.contourf(times[:,0:knyq],kvals[:,0:knyq],r4[:,0:knyq]/r2[:,0:knyq]**2-3.,cmap=plt.cm.OrRd)
