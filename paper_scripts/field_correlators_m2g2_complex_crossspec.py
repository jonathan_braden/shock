#!/usr/bin/env python
import sys
print sys.argv
showPreview = (sys.argv[1] == 'True')
print "preview plots is ", showPreview, " for ",sys.argv[0]

import numpy as np
import matplotlib.pyplot as plt
import mymathutils as mym

def plot_components(itime):
    for i in range(len(spectra)):
        plt.plot( kvals[itime],np.abs(spectra[i][itime]) )
    plt.xscale('log')
    plt.yscale('log')
    return

#
# Plot the cross-correlators normalized to the fluctuations in the fields
#
def plot_crosscorrelations_normalized(itime,time):
    plt.plot( kvals[itime], spectra[4][itime]/spectra[0][itime]**0.5/spectra[2][itime]**0.5, label=r'$\Delta_{\phi\dot{\phi}}$')
    plt.plot( kvals[itime], spectra[5][itime]/spectra[1][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\chi\dot{\chi}}$')
    plt.plot( kvals[itime], spectra[6][itime]/spectra[0][itime]**0.5/spectra[1][itime]**0.5, label=r'$\Delta_{\phi\chi}$')
    plt.plot( kvals[itime], spectra[8][itime]/spectra[0][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\phi\dot{\chi}}$')
    plt.plot( kvals[itime], spectra[9][itime]/spectra[1][itime]**0.5/spectra[2][itime]**0.5, label=r'$\Delta_{\dot{\phi}\chi}$')
    plt.plot( kvals[itime], spectra[7][itime]/spectra[2][itime]**0.5/spectra[3][itime]**0.5, label=r'$\Delta_{\dot{\phi}\dot{\chi}}$')

    plt.xscale('linear')
    plt.yscale('linear')
    plt.ylim(-1,1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$\Delta_{ij}$')

    tlabel=r'$mt='+str(time)+'$'
    plt.text(kvals[0][nlat/20],0.75,tlabel)  # adjust as needed depending on choice of scales

    plt.legend()
    return

def plot_determinants(itime):
    plt.plot( kvals[itime], np.log(nphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi}})$')
    plt.plot( kvals[itime], np.log(nchi[itime]), label=r'$\log(P_{\chi\chi}P_{\dot{\chi}\dot{\chi}})$')
    plt.plot( kvals[itime], np.log(detphi[itime]), label=r'$\log(P_{\phi\phi}P_{\dot{\phi}\dot{\phi} - P_{\phi\dot{\phi}}^2)$')
    plt.plot( kvals[itime], np.log(detchi[itime]), label=r'$\log(P_{\chi\chi}P_{\dot{\chi}\dot{\chi} - P_{\chi\dot{\chi}}^2)$')

    corr = [ [ spectra[0][itime], spectra[4][itime], spectra[6][itime], spectra[8][itime] ],
             [ spectra[4][itime], spectra[2][itime], spectra[9][itime], spectra[7][itime] ],
             [ spectra[6][itime], spectra[9][itime], spectra[1][itime], spectra[5][itime] ],
             [ spectra[8][itime], spectra[7][itime], spectra[5][itime], spectra[3][itime] ]
           ]
    corr=np.array(corr)
    npart=[]
    for i in range(len(corr[0][0])):
        npart.append(np.linalg.det(corr[:,:,i]))

    plt.plot( kvals[itime], np.log(npart), label=r'$n_k$' )
    plt.plot( kvals[itime], np.log(detphi[itime])+np.log(detchi[itime]), label='sum')

    plt.legend()
    return

def compute_crossspec_norm():
    global phi_phidot
    phi_phidot = cov_mat[0][1]/cov_mat[0][0]**0.5/cov_mat[1][1]**0.5
    global chi_chidot
    chi_chidot = cov_mat[2][3]/cov_mat[2][2]**0.5/cov_mat[3][3]**0.5
    global phi_chi
    phi_chi = cov_mat[0][2]/cov_mat[0][0]**0.5/cov_mat[2][2]**0.5
    global phi_chidot
    phi_chidot = cov_mat[0][3]/cov_mat[0][0]**0.5/cov_mat[3][3]**0.5
    global phidot_chi
    phidot_chi = cov_mat[1][2]/cov_mat[1][1]**0.5/cov_mat[2][2]**0.5
    global phidot_chidot
    phidot_chidot = cov_mat[1][3]/cov_mat[1][1]**0.5/cov_mat[3][3]**0.5

#To do: add dk part
def compute_entropies(kcut):
    global s_phi, ds_phi
    s_phi=[]
    global s_chi, ds_chi
    s_chi=[]
    global s_detphi, ds_detphi
    s_detphi=[]
    global s_detchi, ds_detchi
    s_detchi=[]
    global s_dettot, ds_dettot
    s_dettot=[]

    dk = kvals[0][1]-kvals[0][0]
    for kmax in kcut:
        kvol=dk*np.sum(kvals[0][1:kmax]**2)
        s_phi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(nphi[:,1:kmax]),axis=1) )
        s_chi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(nchi[:,1:kmax]),axis=1) )
        s_detphi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(detphi[:,1:kmax]),axis=1) )
        s_detchi.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(detchi[:,1:kmax]),axis=1) )
        s_dettot.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(dettot[:,1:kmax]),axis=1) )

    s_phi=np.array(s_phi)
    ds_phi=np.diff(s_phi,axis=1)
    s_chi=np.array(s_chi)
    ds_chi=np.diff(s_chi,axis=1)
    s_detphi=np.array(s_detphi)
    ds_detphi=np.diff(s_detphi,axis=1)
    s_detchi=np.array(s_detchi)
    ds_detchi=np.diff(s_detchi,axis=1)
    s_dettot=np.array(s_dettot)
    ds_dettot=np.diff(s_dettot,axis=1)
    
    return

def plot_entropy_differences(kind):
    plt.plot(times,s_phi[kind]-s_detphi[kind],label=r'$S_{n_{\phi}}-S_{\Delta_{\phi}}$')
    plt.plot(times,s_chi[kind]-s_detchi[kind],label=r'$S_{n_{\chi}}-S_{\Delta_{\chi}}$')
    plt.plot(times,s_detphi[kind]+s_detchi[kind]-s_dettot[kind],label=r'$S_{\Delta_{\phi}}+S_{\Delta_{\chi}} - S_{\Delta_{tot}}$')
    
    plt.yscale('log')
    plt.xlim(0.,500.)
    plt.xlabel(r'$mt$',fontsize=20)
    plt.ylabel(r'$\Delta S$',fontsize=20)
    plt.legend(fontsize=20)

    tlabel=r'$\frac{k_{cut}}{m}='+str(kvals[kcuts[kind]])+'$'
#    plt.text(,0.75,tlabel,fontsize=20)

def compute_crossentropies(kcut):
    global s_cross
  
    dk=kvals[0][1]-kvals[0][0]
    s_cross=[]
    for kmax in kcut:
        kvol=dk*np.sum(kvals[0][1:kmax]**2)
        s_tmp=[]
        s_tmp.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(1.-np.abs(phi_phidot[:,1:kmax])**2),axis=1) )
        s_tmp.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(1.-np.abs(chi_chidot[:,1:kmax])**2),axis=1) )
        s_tmp.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(1.-np.abs(phi_chi[:,1:kmax])**2),axis=1) )
        s_tmp.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(1.-np.abs(phi_chidot[:,1:kmax])**2),axis=1) )
        s_tmp.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(1.-np.abs(phidot_chi[:,1:kmax])**2),axis=1) )
        s_tmp.append( (0.5*dk/kvol)*np.sum(kvals[:,1:kmax]**2*np.log(1-np.abs(phidot_chidot[:,1:kmax])**2),axis=1) )
        s_cross.append(s_tmp)        
    return

# User modified specifications for the files to make the graph
#basedir="/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/fields/L10/kc2_nomean/"
basedir="/mnt/scratch-lustre/jbraden/crossspec/m2g2/n512/fields/L5/kc2_nomean/"
infile=basedir+'PSD'
logfile=basedir+'LOG.out'
nlat = 512

numk=int(3.**0.5*nlat/2)+2  # don't touch this

datacols=range(3,20)
a=np.genfromtxt(infile,usecols=datacols)
b=np.genfromtxt(logfile,usecols=[0,1])
times=b[:,0]
avals=b[:,1]
numt=len(times)

kvals=np.reshape(a[:,0],(numt,-1))

auto_spectra=[]
for i in range(1,5):
    auto_spectra.append( np.reshape(a[:,i],(numt,-1)) )

cross_spectra=[]
for i in range(5,len(a[0]),2):
    cross_spectra.append( np.reshape(a[:,i],(numt,-1)) + 1j*np.reshape(a[:,i+1],(numt,-1)) )

# Now fill up the covariance matrix (edit this as required by input file)
cov_mat=[ [ auto_spectra[0], cross_spectra[0], cross_spectra[2], cross_spectra[4] ],
          [ np.conjugate(cross_spectra[0]), auto_spectra[2], cross_spectra[5], cross_spectra[3] ],
          [ np.conjugate(cross_spectra[2]), np.conjugate(cross_spectra[5]), auto_spectra[1], cross_spectra[1] ],
          [ np.conjugate(cross_spectra[4]), np.conjugate(cross_spectra[3]), np.conjugate(cross_spectra[1]), auto_spectra[3] ] ]
cov_mat=np.array(cov_mat)

def compute_determinants():
    global nphi
    nphi=cov_mat[0][0]*cov_mat[1][1]
    global nchi
    nchi = cov_mat[2][2]*cov_mat[3][3]
    global detphi
    detphi=nphi - np.abs(cov_mat[0][1])**2
    global detchi
    detchi=nchi - np.abs(cov_mat[2][3])**2
    global dettot
    dettot=[]
    for tstep in range(len(cov_mat[0][0])):
        corr = cov_mat[:,:,tstep,:]
        npart=[]
        for kstep in range(len(corr[0][0])):
            npart.append(np.linalg.det(corr[:,:,kstep]))
        dettot.append(npart)
    dettot=np.array(dettot)   

compute_determinants()

kcuts=[63,127,191,255]
kvols=[]
dk=kvals[0][1]-kvals[0][0]
for kc in kcuts:
    kvols.append(np.sum(kvals[0][1:kc]**2)*dk)

compute_entropies(kcuts)

##########################
# Now start making plots #
##########################

def plot_determinants_normed(itime,tnorm,legend):
    plt.plot( kvals[itime], nphi[itime]/nphi[tnorm], 'b', label=r'$P_{\phi\phi}P_{\Pi_{\phi}\Pi_{\phi}}$', linewidth=1.5)
    plt.plot( kvals[itime], nchi[itime]/nchi[tnorm], 'r', label=r'$P_{\chi\chi}P_{\Pi_{\chi}\Pi_{\chi}}$',linewidth=1.5)
    plt.plot( kvals[itime], detphi[itime]/detphi[tnorm], 'b--',markersize=5., label=r'$\Delta^2_{\phi}$',linewidth=1.5)
    plt.plot( kvals[itime], detchi[itime]/detchi[tnorm], 'r--',markersize=5., label=r'$\Delta^2_{\chi}$',linewidth=1.5)
    plt.plot( kvals[itime], np.abs(dettot[itime])/np.abs(dettot[tnorm]), 'g',label=r'$\Delta^2_{tot}$',linewidth=1.5)

    if (legend):
        plt.legend(bbox_to_anchor=(0,0,1.05,1.1))

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.ylim(ymin=0.1)
    plt.xscale('log')
    plt.yscale('log')
    plt.xlabel(r'$k/m$')
    plt.ylabel(r'$F(k,t)/F(k,t=0)$')
    plt.text(kvals[0][2],0.8,r'$mt={:.1f}$'.format(times[itime]),horizontalalignment='left',verticalalignment='top')
    return

#tplots=[200,300,400,420,440,450,480,490,500,1200,2000]
tplots=[500]
# Now make all of the desired plots (edit this as needed)
for tcur in tplots:
    plot_determinants_normed(tcur,0,True)
    plt.ylim(1.e-6,1.e40)
#    plt.subplots_adjust(bottom=0.14,left=0.17)
    fig=plt.gcf()
    fig.tight_layout()
    plt.savefig('det_spec_norm_t{:.1f}.pdf'.format(times[tcur]))
    if showPreview:
        plt.show()
    plt.clf()

tnorm=0
kmax=nlat/2
dk=kvals[0][1]-kvals[0][0]
for i in range(0,1200,20):
    plt.plot(kvals[i,1:kmax],(kvals[i][1:kmax]**2*dk/kvals[i][kmax]**3)*np.log(detphi[i,1:kmax]/detphi[tnorm,1:kmax]))
plt.xlim(kvals[0,1],kvals[0,kmax])
#plt.ylim(0.1,1.e20)
plt.xscale('log')
plt.yscale('log')
plt.xlabel(r'$k/m$')
plt.ylabel(r'$\ln(P_{\phi\phi}P_{\Pi_\phi\Pi_\phi})$')
plt.tight_layout()
if showPreview:
    plt.show()
plt.clf()

plt.contourf(kvals[0,1:kmax],times,kvals[:,1:kmax]**2*dk*np.log(detphi[:,1:kmax]/detphi[0,1:kmax]),cmap=plt.cm.OrRd)
plt.xlabel(r'$k/m$')
plt.ylabel(r'$mt$')
cb=plt.colorbar()
cb.set_label(r'$2\pi L^{-1}k^2\ln(\Delta_\phi/\Delta_\phi(t=0)$')
fig=plt.gcf()
fig.tight_layout()
fig.savefig('detphi_tevolve_m2g2.pdf')
fig.savefig('detphi_tevolve_m2g2.png')
if showPreview:
    plt.show()
plt.clf()

#### To do, use a pseudocolor plot to see if it looks nicer
#plt.pcolor(kvals[0,1:kmax],times,kvals[:,1:kmax]**2*dk*np.log(detphi[:,1:kmax]/detphi[0,1:kmax]),cmap=plt.cm.OrRd)
#plt.xlabel(r'$k/m$')
#plt.ylabel(r'$mt$')
#cb=plt.colorbar()
#cb.set_label(r'$2\pi L^{-1}k^2\ln(\Delta_\phi/\Delta_\phi(t=0)$')
#plt.tight_layout()
#plt.savefig('detphi_tevolve_m2g2_pcolor.png')
#if showPreview:
#    plt.show()
#plt.clf()

plt.contourf(kvals[0,1:kmax],times,kvals[:,1:kmax]**2*dk*np.log(detchi[:,1:kmax]/detchi[0,1:kmax]),cmap=plt.cm.OrRd)
plt.xlabel(r'$k/m$')
plt.ylabel(r'$mt$')
cb=plt.colorbar()
cb.set_label(r'$2\pi L^{-1}k^2\ln(\Delta_\chi/\Delta_\chi(t=0)$')
#plt.tight_layout()
plt.savefig('detchi_tevolve_m2g2.pdf')
plt.savefig('detchi_tevolve_m2g2.png')
if showPreview:
    plt.show()
plt.clf()

plt.contourf(kvals[0,1:kmax],times,kvals[:,1:kmax]**2*dk*np.log(dettot[:,1:kmax]/dettot[0,1:kmax]),cmap=plt.cm.OrRd)
plt.xlabel(r'$k/m$')
plt.ylabel(r'$mt$')
cb=plt.colorbar()
cb.set_label(r'$2\pi L^{-1}k^2\ln(\Delta_{tot}/\Delta_{tot}(t=0))$')
fig=plt.gcf()
#fig.tight_layout()
fig.savefig('dettot_tevolve_m2g2.pdf')
fig.savefig('dettot_tevolve_m2g2.png')
if showPreview:
    plt.show()
plt.clf()

#######################################
# Plot contributions of cross-spectra #
#######################################
compute_crossspec_norm()
compute_crossentropies(kcuts)
def plot_cross_entropies_wsmooth(kc_ind,wsize=21):
    kmax_ind=kcuts[kc_ind]
    tcut=len(times)-1
    plt.plot(times,-s_cross[kc_ind][0],'g',alpha=0.25)
    plt.plot(times,-mym.smooth(s_cross[kc_ind][0],wsize),'g',label=r'$\Delta S_{\phi\dot{\phi}}$')

    plt.plot(times,-s_cross[kc_ind][1],'b',alpha=0.25)
    plt.plot(times,-mym.smooth(s_cross[kc_ind][1],wsize),'b',label=r'$\Delta S_{\chi\dot{\chi}}$')

    plt.plot(times,-s_cross[kc_ind][2],'r',alpha=0.25)
    plt.plot(times,-mym.smooth(s_cross[kc_ind][2],wsize),'r',label=r'$\Delta S_{\phi\chi}$')
    
    plt.plot(times,-s_cross[kc_ind][3],'c',alpha=0.25)
    plt.plot(times,-mym.smooth(s_cross[kc_ind][3],wsize),'c',label=r'$\Delta S_{\phi\dot{\chi}}$')

    plt.plot(times,-s_cross[kc_ind][4],'m',alpha=0.25)
    plt.plot(times,-mym.smooth(s_cross[kc_ind][4],wsize),'m',label=r'$\Delta S_{\dot{\phi}\chi}$')

    plt.plot(times,-s_cross[kc_ind][5],'k',alpha=0.25)
    plt.plot(times,-mym.smooth(s_cross[kc_ind][5],wsize),'k',label=r'$\Delta S_{\dot{\phi}\dot{\chi}}$')

    plt.ylabel(r'$-\Delta S/\mathcal{N}_{eff}$')
    plt.xlabel(r'$mt$')
    plt.yscale('log')
    plt.legend(bbox_to_anchor=(0,0,1.05,1.1))
    plt.text(0.1*times[tcut],0.5,r'$m^{-1}k_{cut}='+'{:.3f}'.format(kvals[0][kmax_ind])+'$',horizontalalignment='left',verticalalignment='top')
    plt.xlim(0,times[tcut])
    plt.ylim(1.e-6,1.)

def plot_cross_entropies(kc_ind):
    kmax_ind=kcuts[kc_ind]
    tcut=len(times)-1

    plt.plot(times,-s_cross[kc_ind][0],'g',label=r'$\Delta S_{\phi\dot{\phi}}$')
    plt.plot(times,-s_cross[kc_ind][1],'b',label=r'$\Delta S_{\chi\dot{\chi}}$')
    plt.plot(times,-s_cross[kc_ind][2],'r',label=r'$\Delta S_{\phi\chi}$')
    plt.plot(times,-s_cross[kc_ind][3],'c',label=r'$\Delta S_{\phi\dot{\chi}}$')
    plt.plot(times,-s_cross[kc_ind][4],'m',label=r'$\Delta S_{\dot{\phi}\chi}$')
    plt.plot(times,-s_cross[kc_ind][5],'k',label=r'$\Delta S_{\dot{\phi}\dot{\chi}}$')
    plt.ylabel(r'$-\Delta S/N_{eff}$')
    plt.xlabel(r'$mt$')
    plt.yscale('log')
    plt.legend(bbox_to_anchor=(0,0,1.05,1.1))
    plt.text(0.1*times[tcut],0.5,r'$m^{-1}k_{cut}='+'{:.3f}'.format(kvals[0][kmax_ind])+'$',horizontalalignment='left',verticalalignment='top')
    plt.xlim(0,times[tcut])
    plt.ylim(1.e-6,1.)

plot_cross_entropies(3)
plt.tight_layout()
if showPreview:
    plt.show()
plt.clf()

plot_cross_entropies_wsmooth(3,25)
plt.tight_layout()
plt.savefig('cross_entropies_fields_m2g2.pdf')
if showPreview:
    plt.show()
plt.clf()


def plot_crosscorrelations_normalized_abs(itime,time):
    fig, axi = plt.subplots(nrows=2,sharex=True,sharey=True)
    axi[1].plot( kvals[itime], np.abs(cov_mat[0][1][itime])/cov_mat[0][0][itime]**0.5/cov_mat[1][1][itime]**0.5, 'g', label=r'$|C_{\phi\dot{\phi}}|$', linewidth=1.5)
    axi[1].plot( kvals[itime], np.abs(cov_mat[2][3][itime])/cov_mat[2][2][itime]**0.5/cov_mat[3][3][itime]**0.5, 'b', label=r'$|C_{\chi\dot{\chi}}|$', linewidth=1.5)
    axi[0].plot( kvals[itime], np.abs(cov_mat[0][2][itime])/cov_mat[0][0][itime]**0.5/cov_mat[2][2][itime]**0.5, 'r', label=r'$|C_{\phi\chi}|$', linewidth=1.5)
    axi[0].plot( kvals[itime], np.abs(cov_mat[0][3][itime])/cov_mat[0][0][itime]**0.5/cov_mat[3][3][itime]**0.5, 'c',label=r'$|C_{\phi\dot{\chi}}|$', linewidth=1.5)
    axi[0].plot( kvals[itime], np.abs(cov_mat[1][2][itime])/cov_mat[1][1][itime]**0.5/cov_mat[2][2][itime]**0.5, 'm',label=r'$|C_{\dot{\phi}\chi}|$', linewidth=1.5)
    axi[0].plot( kvals[itime], np.abs(cov_mat[1][3][itime])/cov_mat[1][1][itime]**0.5/cov_mat[3][3][itime]**0.5, 'k',label=r'$|C_{\dot{\phi}\dot{\chi}}|$', linewidth=1.5)

    for i in range(len(axi)):
        axi[i].legend(loc='upper right',bbox_to_anchor=(0,0,1.05,1.05))
        axi[i].set_ylabel(r'$|C_{\alpha\beta}|$')
        axi[i].set_xscale('log')

    plt.yscale('linear')
    plt.ylim(-0.1,1.1)

    plt.xlim(kvals[itime][1],kvals[itime][nlat/2])
    plt.xlabel(r'$k/m$')
#    plt.ylabel(r'$|C_{ij}|$',fontsize=30)

    tlabel=r'$mt='+str(time)+'$'
    axi[0].text(kvals[0][2],1.,tlabel,horizontalalignment='left',verticalalignment='top')
    plt.subplots_adjust(hspace = 0.)
    plt.tight_layout()
    return

for tcur in tplots:
   plot_crosscorrelations_normalized_abs(tcur,times[tcur])
   plt.savefig('crosscorrelations_m2g2_t{:.1f}.pdf'.format(times[tcur]))
   if showPreview:
       plt.show()
   plt.clf()

ent_color=['b','r','c','m','g']
def plot_entropies_wsmooth(kc_ind,wsize=21,no_sm=True,sm_plot=True):
    if no_sm:
        plt.plot(times,s_phi[kc_ind],ent_color[0],alpha=0.25)
        plt.plot(times,s_chi[kc_ind],ent_color[1],alpha=0.25)
        plt.plot(times,s_detphi[kc_ind],ent_color[2],alpha=0.25)
        plt.plot(times,s_detchi[kc_ind],ent_color[3],alpha=0.25)
        plt.plot(times,s_dettot[kc_ind],ent_color[4],alpha=0.25)

    if sm_plot:    
        plt.plot(times,mym.smooth(s_phi[kc_ind],wsize),ent_color[0],label=r'$S_{n_\phi}$')
        plt.plot(times,mym.smooth(s_chi[kc_ind],wsize),ent_color[1],label=r'$S_{n_\chi}$')
        plt.plot(times,mym.smooth(s_detphi[kc_ind],wsize),ent_color[2],label=r'$S_\phi$')
        plt.plot(times,mym.smooth(s_detchi[kc_ind],wsize),ent_color[3],label=r'$S_\chi$')
        plt.plot(times,mym.smooth(s_dettot[kc_ind],wsize),ent_color[4],label=r'$S_{tot}$')

    plt.legend(bbox_to_anchor=(0,0,1.05,1.1))
    plt.xlabel(r'$mt$')
    plt.ylabel(r'$S/N_{eff}$')

def plot_entropy_derivs(kc_ind,wsize=21,no_sm=True,sm_plot=True):
    tave=0.5*(times[:len(times)-1]+times[1:])
    if no_sm:
        plt.plot(tave,ds_phi[kc_ind]/np.diff(times),ent_color[0],alpha=0.25)
        plt.plot(tave,ds_chi[kc_ind]/np.diff(times),ent_color[1],alpha=0.25)
        plt.plot(tave,ds_detphi[kc_ind]/np.diff(times),ent_color[2],alpha=0.25)
        plt.plot(tave,ds_detchi[kc_ind]/np.diff(times),ent_color[3],alpha=0.25)
        plt.plot(tave,ds_dettot[kc_ind]/np.diff(times),ent_color[4],alpha=0.25)

    if sm_plot:
        plt.plot(tave,mym.smooth(ds_phi[kc_ind],wsize)/np.diff(times),ent_color[0],label=r'$dS_{n_\phi}/dt$')
        plt.plot(tave,mym.smooth(ds_chi[kc_ind],wsize)/np.diff(times),ent_color[1],label=r'$dS_{n_\chi}/dt$')
        plt.plot(tave,mym.smooth(ds_detphi[kc_ind],wsize)/np.diff(times),ent_color[2],label=r'$dS_\phi/dt$')
        plt.plot(tave,mym.smooth(ds_detchi[kc_ind],wsize)/np.diff(times),ent_color[3],label=r'$dS_\chi/dt$')
        plt.plot(tave,mym.smooth(ds_dettot[kc_ind],wsize)/np.diff(times),ent_color[4],label=r'$dS_{tot}/dt$')

        plt.xlabel(r'$mt$')
        plt.ylabel(r'$\mathcal{N}_{eff}^{-1}dS/dt$')
        plt.legend(bbox_to_anchor=(0,0,1.1,1.1))
        plt.text(100,0.022,r'$m^{-1}k_{cut}='+'{:.1f}$'.format(kvals[0][kcuts[kc_ind]]))

kind=3
plot_entropy_derivs(kind,51,True,True)
plt.ylim(0,0.025)
plt.tight_layout()
plt.savefig('ent_deriv_fields_m2g2.pdf')
if showPreview:
    plt.show()
plt.clf()

tnorm_ind=0
ltypes=['-','--','-.',':']
kind=0
plt.plot(times,s_dettot[kind]-s_dettot[kind][tnorm_ind],'r'+ltypes[kind],label=r'$S_{tot}$')
plt.plot(times,s_detphi[kind]-s_detphi[kind][tnorm_ind],'g'+ltypes[kind],label=r'$S_{\phi}$')
plt.plot(times,s_detchi[kind]-s_detchi[kind][tnorm_ind],'b'+ltypes[kind],label=r'$S_{\chi}$')
for i in range(1,len(kcuts)):
    plt.plot(times,s_dettot[i]-s_dettot[i][tnorm_ind],'r'+ltypes[i])
    plt.plot(times,s_detphi[i]-s_detphi[i][tnorm_ind],'g'+ltypes[i])
    plt.plot(times,s_detchi[i]-s_detchi[i][tnorm_ind],'b'+ltypes[i])
plt.xlabel(r'$mt$')
plt.ylabel(r'$(S-S_{init})/\mathcal{N}_{eff}$')
plt.xlim(times[0],times[len(times)-1])
plt.legend(loc='upper right', bbox_to_anchor=(0,0,1.05,1.1) )
plt.tight_layout()
plt.savefig('field_entropies_m2g2_varykcut.pdf')
if showPreview:
    plt.show()
plt.clf()

kind=3
plt.plot(times,s_detphi[kind]+s_detchi[kind]-s_dettot[kind],'b',label=r'$S_\phi+S_\chi-S_{tot}$',linewidth=1.5)
plt.plot(times,s_phi[kind]-s_detphi[kind],'g',label=r'$S_{n_\phi}-S_\phi$',linewidth=1.5)
plt.plot(times,s_chi[kind]-s_detchi[kind],'r',label=r'$S_{n_\chi}-S_\chi$',linewidth=1.5)
plt.ylabel(r'$\Delta S/N_{eff}$')
plt.xlabel(r'$mt$')
plt.xlim(times[0],times[len(times)-1])
plt.ylim(-0.001,0.003)
plt.legend(loc='upper right',bbox_to_anchor=(0,0,1.05,1.1))
plt.text(times[100],-0.0008,r'$m^{-1}k_{cut}='+'{:.1f}$'.format(kvals[0][kcuts[kind]]),verticalalignment='bottom')
plt.tight_layout()
plt.savefig('entdiff_fields_m2g2.pdf')
if showPreview:
    plt.show()
plt.clf()

tcut=len(times)
for i in [5,9,50,200]:
    plt.plot(avals[:tcut],cov_mat[2,2,:tcut,i]/avals[:tcut],label=r'$k/m={:.1f}$'.format(kvals[0][i]))
plt.yscale('log')
plt.xlim(avals[0],avals[tcut-1])
plt.ylabel(r'$a^2P_{\chi\chi}$')
plt.xlabel(r'$a/a_{end}$')
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('chi_kmodes_tevolve_m2g2.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in [5,9,50,200]:
    plt.plot(avals[:tcut],cov_mat[0,0,:tcut,i]/avals[:tcut],label=r'$k/m={:.1f}$'.format(kvals[0][i]))
plt.yscale('log')
plt.xlim(avals[0],avals[tcut-1])
plt.ylabel(r'$a^2P_{\phi\phi}$')
plt.xlabel(r'$a/a_{end}$')
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('phi_kmodes_tevolve_m2g2.pdf')
if showPreview:
    plt.show()
plt.clf()

for i in [5,9,50,200]:
    plt.plot(avals[:tcut],avals[:tcut]*cov_mat[1,1,:tcut,i],label=r'$k/m={:.1f}$'.format(kvals[0][i]))
plt.yscale('log')
plt.ylabel(r'$a^{-2}P_{\Pi_\phi\Pi_\phi}$')
plt.xlabel(r'$a/a_{end}$')
plt.xlim(avals[0],avals[tcut-1])
plt.legend(loc='lower right')
plt.tight_layout()
plt.savefig('dphi_kmodes_tevolve_m2g2.pdf')
if showPreview:
    plt.show()
plt.clf()

#j=3
#plt.plot(s_phi[j]-s_detphi[j],label=r'$\phi$')
#plt.plot(s_chi[j]-s_detchi[j],label=r'$\chi$')
#plt.plot(-s_dettot[j]+s_detphi[j]+s_detchi[j],label=r'$tot$')

#j=2
#plt.plot(s_detphi,label=r'$\phi$')
#plt.plot(s_detchi,label=r'$\chi$')
#plt.plot(s_dettot,label=r'$tot$')
