#!/usr/bin/env python

fig_sizes=[0.32,0.45,0.6]
text_size=442.29314

golden_ratio=0.5*(5.**0.5-1.)
inches_per_pt = 1.0 / 72.27

for sz in fig_sizes:
    fig_width=text_size*sz
    fig_height=fig_width*golden_ratio
    print sz, "width (inches) = ",fig_width*inches_per_pt,"height (inches) = ",fig_height*inches_per_pt
