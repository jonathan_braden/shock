%\documentclass[10pt, letterpaper, twocolumn]{article}
%\documentclass[aps,preprint,showpacs,groupedaddress]{revtex4}
\documentclass[aps,prl,twocolumn,letterpaper,showpacs,groupedaddress]{revtex4}

\usepackage{amsmath, amssymb}
\usepackage{graphics, graphicx}
\usepackage[small,bf]{caption}

\graphicspath{{figs/}}
\renewcommand{\figurename}{Fig.}

\begin{document}

\title{Preheating: A Shock-in-Time}
\author{J. Richard Bond$^{1}$}
\author{Jonathan Braden$^{1,2}$}
\affiliation{$^{1}$CITA, University of Toronto, 60 St. George Street, Toronto, ON, Canada M5S 1A7}
\affiliation{$^{2}$Dept. of Physics, University of Toronto, 60 St. George Street, Toronto, ON, Canada}

\date{\today}

\begin{abstract}
We study the production of entropy during resonant preheating after $m^2\phi^2$ inflation.
There is a sharp spike in the entropy production rate when the fields first become nonlinear
which we have dubbed a {\it shock-in-time} in analogy with the entropy produced by a shock
wave moving through a medium.
This spike coincides with the onset of nonlinearities in the system, and thus also an abrupt
change in the equation of state.
As a result, the overall expansion history of the universe depends on the scale factor at
which the shock-in-time occurs.
We then make some preliminary investigations of the importance of this for modulated preheating.
\end{abstract}

\maketitle

\section{Introduction}
During much of its early history, the universe was in a state of local thermal equilibrium (LTE),
specified by the values of a few conserved quantities
such as the energy density and baryon density (we are here ignoring the evolution of the gravitational field).
The resulting dynamics is thus rather insensitive to the underlying physics,
and interesting signatures tend to be wiped away.
In contrast, nonequilibrium phenomena tend to be much more dependent on the underlying theory and state of the universe.
This opens the possibility of observable of imprints from the nonequilibrium evolution, which provide a probe of the underlying physics.
For this reason, nonequilibrium epochs are among the most interesting in cosmology.
Of course, the challenge is to find signatures which aren't destroyed by subsequent epochs of LTE.

A common feature of nonequilibrium dynamics is the production of entropy as the system attempts to return to equilibrium.
One expects that the more violent the nonequilibrium evolution is the greater the entropy production rate will be,
although this does not have to be true in general.
We might also expect that the more violent the dynamics, the more likely it is to leave an observable trace.
Therefore, tracking the evolution of entropy in the universe allows us to identify epochs which could potentially leave observable imprints on the cosmos.
Here we will consider preheating~\cite{Kofman:1997yn}, the first highly nonequilibrium epoch in the post inflationary universe.
Past studies have considered many interesting signatures from preheating such as baryogenesis, gravitational waves, topological defect production, and nongaussianities.
Our focus will be on the entropy production, with an eye to connecting it to nongaussianities.
Due to the nonlinearity that develops, we must approach this problem with numerical simulations.
We find that the onset of nonlinearities is accompanied by a sharp spike in entropy production, 
for which we have coined the term {\it shock-in-time}.
This moment leaves an imprint on the expansion history of the universe which we consider from the viewpoint of a jump in a conserved charge associated with our shock.
We then apply this to a simple model of modulated preheating and find modulations in the shock time that could produce observationally interesting curvature perturbations.

\section{Nonequilbrium Entropy}
We start by defining our notion of nonequilibrium entropy.
For a classical random field with probability density functional $P[f]$, the appropriate definition is the Shannon entropy (the analogue of the von Neumann entropy)
\begin{equation}
  S_{Shannon} = -\mathrm{Tr}(P[f]\ln P[f]) \, ,
\end{equation}
where $\mathrm{Tr}$ stands for a functional integration over the fields $f$.
Unfortunately, we can only compute this quantity if we know the full $P[f]$, which is a rather duanting task.
As well, the above only applies for an inherently stochastic system.
The only randomness in our simulations comes from the choice of a particular realization of the initial field fluctuations.
Once this is done, the subsequent evolution is unitary (up to numerical noises) and strictly speaking should not lead to any entropy production.

We resolve both of these difficulties by recalling that any definition of entropy is accompanied by a corresponding notion of coarse-graining.
Here, we will employ the maximum entropy principle
and define the entropy of a field $f$  as the maximal value of the Shannon entropy over the space of all $P[f]$'s, 
subject to a set of constraints.
Specifically, we will constrain our field to have vanishing mean and the 2-point correlation function measured in our simulations.
For this set of constraints, it is well known that the Shannon entropy is maximized by the multivariate Gaussian with covariance matrix given by the constraints.
The resulting entropy is
\begin{equation}
  \frac{S_{gaussian}}{N} = \frac{1}{2} + \frac{1}{2}\ln(2\pi) + \frac{1}{2N}\sum_{{\bf k}}\ln P(k)
\end{equation}
where $N$ is the number of degrees of freedom of the field (the number of lattice sites for a discretized field) and $P(k)$ are the eigenvectors of the covariance matrix $\langle f_if_j\rangle$.
For a homogeneous field these are given by the power spectrum.
In the above expression, the $\ln(2\pi)$ term results from the entropy of the uniformly distributed angles of the Fourier coefficients,
while the remaining terms are associated with the entropy of fluctuations in the moduli of these coefficients.

This maximum entropy procedure is familiar from classical statistical mechanics where one considers an ensemble of physical systems
subject to various statistical constraints.
If we allow the members of the ensemble to exchange energy (the canonical ensemble) but fix the expectation value of the energy,
then Shannon entropy is maximized by the Boltzmann distribution. 
Similar considerations return the the probability distributions associated with the microcanonical and grand canonical ensembles.
In this case the maximum entropy philosophy has actually been taken a step further and used to infer the underlying probability distribution,
not just to assign an entropy to the system.
We will not take this additional step here.


We can also put our procedure into the language of a system-environment split.
Let's define our field theory in terms of its n-point correlation functions (and potentially some additional information escaping the correlation hierarchy).
We then split the full theory into a system and environment, with the system defined as the one and two-point correlators.
In a non-linear theory, the time evolution of these correlators depends on higher order correlation functions,
so that our system couples to the environmental degrees of freedom.
As a result, even if the full state of the field theory is initially specified by only the first two correlators,
the interation will lead to the development of system-environment and environment-environment correlations.
Thus, information can be carried from the system into the environment, resulting in an increase in entropy of the system degrees of freedom.

Of course, the presence of additional constraints will modify the above result for the entropy.
An example would be if an inhomogeneous condensate forms (such as a collection topological defects).
The condensate indtroduces correlations between various Fourier modes, whose entropy is not properly determined by considering only the power spectrum.
Effectively, the condensate acts as a background field around which the field is constrained to fluctuate.
Despite this qualification, the primary result of this letter is the presence of a spike in the entropy production around the onset of nonlinearities in the fields.
This general feature should be robust even if a condensate forms and thus does not affect our main conclusions.
In the particular example considered here, the dominant contribution to the entropy comes from the largest k-modes (which are not part of some slowly varying condensate).
More generally, if we were to consider a model in which topological defects are produced, there will still be a rapid production of entropy at the moment the defects form, 
followed by additional production of entropy as the defects annihilate or decay.

%{\bf Say something about entanglement entropy, etc.}

%An illuminating analogy can also be made with particle dynamics and Boltzman's H-theorem.
%Consider $N$ interacting particles confined to a finite volume and colliding via short-range interations.
%For low particle densities, the interaction rates are small and we can approximate the collision integral with the lowest order terms describing interactions between only a handful of particles.
%Every time a group of particles interacts, their motions become correlated at later times, and as more collisions occur more and more correlations are created.
%As a result, information on the full $2N^3$ dimensional phase space becomes encoded in higher and higher order n-point functions.
%However, these high order correlations will be invisible to an observer who only has access to a few low-order correlation functions and as a result they will lose the information stored in the
%higher-order correlators and interpret this as entropy production.
%{\bf Estimate this here using some interaction rates, etc., write down forumla for dPk/dt}

\section{Preheating as a Shock-in-Time}
We now apply the above framework to preheating after chaotic inflation described by the Lagrangian
\begin{equation}
  \mathcal{L} = -\frac{1}{2}\partial_{\mu}\phi\partial^{\mu}\phi -\frac{1}{2}\partial_{\mu}\chi\partial^{\mu}\chi - \frac{m^2}{2}\phi^2 - \frac{g^2}{2}\phi^2\chi^2 
\end{equation}
evolving in the background $ds^2 = -dt^2 + a^2(t)d{\bf x}^2$ with the scale factor $a$ determined by
\begin{equation}
  H^2 = \left(\frac{\dot{a}}{a}\right)^2 = \frac{\bar{\rho}}{3m_{pl}^2} \, .
\end{equation}
Here $\bar{\rho} = -\langle T^0_0 \rangle$ with $\langle \cdot \rangle$ denoting a spatial average over the lattice
and $m_{pl} = \left(8\pi G\right)^{-1/2}$ is the reduced Planck mass.
$\phi$ is the inflaton field while $\chi$ is a massless field into which $\phi$ can decay which we will call the preheat field.
Although we restrict ourselves to this specific Lagrangian in this letter, 
our general conclusions hold in a much broader range of models. 

After inflation, the inflaton oscillates around the minimum of its potential with frequency $m$ and amplitude decaying as $a^{-3/2}$.
The preheating dynamics in this theory have been extensively studied (see {\it e.g.}~\cite{Kofman:1997yn}).
For $g^2m_{pl}^2/m^2 \gtrsim 10^3$, the dynamics proceeds through four stages.
First, a linear stage of resonance occurs during which bands of wavenumbers of the $\chi$ field grow exponentially
due to the oscillating $\phi$.
This continues until the effective occupation numbers of the $\chi$ field become of order the inverse coupling strength.
At this point, the fields become nonlinear and a short stage occurs during which wavenumbers across a range of scales are excited.
After this, the dyanamics slow down considerably and a slowly evolving nonthermal regime is entered.
Finally, the fields must eventually thermalize, or more realistically couplings to the standard model must cause decays of the scalars into the radiation bath that will start the hot big bang.

Due to the eventual nonlinearity of the fields, numerical techniques are required in order to solve for the dynamics.
Here, we make use of modified versions of the public codes DEFROST~\cite{Frolov:2008hy} and HLATTICE~\cite{Huang:2011gf} to perform our simulations.
We will take $\ln(\rho/3H^2)$ as our dynamical field.
We have chosen to work with a function of the energy density (rather than the underlying scalars)
because it is a well-defined measurable quantity, whereas the scalar field values are not.
In this sense, it is less sensitive to the underlying field theory model that we choose to study.
As well, we have chosen to work with the logarithm rather than the energy density itself because the 
energy density is an inherently positive quantity,
which would require an additional constraint to be added in our maximum entropy procedurue.
A final motivation is that 1-pt probability distribution of $\ln(\rho/\bar{\rho})$ is also nearly gaussian at early and late times~\cite{Frolov:2008hy}.

\begin{figure}[h]
%\includegraphics[width=0.95\linewidth]{shock}
\includegraphics[width=0.95\linewidth]{shock_variables_n512_L5_filter2}
\caption{Evolution of $\rho/\sqrt{\langle(\delta\rho)^2\rangle}$ ({\it top}) and
the entropy per mode $\ln(\rho/\bar{\rho})$ normalized to the initial entropy ({\it bottom}) for a single realization of the initial fluctuations.
The simulation used a $512^3$ lattice with $g^2m_{pl}^2/m^2 = 10^4$ and a time step of $mdt = 0.001$.
There is a precipitous drop in the ratio of the mean energy to its fluctuations at the time when the fields become nonlinear,
and a corresponding steplike feature in the entropy.
We have indicated this region with a yellow band and denote it as a {\it shock-in-time}.}
\label{fig:shock}
\end{figure}

Figure~\ref{fig:shock} shows the time evolution of the entropy along with $\bar{\rho}/\sqrt{\langle\delta\rho^2\rangle}$, which measures the degree of inhomogeneity in the system.
The most important feature is the sharp step in the entropy at the onset of nonlinearity (which coincides with the emergence of inhomogeneity).
Although the numerical value of the entropy is sensitive to our choice of cutoff in momentum space, 
the presence of the step is robust to changes in the cutoff.
Here we take $k_{cut}$ to be the Nyquist frequency.
The underlying reason for the sudden production of entropy is easy to understand heuristically.
During the linear stages of preheating, there are generally bands of wavenumbers that are unstable and grow exponentially fast.
This entropy growth is restricted by the rather small phase space of the growing modes.
However, the exponential growth eventually causes the fields to become strongly nonlinear.
At this point, simultaneous interactions between many different Fourier modes occur, and high order correlations are rapidly built up.
This results in an increase in entropy when we restrict ourselves to only studying the 2-point function as the information about the
system is transferred to the higher order correlators we are treating as an environment.

We now wish to use to above picture to make an analogy with shock fronts.
Suppose that we have medium with a propagating shock and an observer free to explore the medium who is equipped with a
device for measuring entropy.
If the observer is initially in front of the shock wave, then the readings will remain constant until
the shock hits them.
At this moment, there will be a jump in the entropy density, potentially followed by a much slower post-shock
evolution as the medium attempts to equilibrate itself.

In the cosmological context, rather than having an observer who is free to walk around the medium 
we instead would like to focus on a set of comoving observers.
Each member of this set must stay at a fixed spatial coordinate, but they are permitted to move (forward) in time.
When preheating occurs within their local Hubble patch, each of these observers sees a jump in the entropy.
In analogy with the case of a true shock, we will term this a {\it shock-in-time}.
Just as a shock can be viewed as a randomization front that produces entropy,
we can view preheating as a scrambling mechanism that takes the initially coherent energy density stored in the
inflaton and converts it into the incoherent energy of fluctuations.
As a caution, our use of the term shock-in-time does not imply that there is an actual shock front propagating through the cosmos.
In particular, the shock-in-time surface can be spacelike while the surface swept out by a propagating shock must be timelike.

%{\bf From the shock viewpoint in 1+1 dimensions, our shock surface is really like the nucleation of various shock fronts at various locations, followed by the propagation of two outgoing shock fronts, and finally pair annihilations of these shocks at some other location.  Is there any way that this point of view can be useful?  The nucleations occur for parameter values where non-linearity kicks in early, and they annihilate where it kicks in late.}

\section{Conserved Charges and Curvature Perturbations}
In addition to the production of entropy, shocks also produce jumps in the local value of hyperbolically conserved quantities.
As an analogue for our shock-in-time in time picture, consider $C(a) \equiv a^{3(1+w)}\bar{\rho}$ with $w=\bar{P}/\bar{\rho}$.
For any epoch with $w=\mathrm{const}$, $C$ is also a constant.
$C(a)$ is a particularly interesting quantity, since it allows us to extract perturbations to the expansion history evaluated on uniform energy density (Hubble) slices.
These can then be related to the comoving curvature perturbation via $\zeta \equiv \delta\ln(a)|_{H}$~\cite{Salopek:1990jq,Sasaki:1995aw}.
In figure~\ref{fig:cons_charge} we show the evolution of our charge $C(a)$ averaged over one oscillation of the background $\phi$ condensate.
As we would expect, there is a sharp jump at the shock-in-time, followed by a much slower decrease as the fields attempt to equilibrate themselves.
%\begin{figure}[ht]
%  \includegraphics[width=\linewidth]{lna3rho}
%  \caption{The value of $C=\ln(a^{3(1+w)}\bar{\rho})$ as a function of the number of efolds after inflation.  The shock-in-time provides a kick to $C$ followed by a slower post shock evolution.  The parameters are as in fig~\ref{fig:shock}.}
%  \label{fig:cons_charge}
%\end{figure}
For comparison, let's consider the behaviour of $C$ for the case of a universe composed of noninteracting matter and radiation with $f=\rho_{\mathrm{mat,init}}/\rho_{\mathrm{rad,init}}$,
and also for a universe filled by a fluid with $w=w_i$ which instantaneously decays another fluid with $w=w_f$ at some moment $a_{dec}$.
In the former case, we have
\begin{equation}
  C(a) = -\frac{af}{1+af}\ln(a) +\ln(1+af) + \ln(\rho_{\mathrm{rad,init}})
\end{equation}
while in the latter $C$ has a jump at $a_{dec}$ of size
\begin{equation}
  \Delta C = 3(w_f-w_i)\ln(a_{dec}) \, .
\end{equation}
We see that our model of preheating combines these two effects.
The sharp step is analogous to the instantaneous decay of one fluid into another, while the more gradual decrease is coincides with the
evolution of a universe composed of two fluids.
More generally, this post-shock evolution will occur for any model whose lagrangian has dimensionful parameters and fields which have virialized and gone nonlinear,
since $w$ is then a function of various moments of the fields.
For lagrangians with no dimensionful parameters, the post-shock evolution will be absent ($w=1/3$ in 3+1 dimensions) but the shock-in-time itself can still leave an imprint on C~\cite{Bond:2009xx}.

%\section{Modulated Preheating}
A possible source of (nongaussian) curvature perturbation arises from the modulated reheating mechanism~\cite{Kofman:2003nx}.
In this scenario, the decay rate of the inflaton is a function of some other light (moduli) fields which aquire large-scale
perturbations during inflation.
This leads to a spatially dependent decay rate for the inflaton and a corresponding spatial modulation in the expansion history.
A similar mechanism is at work here if the parameter $g^2$ is a function of some other light field $\sigma$ which acquires large scale perturbations.
Above we saw that the expansion history is modified by both the shock-in-time itself and the post shock evolution.
In general, the post-shock evolution is complicated and requires either high resolution numerical simulation or 
else an appropriate analytic theory onto which to match the numerics.
As well, it will be influenced by the couplings of the fields to the Standard Model, which are not included in any lattice codes.
Due to these complications, we will instead restrict ourselves to the first effect, 
specifically the timing of the shock as a function of model parameters.
Much lower resolution simulations can properly capture this, which allows us to make progress using numerical techniques.

\begin{figure}[h]
%\includegraphics[width=\linewidth]{scang_dsdt_white_lna3pt3}
\includegraphics[width=0.9\linewidth]{shock_g}
\caption{The entropy production rate $dS/dt$ as a function of $\ln(a)$ and coupling constant $g^2$ and normalized to
the maximal production rate over the set of parameters ($g^2$ and $\ln(a)$).  The simulations were run on a $32^3$ grid with comoving side $mL=5$ and a time step of $m\mathrm{d}t=0.00025$.}
\label{fig:modulated}
\end{figure}

If superhorizon fluctuations of $\sigma$ are present, then we can approximate $g^2(\sigma)$ as taking
a spatially dependent value that is nontheless uniform over a Hubble patch.
Then different patches will undergo evolutions approximated by a single realization of one of our lattice simulations,
with the choice of parameter $g^2$ determined by the local value of $\sigma$.
In figure~\ref{fig:modulated} we show the entropy production rates as we vary the parameter $g^2$
The time of the shock is a rather spiky function of $g^2$, 
suggesting that the resulting shock surface would be rather corrugated in the physical spacetime. 
This could lead to interesting levels of nongaussianity and possibly isolated features such as cold spots.
A proper accounting the final observable effects requires a proper accounting of the post-shock evolution,
and we leave a more detailed study to future work. 
 
%\section{Acknowledgements}
JB was supported by NSERC and the University of Toronto.

\bibliography{shock}{}

\end{document}
