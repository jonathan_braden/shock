\documentclass{article}

\usepackage{graphics}
\usepackage{graphicx}
\usepackage{amsmath, amssymb}

\begin{document}

\begin{figure}
  \centering
  \includegraphics[width=0.75\linewidth]{figs/shock_variables_n512_L5_filter2}
  \caption{{\it top:} The entropy per effective degree of freedom as a function of the number of efolds since the end of inflation (defined as $\epsilon=1$).{\it middle:} Our equivalent to the Mach number $1/<\log(\rho/\bar{\rho})>$ which provides a measure of how fluctuation dominated the system is. {\it bottom:}  The quantity $C \equiv \ln(a^{3(1+w)}\ln(\rho))$ which is a constant during any epoch when the equation of state parameter $w \equiv \bar{P}/\bar{\rho}$ is constant.  Since the universe must eventually reach a radiation dominated state prior to BBN, variations in $C$ in various Hubble patches can be used to detect perturbations to the expansion history $\delta\ln(a)$.  In all three panels, the red band is shaded according to $dS/dt$ and is meant to indicate the location of the shock-in-time.  As well, the light blue line represents the raw unfiltered time series, with the dark blue line the result after applying a kaiser filter to remove high frequency oscillations.  The green curve represents the (normalized) entropy production rate.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{figs/shock_cut}
  \caption{The entropy production rate as a function of the upper limit taken in the sum over Fourier modes.  Here $dS/dt$ has been normalized to the maximum value in the graph.  This is mostly to show the shock timing is fairly robust to our choice of cutoff in momentum space.  The only slightly worrisome feature is that for the $N=32^3$ boxes that I used to do the big scan, the Nyquist wavenumber as well as the maximal wavenumber inside the box are smaller than 50, which is roughly where the shock timing begins to level out.  Scanning using higher resolution boxes doesn't remove the corrugation (at least over the restricted range of parameters I checked), so the general conclusion won't change.  If we want, I can rerun with $64^3$ boxes and the same side length for the box.  I haven't done this yet simply because it takes about 10 times longer.}
\end{figure}

%\begin{figure}
%  \centering
%  \includegraphics[width=0.45\linewidth]{figs/shock_variables_n512_L5_filter2}
%  \caption{The same as the figure above, except this time using the potential $V(\phi,\chi) = 1/2m^2\phi^2 + 1/2\sigma\chi\phi^2 + 1/4\lambda\chi^4$.
%\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{figs/scan_g_fine_badcolours}
  \hfill
  \includegraphics[width=0.45\linewidth]{figs/shock_g}
  \caption{The entropy production rate (with arbitrary normalization) for $V(\phi,\chi) = 1/2m^2\phi^2 + g^2\phi^2\chi^2$ as a function of the coupling constant $g$ and scale factor $\ln(a/a_{end})$.  All simulations were performed on a lattice of side length $mL=5$ with $N=32$ lattice points per side and a time step of $mdt \leq dx/80 = 5/(32*80)$.  We checked that the results are insensitive to taking a smaller time step, the actual realization of the initial fluctuations, and the size of the simulation box for a fixed $dx$.  This is showing the same thing as the figure already in the paper, except I scanned with a much finer resolution in the coupling constant $g$ (steps of $\delta gM_{pl} = 0.25m$).  As well, this uses the symplectic code, so the Hubble constraint is satisfied to better than $10^{-6}$ at all times for every run used to make the plot (the constraint is satisfied better for smaller values of $g$, and is at roughly machine precision for the smaller values of $g$).  For comparison, I've also include the old version on the right.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{figs/entropy_rate_3fld_g0}
  \caption{The entropy production rate (with arbitrary normalization) for the potential $V(\phi,\chi,\sigma) = 1/2m^2\phi^2 + \delta^2\sigma^2\chi^2\phi^2$ with $\delta = 10^2$ as a function of $\delta\bar{\sigma} \equiv g_{eff}$ and the scale factor.  Here, we have varied the initial mean value of the field $\sigma$ in the cube in order to modulate the effective coupling between $\phi$ and $\chi$ rather than simply setting the coupling by hand.  As can be seen, at least for this choice of $\delta$, modulating the coupling with another field rather than by changing the coupling by hand does not destroy the spiky nature of the shock-in-time surface.  It remains to be seen whether or not choosing larger values of $\delta$ modifies this picture.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{figs/ent_spec_phi}
  \hfill
  \includegraphics[width=0.45\linewidth]{figs/ent_spec_phi_contour}
  \caption{The entropy spectra for the two the field $\phi$ treating $\phi$,$\pi_{\phi}$ (with $\pi_{\phi}$ the canonical momenta) as the underlying gaussian random fields.  I've defined $F_k \equiv P_{\phi\phi}P_{\pi\pi} - |\phi_k\pi_{-k}|^2$.  If we ignore cross-correlations between the fields $\phi$ and $\chi$, then $S=\sum_k \log(F_k)$ and $P_{f,f}$ power spectrum of $f$.  Here, if we consider the full gaussian entropy of the two field system, there are additional terms from cross-correlations between the two fields $\phi$ and $\chi$.  This choice of variables has the nice property of not having wild oscillations in the spectrum while the homogeneous part of the inflaton is oscillating around the minimum of the potential.}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{figs/ent_spec_chi}
  \hfill
  \includegraphics[width=0.45\linewidth]{figs/ent_spec_chi_contour}
  \caption{Same as the figure above except for the field $\chi$.}
\end{figure}

A slight word of warning here.  The entropy spectra rely on computing cross-power spectra between various fields.  I've done some testing to make sure that all of my subroutines that compute these are working properly, but I haven't done nearly as much testing as for the toher

I also tried plotting the equivalent of the entropy spectrum for the $\log(\rho)$ field, but it ends up being really messy looking at early times because the spectrum undergoes oscillations of orders of magnitude in amplitude (especially at larger k) which leads to a wildly oscillating contribution to $k^2\log(P_k)$ (which takes on huge negative values).  Until I figure out a nice way to deal with this (probably doing a time average will fix this), I'm not going to include the figure.  These same oscillations are present of in the spectra of $\phi$ or $\pi_{\phi}$ individually, but the combination present in the entropy calculation removes most of the oscillations leading to a much nicer looking quantity.  

Another potentially interesting figure is the spectrum of $\frac{\langle \delta P_k\delta\rho_{-k}\rangle}{\langle |\delta\rho_k|^2\rangle}$ (or the appropriate alternative with $\log(\rho)$ instead of $\rho$ which is sort of like a wavenumber dependent sound speed.  I was going to send a plot of this, but what I get looks very strange (in particular the cross-spectrum) and I'm not sure that it's right so I'm going to run some more tests to figure out what's going on.

\begin{figure}
  \begin{tabular}{ccc}
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t0}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t115}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t118}}} \\

    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t120}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t121}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t122}}} \\

    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t123}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t125}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t130}}} \\

    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t300}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t400}}} &
    \includegraphics[width=0.33\linewidth]{{{m2g2_kurtosis_t500}}}
  \end{tabular}
  \caption{Evolution of $\langle|f|^4\rangle/\langle|f|^2\rangle^2$ for $\phi$, $\chi$ and $\ln(\rho/\bar{\rho})$.  For a Gaussian field this quantity is 2.}
\end{figure}

\end{document}
